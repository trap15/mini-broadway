#!/bin/sh
SCRIPTDIR=`dirname $PWD/$0`
die() {
	echo $@
	exit 1
}
build() {
	(echo "****************\nBuilding $@ test!" && cd $SCRIPTDIR/$@ && make) || die "Can't build $@ test"
}
clean() {
	(echo "****************\nCleaning $@ test!" && cd $SCRIPTDIR/$@ && make clean) || die "Can't clean $@ test"
}
if [ "$1" = "clean" ] || [ "$1" = "rebuild" ] ; then
	clean dsp
	clean dvd
	clean nand
	clean paired
	clean slave
	clean thread
fi
if [ "$1" != "clean" ] ; then
	build dsp
	build dvd
	build nand
	build paired
	build slave
	build thread
fi
cd $SCRIPTDIR

