/*
        Paired singles example -- Shows an example using paired singles

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>

void *xfb = NULL;

int perform_paired_testing()
{
	int pass = 0;
	double pair[4];
	s16 data[2];
	s16 test[2];
	
	s16 out[2];
	paired_set_gqr(0, 0, GQR_TYPE_S16, 0);
	paired_set_gqr(0, 1, GQR_TYPE_S16, 0);
	
	printf("Load/Store test... ");
	data[0] = test[0] = -43;
	data[1] = test[1] = 2;
	pair[3] = paired_load(0, data, 0);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	
	printf("Absolute test... ");
	data[0] = -43;
	data[1] = 2;
	test[0] = abs(data[0]);
	test[1] = abs(data[1]);
	pair[3] = paired_abs(paired_load(0, data, 0));
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	
	printf("Negative Absolute test... ");
	data[0] = -43;
	data[1] = 2;
	test[0] = -abs(data[0]);
	test[1] = -abs(data[1]);
	pair[3] = paired_nabs(paired_load(0, data, 0));
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	
	printf("Negative test... ");
	data[0] = -43;
	data[1] = 2;
	test[0] = -data[0];
	test[1] = -data[1];
	pair[3] = paired_neg(paired_load(0, data, 0));
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Reciprocal test... ");
	data[0] = -43;
	data[1] = 2;
	test[0] = -(1/data[0]);
	test[1] = -(1/data[1]);
	pair[3] = paired_reciprocal(paired_load(0, data, 0));
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	
	/* can't test sqrt_reciprocal */
	
	printf("Add test... ");
	data[0] = -43;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 21;
	data[1] = -7;
	test[0] += data[0];
	test[1] += data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_add(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	
	printf("Divide test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] /= data[0];
	test[1] /= data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_div(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	
	printf("Multiply test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] *= data[0];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_mul(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Subtract test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] -= data[0];
	test[1] -= data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_sub(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Multiply-add test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 2;
	data[1] = 4;
	test[0] *= data[0];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] += data[0];
	test[1] += data[1];
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_madd(pair[0], pair[1], pair[2]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Multiply-add high test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 2;
	data[1] = 4;
	test[0] *= data[0];
	test[1] *= data[0];
	pair[1] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] += data[0];
	test[1] += data[1];
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_madds0(pair[0], pair[1], pair[2]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Multiply-add low test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 2;
	data[1] = 4;
	test[0] *= data[1];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] += data[0];
	test[1] += data[1];
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_madds1(pair[0], pair[1], pair[2]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Multiply-subtract test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 2;
	data[1] = 4;
	test[0] *= data[0];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] -= data[0];
	test[1] -= data[1];
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_msub(pair[0], pair[1], pair[2]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Multiply high test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] *= data[0];
	test[1] *= data[0];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_muls0(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Multiply low test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] *= data[1];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_muls1(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Negative Multiply-add test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 2;
	data[1] = 4;
	test[0] *= data[0];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] += data[0];
	test[1] += data[1];
	pair[2] = paired_load(0, data, 0);

	test[0] = -test[0];
	test[1] = -test[1];
	pair[3] = paired_nmadd(pair[0], pair[1], pair[2]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Negative Multiply-subtract test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 2;
	data[1] = 4;
	test[0] *= data[0];
	test[1] *= data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[0] -= data[0];
	test[1] -= data[1];
	pair[2] = paired_load(0, data, 0);

	test[0] = -test[0];
	test[1] = -test[1];
	pair[3] = paired_nmsub(pair[0], pair[1], pair[2]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Merge high test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[1] = data[0];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_merge00(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Merge direct test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[0];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[1] = data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_merge01(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Merge swapped test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[1] = data[0];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_merge10(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Merge low test... ");
	data[0] = -44;
	data[1] = 2;
	test[0] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 22;
	data[1] = -2;
	test[1] = data[1];
	pair[1] = paired_load(0, data, 0);

	pair[3] = paired_merge11(pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Select test... ");
	data[0] = 22;
	data[1] = -2;
	test[1] = data[1];
	pair[0] = paired_load(0, data, 0);

	data[0] = 7;
	data[1] = 58;
	test[0] = data[0];
	pair[1] = paired_load(0, data, 0);

	data[0] = -1;
	data[1] = 1;
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_select(pair[2], pair[0], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Vector sum high test... ");
	data[0] = 22;
	data[1] = -2;
	test[0] = data[0];
	pair[0] = paired_load(0, data, 0);

	data[0] = 7;
	data[1] = 58;
	test[0] += data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = -1;
	data[1] = 29;
	test[1] = data[1];
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_sum0(pair[0], pair[2], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	printf("Vector sum low test... ");
	data[0] = 22;
	data[1] = -2;
	test[1] = data[0];
	pair[0] = paired_load(0, data, 0);

	data[0] = 7;
	data[1] = 58;
	test[1] += data[1];
	pair[1] = paired_load(0, data, 0);

	data[0] = -1;
	data[1] = 29;
	test[0] = data[0];
	pair[2] = paired_load(0, data, 0);

	pair[3] = paired_sum1(pair[0], pair[2], pair[1]);
	paired_store(0, out, 0, pair[3]);
	if((out[0] != test[0]) || (out[1] != test[1])) {
		printf("FAILURE (%d %d %d %d)\n", out[0], out[1], test[0], test[1]);
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	return pass;
}

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	int pass = 0;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}
	
	printf("===============================\n");
	pass = perform_paired_testing();
	printf("\n%d/24 tests passed.\n", pass);
	printf("===============================\n");
	usleep(5000000);

	return 0;
}

