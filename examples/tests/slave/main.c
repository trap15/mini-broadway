/*
        Slave example -- Shows an example using the Slave interface

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>

#include "slave_bin.h"

void *xfb = NULL;

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	int pass = 0;
	u32 params[4];
	u32 *results;
	int tmp = 0;
	char spinchar[4] = "/-\\|";

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}
	
	printf("===============================\n");
	printf("Loading slave code... ");
	if(slave_load(slave_bin, slave_bin_size) != 0) {
		printf("FAILURE\n");
		goto end_code;
	}
	printf("SUCCESS\n");
	printf("Loading parameters... ");
	params[0] = 0xDEADBEEF;
	slave_params(params, 4);
	printf("SUCCESS\n");
	printf("Scheduling code to execute... ");
	slave_execute();
	printf("EXECUTING\n");
	tmp = 0;
	while(slave_executing())
		printf("\b%c", spinchar[tmp++ % 4]);
	printf("	Code executed in %d spins.\n", tmp);

	printf("Analyzing results...\n");
	results = slave_results();
	printf("	STR 42 Test: ");
	if(results[0] != 42) {
		printf("FAILURE (%lu)\n", results[0]);
	}else{
		pass++;
		printf("SUCCESS\n");
	}
	printf("	STR param[0] Test: ");
	if(results[1] != params[0]) {
		printf("FAILURE (%lu)\n", results[1]);
	}else{
		pass++;
		printf("SUCCESS\n");
	}
	printf("	memset 5 Test: ");
	if((results[2] != 0x05050505) || (results[3] != 0x05050505)) {
		printf("FAILURE (%08lX %08lX)\n", results[2], results[3]);
	}else{
		pass++;
		printf("SUCCESS\n");
	}
	printf("	memcpy Test: ");
	if((results[4] != results[1]) || (results[5] != results[2])) {
		printf("FAILURE (%08lX %08lX)\n", results[4], results[5]);
	}else{
		pass++;
		printf("SUCCESS\n");
	}
end_code:
	printf("\n%d/4 tests passed.\n", pass);
	printf("===============================\n");
	usleep(5000000);

	return 0;
}

