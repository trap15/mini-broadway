/*
        NAND example -- Shows an example using the NANDFS interface

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <diskmii.h>
#include <video_low.h>
#include <console.h>

void *xfb = NULL;

static char ascii(char s)
{
	if(s < 0x20) return '.';
	if(s > 0x7E) return '.';
	return s;
}

void hexdump(void *d, int len)
{
	u8 *data;
	int i, off;
	data = (u8*)d;
	for(off = 0; off < len; off += 16) {
		printf("%08x  ",off);
		for(i = 0; i < 16; i++)
			if((i + off) >= len) printf("   ");
			else printf("%02x ", data[off + i]);

		printf(" ");
		for(i = 0; i < 16; i++)
			if((i + off) >= len) printf(" ");
			else printf("%c", ascii(data[off + i]));
		printf("\n");
	}
}

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	struct nandfs_fp fp;
	u8* data;
	int pass = 0;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}
	if(nandfs_initialize() != 0) {
		printf("Unable to init NAND FS.\n");
		goto end_test;
	}
	
	printf("===============================\n");
	printf("NAND Usage: %lu%%\n", nandfs_get_usage());
	if(nandfs_open(&fp, "/sys/cert.sys") != 0) {
		printf("Opening /sys/cert.sys failed.\n");
		goto end_test;
	}
	
	data = malloc(fp.size);
	if(data == NULL) {
		printf("Allocating file memory %ld failed.\n", fp.size);
		goto end_test;
	}
	
	if((u32)nandfs_read(data, fp.size, 1, &fp) != fp.size) {
		printf("Reading file failed.\n");
	}else{
		hexdump(data, fp.size);
		pass++;
	}
	free(data);
end_test:
	printf("\n%d/1 test passed.\n", pass);
	printf("===============================\n");
	usleep(5000000);

	return 0;
}

