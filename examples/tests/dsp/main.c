/*
        DSP example -- Shows an example using the DSP interface

Copyright (C) 2008, 2009        Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009              Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2008, 2009        Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2008, 2009        Sven Peter <svenpeter@gmail.com>
Copyright (C) 2009              John Kelley <wiidev@kelley.ca>
Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <input.h>
#include <console.h>

#include "dsp_test_dsp_bin.h"

u16 dsp_buffer[0x4000] ATTRIBUTE_ALIGN(32);

static int fired = 0;

#define DELAY_TIME		(500000)

void *xfb = NULL;

gc_pad_t pads[4];

int _dsp_ucode_test_handler(u32 irq, void* data)
{
	u32 inbox;
	gecko_printf("DSP IRQ fired.\n");
	while(!dsp_check_inbox());
	inbox = dsp_recv_mail();
	gecko_printf("Receiveda %08X\n", inbox);
	(void)irq;
	(void)data;
	return 1;
}

int do_dsp_testing()
{
	u32 inbox;
	void* ucode_iram;
	dsp_ucode_t ucode;
	(void)ucode_iram;
	memcpy(dsp_buffer, dsp_test_dsp_bin, dsp_test_dsp_bin_size);
	ucode.iram_src = dsp_buffer;
	ucode.iram_dst = 0;
	ucode.iram_len = dsp_test_dsp_bin_size & ~0x1F;
	ucode.dram_src = NULL;
	ucode.dram_dst = 0;
	ucode.dram_len = 0;
	ucode.pc = 0x10;
	for(fired = 0; fired < 4; fired++) {
		printf("Uploading uCode...\n");
		dsp_reset();
		if(!dsp_upload_ucode(ucode)) {
			printf("Error uploading uCode.\n");
			goto dsp_test_end;
		}
	}
	
	printf("Shooting 0x8BADC0DE\n");
	usleep(5000);
	dsp_send_mail(0x8BADC0DE);
	usleep(5000);
//	while(dsp_check_outbox());
	inbox = dsp_recv_mail();
	gecko_printf("Received %08X\n", inbox);

	printf("Shooting 0x9337C0DE\n");
	usleep(5000);
	dsp_send_mail(0x9337C0DE);
//	while(dsp_check_outbox());
	inbox = dsp_recv_mail();
	gecko_printf("Received %08X\n", inbox);
	
	printf("Shooting 0x80088008\n");
	usleep(5000);
	dsp_send_mail(0x80088008);
//	while(dsp_check_outbox());
	inbox = dsp_recv_mail();
	gecko_printf("Received %08X\n", inbox);
	usleep(5000);
	
dsp_test_end:
	printf("===============================\n");
	return 0;
}

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	int pass = 0;
	int tests = 0;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}

	tests++;
	printf("===============================\n");
	printf("Initializing DSP... ");
	ai_initialize();
	if(!dsp_initialize()) {
		printf("FAILURE\n");
		goto end_test;
	}
	dsp_unhalt();
	dsp_reset();
	if(!irq_dsp_register_handler(_dsp_ucode_test_handler, NULL)) {
		printf("FAILURE\n");
		goto end_test;
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	tests++;
	if(!do_dsp_testing()) {
		printf("FAILURE\n");
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	tests++;
	printf("Shutting down DSP... ");
	if(!dsp_shutdown()) {
		printf("FAILURE\n");
		goto end_test;
	}else{
		printf("SUCCESS\n");
		pass++;
	}
end_test:
	printf("\n%d/%d test passed.\n", pass, tests);
	printf("===============================\n");
	for(;;);

	return 0;
}

