/*
        Threads example -- Shows an example using libthreads

Copyright (C) 2010-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>
#include <threads.h>
#include <atomics.h>
#include <locks.h>

void *xfb = NULL;

int single_thread_sanity_test();
int second_thread_test(int prio);
int single_atomics_test();
int threaded_atomics_test();
int single_locks_test();
int threaded_locks_test();

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	int pass = 0;
	int tests = 0;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}

	printf("===============================\n");
	printf("Initializing libthreads... ");
	tests++;
	if(!threads_initialize()) {
		printf("FAILURE\n");
		goto end_test;
	}else{
		printf("SUCCESS\n");
		pass++;
	}

	tests++;
	if(single_thread_sanity_test()) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(second_thread_test(-5)) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(second_thread_test(5)) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(second_thread_test(0)) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(single_atomics_test()) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(threaded_atomics_test()) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(single_locks_test()) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	if(threaded_locks_test()) {
		printf("SUCCESS\n");
		pass++;
	}else
		printf("FAILURE\n");

	tests++;
	printf("Shutting down libthreads... ");
	if(!threads_shutdown()) {
		printf("FAILURE\n");
		goto end_test;
	}else{
		printf("SUCCESS\n");
		pass++;
	}
end_test:
	printf("\n%d/%d tests passed.\n", pass, tests);
	printf("===============================\n");
	for(;;);

	return 0;
}

int single_thread_sanity_test()
{
	int i;
	char stack_data[1024];
	printf("Testing single-threaded sanity... ");
	for(i = 0; i < 1024; i++) {
		stack_data[i] = i & 0xFF;
	}
	usleep(500);
	for(i = 0; i < 1024; i++) {
		if(stack_data[i] != (i & 0xFF)) {
			return 0;
		}
	}
	return 1;
}

int second_thread_test_thread()
{
	char stack_data[16];
	stack_data[0] = 0xDE;
	stack_data[1] = 0xAD;
	stack_data[2] = 0xBE;
	stack_data[3] = 0xEF;
	stack_data[4] = 0xCA;
	stack_data[5] = 0xFE;
	stack_data[6] = 0xBA;
	stack_data[7] = 0xBE;
	stack_data[8] = 0xDE;
	stack_data[9] = 0xAD;
	stack_data[10] = 0xBA;
	stack_data[11] = 0xBE;
	stack_data[12] = 0xCA;
	stack_data[13] = 0xFE;
	stack_data[14] = 0xBE;
	stack_data[15] = 0xEF;
	return 1;
}

int second_thread_test(int prio)
{
	char* secondstack = malloc(2048);
	printf("Testing second thread with %d priority... ", prio);
	int pid = threads_create(prio, THREAD_RUNNABLE, secondstack, 2048, (void*)second_thread_test_thread);
	while(threads_state(pid) != -2) {
		threads_yield();
	}
	int exitcode = threads_exitcode(pid);
	if(exitcode != 1) {
		gecko_printf("Bad Exitcode %d ", exitcode);
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!threads_destroy(pid)) {
		free(secondstack);
		return 0;
	}
	free(secondstack);
	return 1;
}

int single_atomics_test()
{
	u32 outresult;
	printf("Testing atomic operations with single thread... ");
	u32 atom1 = 72, atom2 = 9001, atom3 = 192837;
	outresult = atomics_read(&atom1);
	if(outresult != 72) {
		gecko_printf("Atomic Read atom1 %08X ", outresult);
		return 0;
	}
	outresult = atomics_read(&atom2);
	if(outresult != 9001) {
		gecko_printf("Atomic Read atom2 %08X ", outresult);
		return 0;
	}
	outresult = atomics_read(&atom3);
	if(outresult != 192837) {
		gecko_printf("Atomic Read atom3 %08X ", outresult);
		return 0;
	}
	atomics_set(&atom2, 0xCAFEBABE);
	outresult = atomics_read(&atom2);
	if(outresult != 0xCAFEBABE) {
		gecko_printf("Atomic Set atom2 %08X ", outresult);
		return 0;
	}
	atomics_add(&atom2, 0x13AF0431, ATOMIC_COMPARE_NONE);
	outresult = atomics_read(&atom2);
	if(outresult != 0xDEADBEEF) {
		gecko_printf("Atomic add atom2 %08X ", outresult);
		return 0;
	}
	atomics_inc(&atom3, ATOMIC_COMPARE_NONE);
	outresult = atomics_read(&atom3);
	if(outresult != 192838) {
		gecko_printf("Atomic inc atom3 %08X ", outresult);
		return 0;
	}
	atomics_sub(&atom2, 0x13AF0431, ATOMIC_COMPARE_NONE);
	outresult = atomics_read(&atom2);
	if(outresult != 0xCAFEBABE) {
		gecko_printf("Atomic sub atom2 %08X ", outresult);
		return 0;
	}
	atomics_dec(&atom3, ATOMIC_COMPARE_NONE);
	outresult = atomics_read(&atom3);
	if(outresult != 192837) {
		gecko_printf("Atomic dec atom3 %08X ", outresult);
		return 0;
	}
	int bit = atomics_bitset(&atom2, 0);
	outresult = atomics_read(&atom2);
	if(outresult != 0xCAFEBABF) {
		gecko_printf("Atomic bitset atom2a %08X ", outresult);
		return 0;
	}
	if(bit != 0) {
		gecko_printf("Atomic bitchk atom2a %d ", bit);
		return 0;
	}
	bit = atomics_bitclear(&atom2, 0);
	outresult = atomics_read(&atom2);
	if(outresult != 0xCAFEBABE) {
		gecko_printf("Atomic bitclr atom2b %08X ", outresult);
		return 0;
	}
	if(bit != 1) {
		gecko_printf("Atomic bitchk atom2b %d ", bit);
		return 0;
	}
	bit = atomics_bitchange(&atom2, 0);
	outresult = atomics_read(&atom2);
	if(outresult != 0xCAFEBABF) {
		gecko_printf("Atomic bitchg atom2c %08X ", outresult);
		return 0;
	}
	if(bit != 0) {
		gecko_printf("Atomic bitchk atom2c %d ", bit);
		return 0;
	}
	bit = atomics_bitcheck(&atom2, 0);
	if(bit != 1) {
		gecko_printf("Atomic bitchk atom2d %d ", bit);
		return 0;
	}
	return 1;
}

static u32 thread_atom1, thread_atom2, thread_atom3;

int atomic_thread_test_thread(void)
{
	int i;
	u32 outresult = 0;
	while(outresult != 0x0BADC0DE) {
		threads_yield();
		outresult = atomics_read(&thread_atom1);
	}
	atomics_set(&thread_atom3, 0);
	atomics_set(&thread_atom2, 0xDEADBABE);
	for(i = 0; i < 100000; i++) {
		atomics_add(&thread_atom3, 2, ATOMIC_COMPARE_NONE);
	}
	atomics_set(&thread_atom1, 0xBEEFBABE);
	return 1;
}

int threaded_atomics_test()
{
	u32 outresult;
	thread_atom1 = 72;
	thread_atom2 = 9001;
	thread_atom3 = 192837;
	printf("Testing atomic operations with two threads... ");
	char* secondstack = malloc(2048);
	int pid = threads_create(0, THREAD_RUNNABLE, secondstack, 2048, (void*)atomic_thread_test_thread);
	atomics_set(&thread_atom1, 0x0BADC0DE);
	threads_yield();
	outresult = 0;
	int i = 0;
	while(outresult != 0xDEADBABE) {
		threads_yield();
		outresult = atomics_read(&thread_atom2);
		i++;
	}
	for(i = 0; i < 100000; i++) {
		atomics_add(&thread_atom3, 4, ATOMIC_COMPARE_NONE);
	}
	outresult = 0;
	while(outresult != 0xBEEFBABE) {
		threads_yield();
		outresult = atomics_read(&thread_atom1);
	}
	if(atomics_read(&thread_atom3) != 600000) {
		gecko_printf("Atomic adds failed %d ", atomics_read(&thread_atom3));
	}
	while(threads_state(pid) != -2) {
		threads_yield();
	}
	int exitcode = threads_exitcode(pid);
	if(exitcode != 1) {
		gecko_printf("Bad Exitcode %d ", exitcode);
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!threads_destroy(pid)) {
		free(secondstack);
		return 0;
	}
	free(secondstack);
	return 1;
}

int single_locks_test()
{
	printf("Testing locks with single thread... ");
	semaphore_t sema1, sema2, sema3;
	sema1 = semaphores_create(1);
	sema2 = semaphores_create(2);
	sema3 = semaphores_create(3);
	if(!semaphores_lock_noblock(&sema1)) {
		gecko_printf("Unable to lock sema1a ");
		return 0;
	}
	if(semaphores_lock_noblock(&sema1)) {
		gecko_printf("Able to lock sema1a ");
		return 0;
	}
	semaphores_unlock(&sema1);
	if(!semaphores_lock_noblock(&sema1)) {
		gecko_printf("Unable to lock sema1b ");
		return 0;
	}

	if(!semaphores_lock_noblock(&sema2)) {
		gecko_printf("Unable to lock sema2a ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema2)) {
		gecko_printf("Unable to lock sema2b ");
		return 0;
	}
	if(semaphores_lock_noblock(&sema2)) {
		gecko_printf("Able to lock sema2b ");
		return 0;
	}
	semaphores_unlock(&sema2);
	if(!semaphores_lock_noblock(&sema2)) {
		gecko_printf("Unable to lock sema2c ");
		return 0;
	}
	if(semaphores_lock_noblock(&sema2)) {
		gecko_printf("Able to lock sema2c ");
		return 0;
	}
	semaphores_unlock(&sema2);
	semaphores_unlock(&sema2);
	if(!semaphores_lock_noblock(&sema2)) {
		gecko_printf("Unable to lock sema2d ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema2)) {
		gecko_printf("Unable to lock sema2e ");
		return 0;
	}

	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3a ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3b ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3c ");
		return 0;
	}
	if(semaphores_lock_noblock(&sema3)) {
		gecko_printf("Able to lock sema3c ");
		return 0;
	}
	semaphores_unlock(&sema3);
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3d ");
		return 0;
	}
	if(semaphores_lock_noblock(&sema3)) {
		gecko_printf("Able to lock sema3d ");
		return 0;
	}
	semaphores_unlock(&sema3);
	semaphores_unlock(&sema3);
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3e ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3f ");
		return 0;
	}
	semaphores_unlock(&sema3);
	semaphores_unlock(&sema3);
	semaphores_unlock(&sema3);
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3g ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3h ");
		return 0;
	}
	if(!semaphores_lock_noblock(&sema3)) {
		gecko_printf("Unable to lock sema3i ");
		return 0;
	}

	return 1;
}

semaphore_t thread_sema1, thread_sema2, thread_sema3;

int locking_thread_test_thread(void)
{
	semaphores_lock(&thread_sema1);
	int oldfrz = threads_freeze();
	if(!semaphores_lock_noblock(&thread_sema2)) {
		gecko_printf("Unable to lock sema2b ");
		return 0;
	}
	if(semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Able to lock sema3a ");
		return 0;
	}
	semaphores_unlock(&thread_sema1);
	semaphores_unlock(&thread_sema3);
	semaphores_unlock(&thread_sema3);
	semaphores_unlock(&thread_sema3);
	threads_unfreeze(oldfrz);
	semaphores_unlock(&thread_sema2);
	return 1;
}

int threaded_locks_test()
{
	thread_sema1 = semaphores_create(1);
	thread_sema2 = semaphores_create(2);
	thread_sema3 = semaphores_create(3);
	printf("Testing locks with multiple threads... ");
	if(!semaphores_lock_noblock(&thread_sema1)) {
		gecko_printf("Unable to lock sema1a ");
		return 0;
	}
	char* secondstack = malloc(2048);
	int pid = threads_create(0, THREAD_RUNNABLE, secondstack, 2048, (void*)locking_thread_test_thread);
	if(!semaphores_lock_noblock(&thread_sema2)) {
		gecko_printf("Unable to lock sema2a ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Unable to lock sema3a ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Unable to lock sema3b ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Unable to lock sema3c ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	threads_yield();
	semaphores_unlock(&thread_sema1);
	threads_yield();
	semaphores_lock(&thread_sema2);
	if(!semaphores_lock_noblock(&thread_sema1)) {
		gecko_printf("Unable to lock sema1e ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(semaphores_lock_noblock(&thread_sema2)) {
		gecko_printf("Able to lock sema2f ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Unable to lock sema3g ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Unable to lock sema3h ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!semaphores_lock_noblock(&thread_sema3)) {
		gecko_printf("Unable to lock sema3i ");
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	while(threads_state(pid) != -2) {
		threads_yield();
	}
	int exitcode = threads_exitcode(pid);
	if(exitcode != 1) {
		gecko_printf("Bad Exitcode %d ", exitcode);
		threads_destroy(pid);
		free(secondstack);
		return 0;
	}
	if(!threads_destroy(pid)) {
		free(secondstack);
		return 0;
	}
	free(secondstack);
	return 1;
}

