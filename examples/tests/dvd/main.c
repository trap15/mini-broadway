/*
        DVD example -- Shows an example using the DI interface

Copyright (C) 2010-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>

#define MAX_TIMEOUT	(500000)

void *xfb = NULL;

static int completed_transfer = 0;
int transfer_complete(u32 irq, void* data)
{
	(void)data;
	usleep(500);
	gecko_printf("IRQ %d\n", irq);
	completed_transfer = 1;
	return 0;
}

static int drive_unlocked = 0;
int drive_unlock_3(u32 irq, void* data)
{
	(void)irq;
	(void)data;
	drive_unlocked = 1;
	return 0;
}

int drive_unlock_2(u32 irq, void* data)
{
	(void)irq;
	(void)data;
	irq_di_register_handler(IRQ_DI_TC, drive_unlock_3, NULL);
	di_set_command(DI_CMD_UNLOCK, 0x00, ('D' << 8) | 'V',
			('D' << 24) | ('-' << 16) | ('G' << 8) | 'A',
			('M' << 24) | ('E' << 16) | 0x0300);
	di_transfer(FALSE, FALSE);
	while(!drive_unlocked) usleep(100);
	return 0;
}

void drive_unlock(void)
{
	irq_handler_t oldhandler;
	oldhandler = irq_di_get_handler(IRQ_DI_TC);
	irq_di_register_handler(IRQ_DI_TC, drive_unlock_2, NULL);
	di_set_command(DI_CMD_UNLOCK, 0x01, ('M' << 8) | 'A',
			('T' << 24) | ('S' << 16) | ('H' << 8) | 'I',
			('T' << 24) | ('A' << 16) | 0x0200);
	di_transfer(FALSE, FALSE);
	while(!drive_unlocked) usleep(100);
	irq_di_register_handler(IRQ_DI_TC, oldhandler.exec, oldhandler.data);
}

int drive_error(u32 irq, void* data)
{
	u32 err;
	(void)irq;
	(void)data;
	usleep(100);
	gecko_printf("Drive error\n");
	di_set_command(DI_CMD_GETERROR, 0, 0, 0, 0);
	di_transfer(FALSE, FALSE);
	err = di_immediate_recv();
	gecko_printf("Error code: %08X\n", err);
	di_set_command(DI_CMD_DMAREAD, DI_DMAREAD_DISCID, 0, 0, 0x20);
	di_set_dma((u32)data, 0x20);
	di_transfer(FALSE, TRUE);
	return 0;
}

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	u8* data;
	int pass = 0;
	int timer;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}

	printf("===============================\n");
	printf("Initializing DVD... ");
	if(!di_initialize()) {
		printf("FAILURE\n");
		goto end_test;
	}else{
		printf("SUCCESS\n");
		pass++;
	}
	if(di_cover_open()) {
		printf("Please insert a disc.\n");
		while(di_cover_open()) usleep(100);
	}

	printf("Resetting drive... \n");
	di_reset(TRUE);
	usleep(1150*1000);
	/* Spin up the drive motor and force it to accept the disc. */
	irq_di_register_handler(IRQ_DI_BREAK, transfer_complete, NULL);
	irq_di_register_handler(IRQ_DI_TC, transfer_complete, NULL);
	irq_di_register_handler(IRQ_DI_ERROR, drive_error, NULL);
	irq_di_register_handler(IRQ_DI_COVER, transfer_complete, NULL);
	irq_di_enable(IRQ_DI_BREAK);
	irq_di_enable(IRQ_DI_TC);
	irq_di_enable(IRQ_DI_ERROR);
	irq_di_enable(IRQ_DI_COVER);
	printf("IRQs set\n");
	drive_unlock();
	printf("Drive unlocked\n");
	data = memalign(32, 0x20);
	di_set_command(DI_CMD_DMAREAD, DI_DMAREAD_DISCID, 0, 0, 0x20);
	di_set_dma((u32)data, 0x20);
	printf("Disc ID\n");
	di_transfer(FALSE, TRUE);
	timer = 0;
	while(!di_transfer_ended() && (timer < MAX_TIMEOUT)) {
		timer++;
		usleep(100);
	}
	if(timer == MAX_TIMEOUT) {
		printf("TIMED OUT\n");
		goto end_test;
	}else{
		pass++;
		printf("DONE!\n");
	}
	printf("DiscID: %s\n", data);
	free(data);

	di_set_command(DI_CMD_DEBUG, DI_DEBUG_DRVCONTROL,
			DI_MOTOR_UP|DI_DRIVE_ACCEPT, 0, 0);
	printf("Commands set\n");
	completed_transfer = 0;
	di_transfer(FALSE, FALSE);
	printf("\"Transferred\"\n");
	timer = 0;
	while(!di_transfer_ended() && (timer < MAX_TIMEOUT)) {
		timer++;
		usleep(100);
	}
	if(timer == MAX_TIMEOUT) {
		printf("TIMED OUT\n");
		goto end_test;
	}else{
		pass++;
		printf("DONE!\n");
	}

	printf("Reading 2048 bytes... ");
	/* Reading 2048 bytes from 0 */
	data = memalign(32, 2048);
	di_set_command(DI_CMD_DMAREAD, DI_DMAREAD_SECTOR, 0, 0, 2048);
	di_set_dma((u32)data, 2048);
	completed_transfer = 0;
	di_transfer(FALSE, TRUE);
	timer = 0;
	while(!di_transfer_ended() && (timer < MAX_TIMEOUT)) {
		timer++;
		usleep(100);
	}
	if(timer == MAX_TIMEOUT) {
		printf("TIMED OUT\n");
		goto end_test;
	}else{
		pass++;
		printf("DONE!\n");
	}
	
	printf("Game is: %s\n", data);
end_test:
	printf("\n%d/4 test passed.\n", pass);
	printf("===============================\n");
	usleep(5000000);

	return 0;
}

