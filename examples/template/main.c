/*
        template -- A template mini-broadway application

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>

void *xfb = NULL;

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}
	
	printf("===============================\n");
	printf("Hello world!\nPress start to exit.\n");
	gc_pad_t pad0;
	for(;;) {
		pad_read(&pad0, GCPAD_0);
		/* Exit if start pressed. */
		if(pad0.btns_down & PAD_START) {
			break;
		}
		VIDEO_WaitVSync();
	}
	printf("===============================\n");

	/* If we have a loader, return to it; otherwise shut off the Wii. */
	return 0;
}

