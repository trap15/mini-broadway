/*
        template -- A template mini-broadway application

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>

void *xfb = NULL;

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	xfb = memalign(32, 640 * (480 + y_add) * 2);
	memset(xfb, 0, 640 * (480 + y_add) * 2);
	console_simple_init(vmode, xfb);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	
	printf("===============================\n");
	u32 geckoid = gecko_getid();
	int geckoalive = gecko_isalive();
	printf("Hello world!\n");
	printf("Gecko Status:\nAlive: %d\nID: %08X\n", geckoalive, geckoid);
	printf("Testing sending: 'FooBarBaz\\n'\n");
	u32 geckorets[11];
	geckorets[0] = gecko_command(0xB0000000 | ('F'<<20));
	geckorets[1] = gecko_command(0xB0000000 | ('o'<<20));
	geckorets[2] = gecko_command(0xB0000000 | ('o'<<20));
	geckorets[3] = gecko_command(0xB0000000 | ('B'<<20));
	geckorets[4] = gecko_command(0xB0000000 | ('a'<<20));
	geckorets[5] = gecko_command(0xB0000000 | ('r'<<20));
	geckorets[6] = gecko_command(0xB0000000 | ('B'<<20));
	geckorets[7] = gecko_command(0xB0000000 | ('a'<<20));
	geckorets[8] = gecko_command(0xB0000000 | ('z'<<20));
	geckorets[9] = gecko_command(0xB0000000 | ('\n'<<20));
	geckorets[10] = gecko_command(0xB0000000 | (0<<20));
	for(i = 0; i < 11; i++) {
		printf("Ret%d: %08X\n", i, geckorets[i]);
	}
	gc_pad_t pad0;
	for(;;) {
		pad_read(&pad0, GCPAD_0);
		/* Exit if start pressed. */
		if(pad0.btns_down & PAD_START) {
			break;
		}
		VIDEO_WaitVSync();
	}
	printf("===============================\n");

	/* If we have a loader, return to it; otherwise shut off the Wii. */
	return 0;
}

