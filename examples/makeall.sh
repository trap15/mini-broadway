#!/bin/sh
SCRIPTDIR=`dirname $PWD/$0`
die() {
	echo $@
	exit 1
}
build() {
	(echo "****************\nBuilding $@ example!" && cd $SCRIPTDIR/$@ && make) || die "Can't build $@ example"
}
clean() {
	(echo "****************\nCleaning $@ example!" && cd $SCRIPTDIR/$@ && make clean) || die "Can't clean $@ example"
}
(cd $SCRIPTDIR/tests && ./makeall.sh $1) || die "Can't build tests"
if [ "$1" = "clean" ] || [ "$1" = "rebuild" ] ; then
	clean template
	clean graphics/framebuffer
	clean exitester
fi
if [ "$1" != "clean" ] ; then
	build template
	build graphics/framebuffer
	build exitester
fi
cd $SCRIPTDIR

