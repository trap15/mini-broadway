/*
        framebuffer -- An example showing how to use the framebuffer

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>

#include <broadway.h>
#include <video_low.h>
#include <console.h>

u32 *xfb[2] = { NULL, NULL };
int curr_xfb = 0;

int main(int argc, char *argv[])
{
	int i;
	int y_add;
	int vmode = -1;

	(void)argc;
	(void)argv;
	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			y_add = 48;
			break;
	}
	/* Set up a double-buffered configuration */
	xfb[0] = memalign(32, 320 * (480 + y_add) * 4);
	memset(xfb[0], 0, 320 * (480 + y_add) * 4);
	xfb[1] = memalign(32, 320 * (480 + y_add) * 4);
	memset(xfb[1], 0, 320 * (480 + y_add) * 4);
	console_simple_init(vmode, xfb[curr_xfb]);
	VIDEO_Init(vmode);
	VIDEO_SetFrameBuffer(xfb[curr_xfb]);
	VISetupEncoder();
	
	u32 version = ipc_getvers();
	u16 mini_version_major = version >> 16 & 0xFFFF;
	u16 mini_version_minor = version & 0xFFFF;
	gecko_printf("Mini version: %d.%0d\n", mini_version_major, mini_version_minor);
	gecko_printf("Hollywood chipset version: %d rev%d\n", hollywood_version(), hollywood_revision());
	for(i = 0; i < 10; i++) {
		slotled_change();
		usleep(10000);
	}
	if(version < MINIMUM_MINI_VERSION) {
		gecko_printf("Sorry, this version of MINI (armboot.bin)\n"
			   "is too old, please update to at least %d.%0d.\n", 
			   (MINIMUM_MINI_VERSION >> 16), (MINIMUM_MINI_VERSION & 0xFFFF));
		/* Here's a better idea:
		 * Return because our crt0 can handle it :)
		 */
		return 1;
	}
	
	gc_pad_t pad0;
	/* Since our YUV framebuffer isn't very friendly for pixel-level manipulation,
	 * let's run a fake XRGB8 and convert on-the-fly
	 */
	u32* fakefb = malloc(640 * (480 + y_add));
	/* CGA colors :3 */
	const u32 colors[16] = {
		0x000000, 0x0000AA, 0x00AA00, 0x00AAAA,
		0xAA0000, 0xAA00AA, 0xAA5500, 0xAAAAAA,
		0x555555, 0x5555FF, 0x55FF55, 0x55FFFF,
		0xFF5555, 0xFF55FF, 0xFFFF55, 0xFFFFFF
	};
	memset(fakefb, 0, 640 * (480 + y_add) * 4);
	int drawx = 0, drawy = 0, colidx = 1;
	for(;;) {
		pad_read(&pad0, GCPAD_0);
		u16 btns = pad0.btns_down | pad0.btns_held;
		/* Exit if start pressed. */
		if(btns & PAD_START) {
			break;
		}
		if(btns & PAD_LEFT) {
			if(drawx > 0) drawx--;
		}else if(btns & PAD_RIGHT) {
			if(drawx < 640-1) drawx++;
		}
		if(btns & PAD_UP) {
			if(drawy > 0) drawy--;
		}else if(btns & PAD_DOWN) {
			if(drawy < 480+y_add-1) drawy++;
		}
		if(btns & PAD_A) {
			fakefb[drawx + (drawy * 640)] = colors[colidx];
		}else if(btns & PAD_B) {
			fakefb[drawx + (drawy * 640)] = colors[0];
		}
		if(btns & PAD_X) {
			colidx++;
			if(colidx >= 16) colidx = 1;
		}else if(btns & PAD_Y) {
			colidx--;
			if(colidx < 1) colidx = 15;
		}
		int x,y;
		for(x = 0; x < 320; x++) {
			for(y = 0; y < 480+y_add; y++) {
				u32 col[2];
				col[0] = fakefb[(x << 1) +     ((y << 9) + (y << 7))];
				col[1] = fakefb[(x << 1) + 1 + ((y << 9) + (y << 7))];
				/* Can't trust the compiler to optimize the multiplications, so do the equivalent shifts */
				xfb[curr_xfb][((y << 8) + (y << 6)) + x] =
					make_yuv(col[0] >> 16, col[0] >> 8, col[0], col[1] >> 16, col[1] >> 8, col[1]);
			}
		}
		VIDEO_WaitVSync();
		VIDEO_SetFrameBuffer(xfb[curr_xfb]);
		curr_xfb ^= 1;
		console_set_framebuffer(xfb[curr_xfb]);
	}
	printf("===============================\n");

	/* If we have a loader, return to it; otherwise shut off the Wii. */
	return 0;
}

