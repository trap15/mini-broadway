/*
	ppcskel - a Free Software replacement for the Nintendo/BroadOn bootloader.
	hid driver

Copyright (C) 2009     Bernhard Urban <lewurm@gmx.net>
Copyright (C) 2009     Sebastian Falbesoner <sebastian.falbesoner@gmail.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <hextwelve/core/core.h>
#include <hextwelve/core/usb.h>
#include <hextwelve/usbspec/usb11spec.h>
#include <malloc.h>
#include <string.h>

#include <hextwelve/drivers/class/hid.h>

struct usb_driver hidkb = {
	.name	  = "hidkb",
	.probe  = usb_hidkb_probe,
	.check  = usb_hidkb_check,
	.remove = usb_hidkb_remove,
	.data	  = NULL
};

/*
 * just using two very simple US layout code translation tables that
 * are sufficient for providing a getc() C standard library call;
 * the only non-printable character here in this table is ESCAPE which
 * has index 0x29 and is zero
 */
unsigned char code_translation_table[2][57] = {
	{ /* unshifted */
	 0,   0,   0,   0,  'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
	'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2',
	'3', '4', '5', '6', '7', '8', '9', '0', '\n', 0,  '\r','\t',' ', '-', '=', '[',
	']', '\\','\\',';', '\'','`', ',', '.', '/'
	},
	{ /* shifted */
	 0,   0,   0,   0,  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
	'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '@',
	'#', '$', '%', '^', '&', '*', '(', ')', '\n', 0,  '\r','\t',' ', '_', '+', '{',
	'}', '|', '|', ':', '\"','~', '<', '>', '?'
	}
};

static char ascii(char s)
{
	if(s < 0x20) return '.';
	if(s > 0x7E) return '.';
	return s;
}
static void hexdump(void *d, int len)
{
	u8 *data;
	int i, off;
	data = (u8*)d;
	for(off = 0; off < len; off += 16) {
		gecko_printf("%08x  ",off);
		for(i = 0; i < 16; i++)
			if((i + off) >= len) gecko_printf("   ");
			else gecko_printf("%02x ", data[off + i]);

		gecko_printf(" ");
		for(i = 0; i < 16; i++)
			if((i + off) >= len) gecko_printf(" ");
			else gecko_printf("%c", ascii(data[off + i]));
		gecko_printf("\n");
	}
}

void usb_hidkb_init()
{
	usb_register_driver(&hidkb);
}

void usb_hidkb_probe()
{
	struct usb_device *dev;
	struct element *iterator = core.devices->head;
	
	while(iterator != NULL) {
		dev = (struct usb_device*)iterator->data;
		if(dev == NULL) {
			continue;
		}

		if(dev->conf->intf->bInterfaceClass == HID_CLASSCODE &&
				dev->conf->intf->bInterfaceSubClass == 1 && /* keyboard support boot protocol? */
				dev->conf->intf->bInterfaceProtocol == 1) { /* keyboard? */
			gecko_printf("Identified device at 0x%08X as HIDKB\n", dev);
			hidkb.data = (void*) dev;
			gecko_printf("Setting idle\n");
			usb_hidkb_set_idle(dev, 1);
			gecko_printf("Set idle\n");
		}

		iterator=iterator->next;
	}
}

void usb_hidkb_set_idle(struct usb_device *dev, u8 duration) {
#define SET_IDLE 0x0A
	u8 buf[8];
	memset(buf, 0, 8);
	usb_control_msg(dev, 0x21, SET_IDLE, (duration << 8), 0, 0, buf, 0);
	hexdump((void*) buf, 8);
}

void usb_hidkb_check()
{
}

u8 usb_hidkb_inuse()
{
	gecko_printf("kbdata: %s\n", hidkb.data ? "YES" : "NO");
	return hidkb.data ? 1 : 0;
}

void usb_hidkb_remove() {
	hidkb.data = NULL;
}

struct kbrep *usb_hidkb_getChars() {
	struct usb_device *dev = (struct usb_device*) hidkb.data;
	struct kbrep *ret = (struct kbrep*) malloc(sizeof(struct kbrep));

	memset(ret, 0, 8);
	s8 epnum = dev->conf->intf->endp->bEndpointAddress & 0xf;
	(void) usb_interrupt_read(dev, epnum, (u8*) ret, 8, 0);
#if 0
	gecko_printf("============\nusb_interrupt_read:\n");
	hexdump((void*)ret, 8);
#endif
	return ret;
}

unsigned char usb_hidkb_get_char_from_keycode(u8 keycode, int shifted)
{
	unsigned char result = 0;
	if (keycode >= 0x3 && keycode < 57) {
		result = code_translation_table[!!shifted][keycode];
	}
	return result;
}
