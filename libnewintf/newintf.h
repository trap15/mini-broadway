/*
	libnewintf -- Interfacing to Newlib for mini-broadway
	Newlib interfacing

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Newlib interfacing. Most important part is newlib_device_t, which is the
 * equivalent of devoptab's for devkitPro.
 * \todo Make newlib handles into linked lists.
 * \todo Unstubbify environment variables.
 * \todo Unstubbify times.
 * \todo Possible unstubbify threading calls with libthreads (many reasons to not, however)
 */

#ifndef __NEWINTF_H__
#define __NEWINTF_H__

#include <sys/stat.h>
#include <types.h>

/*! The interface between newlib and any user devices to be accessed with stdio.
 */
typedef struct {
	int	valid;	/*!< Don't touch this */
	int	ident;	/*!< Identifier (Don't touch this) */
	char	tag[8]; /*!< The tag for the device filenames (tag:/path/to/file) */
	BOOL	is_tty;	/*!< TRUE if this device is a TTY, FALSE otherwise */

	void*	(*open)(struct _reent* r, const char *name, int flags, int mode);	/*!< open() */
	int	(*close)(struct _reent* r, int fd);					/*!< close() */
	off_t	(*lseek)(struct _reent* r, int fd, off_t pos, int whence);		/*!< lseek() */
	long	(*read)(struct _reent* r, int fd, void* buf, size_t cnt);		/*!< read() */
	long	(*write)(struct _reent* r, int fd, const void* buf, size_t cnt);	/*!< write() */
	int	(*stat)(struct _reent* r, const char *file, struct stat* pstat);	/*!< stat() */
	int	(*fstat)(struct _reent* r, int fd, struct stat* pstat);			/*!< fstat() */
	int	(*link)(struct _reent* r, const char *old, const char *n);		/*!< link() */
	int	(*unlink)(struct _reent* r, const char *file);				/*!< unlink() */
} newlib_device_t;

/*! \brief Initializes the Newlib abstraction
 *
 * \return Returns TRUE if the init was successful. FALSE otherwise.
 */
BOOL newlib_init(void);
/*! \brief Finishes the Newlib abstraction
 */
void newlib_fini(void);
/*! \brief Adds a device to the file I/O abstraction.
 *
 * \param dev a newlib_device_t structure containing device information.
 * \return Returns TRUE if the device was successfully added. FALSE otherwise.
 */
BOOL newlib_add_device(newlib_device_t dev);
/*! \brief Removes a device from the file I/O abstraction.
 *
 * \param tag the tag of the device to remove from the abstraction.
 * \return Returns TRUE if the device was successfully removed. FALSE otherwise.
 */
BOOL newlib_remove_device(char tag[8]);
/*! \brief Gets the structure for a device from the file I/O abstraction.
 *
 * \param tag the tag of the device to get the structure of.
 * \return Returns a pointer to the structure on success, or NULL on failure.
 */
newlib_device_t* newlib_get_device(char tag[8]);
/*! \brief Sets the default device to use when no tag is specified
 *
 * \param tag the tag of the device to use by default.
 * \return Returns TRUE on success, FALSE on failure.
 */
BOOL newlib_set_default_device(char tag[8]);

#endif

