/*
	libnewintf -- Interfacing to Newlib for mini-broadway
	Newlib interfacing

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <types.h>
#include <broadway.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/times.h>

#include "newintf.h"

#undef errno
extern int errno;

typedef struct {
	int	valid;	/* Don't touch this */
	int	ident;	/* Identifier */
	int	dev;	/* Device specifier */
	void*	data;	/* Device-specific data */
} newlib_handle_t;

/* TODO: Making these linked lists wouldn't be a bad idea */
#define HANDLE_COUNT_MAX		(1024)
static newlib_handle_t _handles[HANDLE_COUNT_MAX];
static int _newlib_handle_id = 0;
#define DEVICE_COUNT_MAX		(64)
static newlib_device_t _devices[DEVICE_COUNT_MAX];
static char _default_device_tag[8] = "\0";
#define DEVFILECALL(dev, func, args...)	do { \
	go = 1; \
	if(!_devices[dev].valid) \
		go = 0; \
	else if(_devices[dev].func == NULL) \
		go = 0; \
	else \
		ret = (void*)_devices[dev].func(args); \
} while(0)
#define DEVICECALL(f, func, args...)	do { \
	go = 1; \
	if(!_handles[f].valid) \
		go = 0; \
	else if(_handles[f].dev < 0) \
		go = 0; \
	else \
		DEVFILECALL(_handles[f].dev, func, args); \
} while(0)

/* TODO: Unstubbify */
static char *__env[1] = { 0 };
char **eviron = __env;

int _strncmp(const char *s1, const char *s2, size_t n)
{
	size_t i;
	for(i = 0; (i < n) && (s1[i]) && (s1[i] == s2[i]); i++);
	if(i == n)
		return 0;
	return s1[i] - s2[i];
}

char* _strncpy(char *s1, const char *s2, size_t len)
{
	size_t i;
	for(i = 0; (s1[i] = s2[i]) && (i < len); i++);
	return s1;
}

BOOL newlib_init(void)
{
	int i;
	for(i = 0; i < DEVICE_COUNT_MAX; i++) {
		_devices[i].valid = 0;
		_devices[i].ident = -1;
		_devices[i].open = NULL;
		_devices[i].close = NULL;
		_devices[i].lseek = NULL;
		_devices[i].read = NULL;
		_devices[i].write = NULL;
		_devices[i].stat = NULL;
		_devices[i].fstat = NULL;
		_devices[i].link = NULL;
		_devices[i].unlink = NULL;
	}
	for(i = 0; i < HANDLE_COUNT_MAX; i++) {
		_handles[i].valid = 0;
		_handles[i].dev = -1;
		_handles[i].data = NULL;
	}
	_handles[0].valid = _handles[1].valid = _handles[2].valid = 1;
	_handles[0].dev = _handles[1].dev = _handles[2].dev = -1;
	_handles[0].ident = _newlib_handle_id++;
	_handles[1].ident = _newlib_handle_id++;
	_handles[2].ident = _newlib_handle_id++;
	return TRUE;
}

void newlib_fini(void)
{
	int i;
	for(i = 0; i < DEVICE_COUNT_MAX; i++) {
		_devices[i].valid = 0;
		_devices[i].ident = -1;
		_devices[i].open = NULL;
		_devices[i].close = NULL;
		_devices[i].lseek = NULL;
		_devices[i].read = NULL;
		_devices[i].write = NULL;
		_devices[i].stat = NULL;
		_devices[i].fstat = NULL;
		_devices[i].link = NULL;
		_devices[i].unlink = NULL;
	}
	for(i = 0; i < HANDLE_COUNT_MAX; i++) {
		_handles[i].valid = 0;
		_handles[i].ident = 0;
		_handles[i].dev = -1;
		if(_handles[i].data) {
			free(_handles[i].data);
			_handles[i].data = NULL;
		}
	}
}

BOOL newlib_add_device(newlib_device_t dev)
{
	int i;
	newlib_remove_device(dev.tag);
	for(i = 0; i < DEVICE_COUNT_MAX; i++) {
		if(_devices[i].valid)
			continue;
		_strncpy(_devices[i].tag, dev.tag, 8);
		if(_strncmp(dev.tag, "stdin", 8) == 0) {
			_handles[0].dev = i;
			_handles[0].valid = 1;
		}else if(_strncmp(dev.tag, "stdout", 8) == 0) {
			_handles[1].dev = i;
			_handles[1].valid = 1;
		} else if(_strncmp(dev.tag, "stderr", 8) == 0) {
			_handles[2].dev = i;
			_handles[2].valid = 1;
		}
		_devices[i].is_tty = dev.is_tty;
		_devices[i].open = dev.open;
		_devices[i].close = dev.close;
		_devices[i].lseek = dev.lseek;
		_devices[i].read = dev.read;
		_devices[i].write = dev.write;
		_devices[i].stat = dev.stat;
		_devices[i].fstat = dev.fstat;
		_devices[i].link = dev.link;
		_devices[i].unlink = dev.unlink;
		_devices[i].ident = i;
		_devices[i].valid = 1;
		break;
	}
	if(i == DEVICE_COUNT_MAX)
		return FALSE;
	return TRUE;
}

BOOL newlib_remove_device(char tag[8])
{
	int i;
	for(i = 0; i < DEVICE_COUNT_MAX; i++) {
		if(!_devices[i].valid)
			continue;
		if(_strncmp(_devices[i].tag, tag, 8) != 0)
			continue;
		_devices[i].valid = 0;
		memset(_devices[i].tag, 0, 8);
		_devices[i].ident = -1;
		_devices[i].open = NULL;
		_devices[i].close = NULL;
		_devices[i].lseek = NULL;
		_devices[i].read = NULL;
		_devices[i].write = NULL;
		_devices[i].stat = NULL;
		_devices[i].fstat = NULL;
		_devices[i].link = NULL;
		_devices[i].unlink = NULL;
		break;
	}
	if(i == DEVICE_COUNT_MAX)
		return FALSE;
	return TRUE;
}

newlib_device_t* newlib_get_device(char tag[8])
{
	int i;
	for(i = 0; i < DEVICE_COUNT_MAX; i++) {
		if(!_devices[i].valid)
			continue;
		if(_strncmp(_devices[i].tag, tag, 8) != 0)
			continue;
		return &(_devices[i]);
	}
	return NULL;
}

static newlib_device_t* get_device(char* path)
{
	int i, seploc = 0;
	char tag[8];
	for(i = 0; i < 9; i++) {
		if(path[i] == ':')
			seploc = i;
	}
	if(seploc == 0)
		return newlib_get_device(_default_device_tag);
	else{
		_strncpy(tag, path, seploc);
		return newlib_get_device(tag);
	}
}

BOOL newlib_set_default_device(char tag[8])
{
	_strncpy(_default_device_tag, tag, 8);
	return TRUE;
}

static int _newlib_add_handle(newlib_handle_t hnd)
{
	int i;
	for(i = 0; i < HANDLE_COUNT_MAX; i++) {
		if(_handles[i].valid)
			continue;
		_handles[i] = hnd;
		_handles[i].ident = _newlib_handle_id++;
		_handles[i].valid = 1;
		break;
	}
	if(i == HANDLE_COUNT_MAX)
		return -1;
	return i;
}

static BOOL _newlib_remove_handle(int i)
{
	if(!_handles[i].valid)
		return FALSE;
	_handles[i].valid = 0;
	_handles[i].ident = 0;
	_handles[i].dev = -1;
	if(_handles[i].data) {
		free(_handles[i].data);
		_handles[i].data = NULL;
	}
	return TRUE;
}


int _open_r(struct _reent* r, const char *name, int flags, int mode)
{
	newlib_handle_t hnd;
	newlib_device_t* dev;
	int ret;
	dev = get_device((char*)name);
	hnd.dev = dev->ident;
	hnd.data = dev->open(r, name, flags, mode);
	if(hnd.data == NULL)
		return -1;
	if((ret = _newlib_add_handle(hnd)) < 0)
		return -1;
	return ret;
}

int _open64_r(struct _reent* r, const char *name, int flags, int mode)
{
	return _open_r(r, name, flags, mode);
}

int open(const char *name, int flags, int mode)
{
	return _open_r(_REENT, name, flags, mode);
}

int close(int file)
{
	int go;
	void* ret;
	DEVICECALL(file, close, _REENT, (int)_handles[file].data);
	if(go) {
		_newlib_remove_handle(file);
		return (int)ret;
	}
	return -1;
}

int lseek(int file, int ptr, int dir)
{
	int go;
	void* ret;
	DEVICECALL(file, lseek, _REENT, (int)_handles[file].data, ptr, dir);
	if(go)
		return (int)ret;
	return 0;
}

int _lseek64_r(struct _reent* r, int file, int ptr, int dir)
{
	(void)r;
	return lseek(file, ptr, dir);
}

int read(int file, char *ptr, int len)
{
	int go;
	void* ret;
	DEVICECALL(file, read, _REENT, (int)_handles[file].data, ptr, len);
	if(go)
		return (int)ret;
	return 0;
}

int write(int file, char *ptr, int len)
{
	int go;
	void* ret;
	DEVICECALL(file, write, _REENT, (int)_handles[file].data, ptr, len);
	if(go)
		return (int)ret;
	return 0;
}

/* TODO: Unstubbify */
int _fork_r(struct _reent* r)
{
	r->_errno = EAGAIN;
	return -1;
}

int fork(void)
{
	return _fork_r(_REENT);
}

/* TODO: Unstubbify */
int _wait_r(struct _reent* r, int *status)
{
	(void)status;
	r->_errno = ECHILD;
	return -1;
}

int wait(int *status)
{
	return _wait_r(_REENT, status);
}

int _stat_r(struct _reent* r, const char *file, struct stat *st)
{
	int go;
	void* ret;
	newlib_device_t* dev;
	dev = get_device((char*)file);
	DEVFILECALL(dev->ident, stat, r, file, st);
	if(go)
		return (int)ret;
	return 0;
}

int stat(const char *file, struct stat *st)
{
	return _stat_r(_REENT, file, st);
}

int fstat(int file, struct stat *st)
{
	int go;
	void* ret;
	DEVICECALL(file, fstat, _REENT, (int)_handles[file].data, st);
	if(go)
		return (int)ret;
	return 0;
}

int _fstat64_r(struct _reent* r, int file, struct stat *st)
{
	(void)r;
	return fstat(file, st);
}

int _link_r(struct _reent* r, char *old, char *new)
{
	int go;
	void* ret;
	newlib_device_t* dev;
	dev = get_device(old);
	if(dev != get_device(new)) /* Not the same device */
		return 0;
	DEVFILECALL(dev->ident, link, r, old, new);
	if(go)
		return (int)ret;
	return 0;
}

int link(char *old, char *new)
{
	return _link_r(_REENT, old, new);
}

int _unlink_r(struct _reent* r, char *name)
{
	int go;
	void* ret;
	newlib_device_t* dev;
	dev = get_device(name);
	DEVFILECALL(dev->ident, unlink, r, name);
	if(go)
		return (int)ret;
	return 0;
}

int unlink(char *name)
{
	return _unlink_r(_REENT, name);
}

caddr_t sbrk(int incr)
{
	extern unsigned int _sbrk_start, _sbrk_end;
	static unsigned int heap_end = 0;
	unsigned int prev_heap_end;
	if(heap_end == 0)
		heap_end = (unsigned int)&_sbrk_start;
	if(incr < 0) {
		errno = ENOMEM;
		return (caddr_t)-1;
	}
	if((heap_end + incr) > (unsigned int)&_sbrk_end) {
		errno = ENOMEM;
		return (caddr_t)-1;
	}
	prev_heap_end = heap_end;
	heap_end += incr;
	return (caddr_t)prev_heap_end;
}

/* TODO: Unstubbify */
int getpid(void)
{
	return 1;
}

/* TODO: Unstubbify */
int execve(char *name, char **argv, char **env)
{
	(void)name;
	(void)argv;
	(void)env;
	errno = ENOMEM;
	return -1;
}

int isatty(int file)
{
	if(!_handles[file].valid)
		return 0;
	if(!_devices[_handles[file].dev].valid)
		return 0;
	return _devices[_handles[file].dev].is_tty;
}

/* TODO: Unstubbify */
int kill(int pid, int sig)
{
	(void)pid;
	(void)sig;
	errno = EINVAL;
	return -1;
}

/* TODO: Unstubbify */
clock_t times(struct tms *buf)
{
	(void)buf;
	return -1;
}

