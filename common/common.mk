AR	  = $(PREFIX)ar
AS	  = $(PREFIX)as
DSPAS	  = $(WIIDEV)/bin/dsptool
CC	  = $(PREFIX)gcc
CXX	  = $(PREFIX)g++
LD	  = $(PREFIX)gcc
OBJCOPY	  = $(PREFIX)objcopy
RANLIB	  = $(PREFIX)ranlib
STRIP	  = $(PREFIX)strip
ASFLAGS	 += -D_LANGUAGE_ASSEMBLY

ifeq ($(NOMAPFILE),)
LDFLAGS	 += -Wl,-Map,$(TARGET).map
endif

ifneq ($(LDSCRIPT),)
LDFLAGS	 += -Wl,-T$(LDSCRIPT)
endif

DEPDIR	  = .deps

CFLAGS	 += $(INCLUDES)

ifeq ($(MAKING_LIBBROADWAY),)
CFLAGS	 += -I$(WIIDEV)/include -I$(WIIDEV)
LDFLAGS	 += -L$(WIIDEV)/lib
endif

.PHONY: all clean upload

all: $(OBJS) $(NOLINKOBJS) $(TARGET)

%.elf: $(OBJS) $(NOLINKOBJS)
	@echo "  LINK      $@"
	@$(LD) $(LDFLAGS) $(OBJS) $(LIBS) $(SYSLIBS) -o $@

%.a: $(OBJS) $(NOLINKOBJS)
	@echo "  ARCHIVE   $@"
	@$(AR) rcs "$@" $(OBJS)

ifneq ($(LDSCRIPT),)
$(TARGET): $(LDSCRIPT)
endif

%.o: %.c
	@echo "  COMPILE   $<"
	@mkdir -p $(DEPDIR)
	@$(CC) $(CFLAGS) $(DEFINES) -Wp,-MMD,$(DEPDIR)/$(*F).d,-MQ,"$@",-MP -c $< -o $@

%.dsp.bin: %.ds
	@echo "  ASSEMBLE  $<"
	@$(DSPAS) $(DSPSFLAGS) -o $@ $<

%.dsp.bin: %.DS
	@echo "  ASSEMBLE  $<"
	@$(DSPAS) $(DSPSFLAGS) -o $@ $<

%.dsp: %.dsp.bin
	$(bin2o)

%.o: %.s
	@echo "  ASSEMBLE  $<"
	@$(CC) $(CFLAGS) $(DEFINES) $(ASFLAGS) -c $< -o $@

%.o: %.S
	@echo "  ASSEMBLE  $<"
	@$(CC) $(CFLAGS) $(DEFINES) $(ASFLAGS) -c $< -o $@

clean::
	rm -rf $(DEPDIR)
	rm -f $(TARGET) $(TARGET).map $(OBJS) $(NOLINKOBJS) $(CLEAN)

ifeq ($(ARCH),ppc)
upload: $(TARGET)
	@$(WIIDEV)/bin/bootmii -p $<
endif
ifeq ($(ARCH),arm)
upload: $(TARGET)
	@$(WIIDEV)/bin/bootmii -a $<
endif

ifeq ($(ARCH),ppc)
_ELFTARGET = powerpc
endif
ifeq ($(ARCH),arm)
_ELFTARGET = arm
endif

define bin2o
	@echo "  BIN2O     $<"
	@$(OBJCOPY) -I binary -O elf32-$(_ELFTARGET) --binary-architecture $(_ELFTARGET) $< $@
	@echo "#ifndef _BINARY_INCLUDE_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_" > `(echo $(<F) | tr . _)`.h
	@echo "#define _BINARY_INCLUDE_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_" >> `(echo $(<F) | tr . _)`.h
	@echo "extern const u32 _binary_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_start;" >> `(echo $(<F) | tr . _)`.h
	@echo "extern const u32 _binary_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_end;" >> `(echo $(<F) | tr . _)`.h
	@echo "extern const u32 _binary_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_size;" >> `(echo $(<F) | tr . _)`.h
	@echo "#define "`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_size ((u32)&_binary_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_size)" >> `(echo $(<F) | tr . _)`.h
	@echo "#define "`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`" ((u8*)&_binary_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_start)" >> `(echo $(<F) | tr . _)`.h
	@echo "#define "`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_end ((u8*)&_binary_"`(echo $(<F) | sed -e 's/^\([0-9]\)/_\1/' | tr . _)`"_end)" >> `(echo $(<F) | tr . _)`.h
	@echo "#endif" >> `(echo $(<F) | tr . _)`.h
endef

-include $(DEPDIR)/*

