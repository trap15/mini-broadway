ifeq ($(strip $(WIIDEV)),)
$(error "Set WIIDEV in your environment.")
endif

PREFIX	 = $(WIIDEV)/bin/powerpc-elf-

ARCH	 = ppc
CFLAGS	+= -mcpu=750 -mpaired -m32 -mhard-float -mno-eabi -mno-sdata
CFLAGS	+= -ffreestanding -ffunction-sections 
CFLAGS	+= -Wall -Wextra -pipe -DBM_BROADWAY -DBOOTMII
CFLAGS	+= -g -fdata-sections
ASFLAGS	+= 
LDFLAGS	+= -mcpu=750 -m32 -n -nostartfiles -nodefaultlibs -Wl,-gc-sections
LDSCRIPT = $(WIIDEV)/ppc.ld
SYSLIBS	 = -lc -lnewintf -lgcc
LIBS	 = 

ifeq ($(MAKING_LIBBROADWAY),)
LDFLAGS += $(WIIDEV)/powerpc-elf/lib/realmode.o
else
LDSCRIPT = ../common/ppc.ld
endif

