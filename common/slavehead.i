/*
	Slave general header

Copyright (C) 2010-2011		Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef _SLAVEHEAD_I_
#define _SLAVEHEAD_I_

#define SWI_LIBC		(0x000000)
#define SWI_SYSTEM		(0x010000)

#define swiMemset		(SWI_LIBC   | 0x0000)
#define swiMemcpy		(SWI_LIBC   | 0x0001)
#define swiMemcmp		(SWI_LIBC   | 0x0002)
#define swiWatchdogKick		(SWI_SYSTEM | 0x0000)
#define swiEnd			(SWI_SYSTEM | 0x0001)
#define swiGeckoPrint		(SWI_SYSTEM | 0x0002)

#endif

