/*
	diskio.c -- glue interface to ElmChan FAT FS driver. Part of the
	BootMii project.
 
Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2008, 2009	Sven Peter <svenpeter@gmail.com>
Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>
#include <string.h>

/* Our stuff. */
#include <diskmii.h>
#include <diskmii/ff.h>
#include <diskmii/diskio.h>
#include <diskmii/cache.h>

int SD_disk_deinitialize();

int SD_disk_initialize();

int SD_disk_status();

int SD_disk_read(BYTE *buff, DWORD sector, BYTE count);

int SD_disk_write(const BYTE *buff, DWORD sector, BYTE count);

int SD_disk_ioctl(BYTE ctrl, BYTE *buff);

BYTE buffer[512] ATTRIBUTE_ALIGN(32);

DRESULT get_results(int result)
{
	if(result == -1)
		return RES_PARERR;
	if(result == -2)
		return RES_ERROR;
	if(result == -3)
		return RES_WRPRT;
	if(result == -4)
		return RES_NOTRDY;
	return RES_OK;
}

DSTATUS get_stats(int result)
{
	if(result == -1)
		return STA_NOINIT;
	if(result == -2)
		return STA_NODISK;
	if(result == -3)
		return STA_PROTECT;
	return 0;
}

/*-----------------------------------------------------------------------*/
/* Deinitialize a Drive                                                  */
/* drv						Drive number.		 */

DSTATUS disk_deinitialize(BYTE drv)
{
	DSTATUS status;
	int result = -1;
	
	switch(drv) {
		case ELM_SD:
			result = SD_disk_deinitialize();
			break;
	}
	status = get_stats(result);
	return status;
}

int SD_disk_deinitialize()
{
	return 0;
}

/*-----------------------------------------------------------------------*/
/* Initialize a Drive                                                    */
/* drv						Drive number.		 */

DSTATUS disk_initialize(BYTE drv)
{
	DSTATUS status;
	int result = -1;

	switch(drv) {
		case ELM_SD:
			result = SD_disk_initialize();
			break;
		default:
			break;
	}
	status = get_stats(result);
	return status;
}

int SD_disk_initialize()
{
	int result = 0;
	int state = sd_get_state();
	switch(state) {
		case SDMMC_NO_CARD:
			result = -2;
			break;
		case SDMMC_NEW_CARD:
			if(sd_mount())
				result = -1;
			else
				result = 0;
			break;
		default:
			result = 0;
			break;
	}
	return result;
}

/*-----------------------------------------------------------------------*/
/* Return Disk Status                                                    */
/* drv						Drive number.		 */

DSTATUS disk_status(BYTE drv)
{
	DSTATUS status;
	int result = -1;

	switch(drv) {
		case ELM_SD:
			result = SD_disk_status();
			break;
	}
	status = get_stats(result);
	return status;
}

int SD_disk_status()
{
	int result = 0;
	int state = sd_get_state();
	
	switch(state) {
		case SDMMC_NO_CARD:
			result = -2;
			break;
		case SDMMC_NEW_CARD:
			result = -1;
			break;
		default:
			result = 0;
			break;
	}
	return result;
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/* drv						Drive number.		 */
/* buff						Data buffer.		 */
/* sector					Sector Address. (LBA)	 */
/* count					Sector Count. (1 - 255)	 */

DRESULT disk_read(BYTE drv, BYTE *buff, DWORD sector, BYTE count)
{
	DRESULT res;
	int result = -1;

	switch(drv) {
		case ELM_SD:
			result = SD_disk_read(buff, sector, count);
			break;
	}
	res = get_results(result);
	return res;
}

int SD_disk_read_core(BYTE *buff, DWORD sector, BYTE count)
{
	u32 i;
	int result = 0;
	
	if((count > 1) && (((u32)buff % 64) == 0)) {
		if(sd_read(sector, count, buff) != 0)
			result = -2;
		return result;
	}
	
	result = 0;
	for(i = 0; i < count; i++) {
		if(sd_read(sector + i, 1, buffer) != 0) {
			result = -2;
			break;
		}
		memcpy(buff + i * 512, buffer, 512);
	}
	return result;
}

int SD_disk_read(BYTE *buff, DWORD sector, BYTE count)
{
	int result = 0;
	if((count == 1) && (_libelmcache_read_from_cache(ELM_SD, sector, buff)))
		return RES_OK;
	if(count != 1)
		_libelmcache_invalidate_cache(ELM_SD, sector, count);
	result = SD_disk_read_core(buff, sector, count);
	if((result == 0) && (count == 1))
		_libelmcache_add_to_cache(ELM_SD, sector, buff);
	return result;
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/* drv						Drive number.		 */
/* buff						Data buffer.		 */
/* sector					Sector Address. (LBA)	 */
/* count					Sector Count. (1 - 255)	 */

#if _READONLY == 0
DRESULT disk_write(BYTE drv, const BYTE *buff, DWORD sector, BYTE count)
{
	DRESULT res;
	int result = -1;

	switch(drv) {
		case ELM_SD:
			result = SD_disk_write(buff, sector, count);
			break;
	}
	res = get_results(result);
	return res;
}

int SD_disk_write(const BYTE *buff, DWORD sector, BYTE count)
{
	u32 i;
	int result = 0;
	_libelmcache_invalidate_cache(ELM_SD, sector, count);
	if((count > 1) && (((u32)buff % 64) == 0)) {
		if(sd_write(sector, count, buff) != 0)
			result = -2;
		return result;
	}
	
	for(i = 0; i < count; i++) {
		memcpy(buffer, buff + i * 512, 512);
		if(sd_write(sector + i, 1, buffer) != 0) {
			result = -2;
			break;
		}
	}
	return result;
}

#endif /* _READONLY */

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/* drv						Drive number.		 */
/* ctrl						Control Code.		 */
/* buff						Data buffer.		 */

DRESULT disk_ioctl(BYTE drv, BYTE ctrl, void *buff)
{
	DRESULT res;
	int result = -1;

	switch(drv) {
		case ELM_SD:
			result = SD_disk_ioctl(ctrl, buff);
			break;
	}
	res = get_results(result);
	return res;
}

int SD_disk_ioctl(BYTE ctrl, BYTE *buff)
{
	u32 *buff_u32 = (u32*)buff;
	int result = 0;
	
	switch (ctrl) {
		case CTRL_SYNC:
			break;
		case GET_SECTOR_COUNT:
			*buff_u32 = sd_getsize();
			break;
		case GET_SECTOR_SIZE:
			*buff_u32 = 512;
			break;
		case GET_BLOCK_SIZE:
			*buff_u32 = 512;
			break;
		default:
			result = -1;
			break;
	}
	return result;
}

DWORD get_fattime()
{
	return 0;
}

