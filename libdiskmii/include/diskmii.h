/*
	libdiskmii - a disk input/output library for the BootMii platform.

Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Control of the diskmii interface. Doesn't use newlib_device_t yet.
 */

#ifndef __DISKMII_H__
#define __DISKMII_H__

#include <diskmii/ff.h>
#include <diskmii/diskio.h>
#include <diskmii/nandfs.h>

#define ELM_SD		0

/*! \brief Initialize libdiskmii
 *
 * \return 0 on success, anything else is failure.
 */
int diskmii_init();
/*! \brief Mounts a device
 *
 * \param drv the device to mount.
 * \return 0 on success, anything else is failure.
 */
int diskmii_mountdevice(int drv);
/*! \brief Unmounts a device, then mounts it again
 *
 * \param drv the device to remount.
 * \return 0 on success, anything else is failure.
 */
int diskmii_remountdevice(int drv);
/*! \brief Unmounts a device
 *
 * \param drv the device to unmount.
 * \return 0 on success, anything else is failure.
 */
int diskmii_unmountdevice(int drv);
/*! \brief Mounts all available devices
 *
 * \return 0 on success, anything else is failure.
 */
int diskmii_mount();
/*! \brief Remounts all available devices
 *
 * \return 0 on success, anything else is failure.
 */
int diskmii_remount();
/*! \brief Unmounts all available devices
 *
 * \return 0 on success, anything else is failure.
 */
int diskmii_unmount();
/*! \brief Checks if a device has a disk inserted
 *
 * \return TRUE if the device has a disk, FALSE otherwise.
 */
BOOL diskmii_is_inserted(int drv);

#endif

