/*
	BootMii - a Free Software replacement for the Nintendo/BroadOn bootloader.
	Requires mini.

Copyright (C) 2008, 2009	Sven Peter <svenpeter@gmail.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * NAND filesystem interface.
 */

#ifndef __NANDFS_H__
#define __NANDFS_H__

#define	NANDFS_NAME_LEN	12

#define	NANDFS_SEEK_SET	0
#define	NANDFS_SEEK_CUR	1
#define	NANDFS_SEEK_END	2

struct nandfs_fp {
	s16 first_cluster;
	s32 cur_cluster;
	u32 size;		/*!< Size of the file */
	u32 offset;		/*!< Current file offset */
};

/*! \brief Initializes the NAND FS
 *
 * \return 0 on success, anything else is failure.
 */
s32 nandfs_initialize(void);
/*! \brief Gets what percentage of the NAND FS is used
 *
 * \return The percentage of used blocks.
 */
u32 nandfs_get_usage(void);

/*! \brief Opens a NAND FS file for reading
 *
 * \param fp the file pointer to store the file information.
 * \param path the path to the file to open.
 * \return 0 on success, anything else is failure.
 */
s32 nandfs_open(struct nandfs_fp *fp, const char *path);
/*! \brief Reads data from a file on the NAND FS
 *
 * \param ptr the memory to read into.
 * \param size the size of each element to read.
 * \param nmemb how many elements to read.
 * \param fp the file pointer to read data from.
 * \return How many bytes have been read.
 */
s32 nandfs_read(void *ptr, u32 size, u32 nmemb, struct nandfs_fp *fp);
/*! \brief Seeks to a position in a file
 *
 * \param fp the file pointer to seek in.
 * \param offset the offset to seek.
 * \param whence where to seek from.
 * \return 0 on success, anything else is failure.
 */
s32 nandfs_seek(struct nandfs_fp *fp, s32 offset, u32 whence);

#endif

