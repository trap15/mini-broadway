#!/bin/sh
SCRIPTDIR=`dirname $PWD/$0`
die() {
	echo $@
	exit 1
}
buildinstall() {
	(echo "****************\nBuilding $@!" && cd $SCRIPTDIR/$@ && make && make install) || die "Can't build $@"
}
build() {
	(echo "****************\nBuilding $@!" && cd $SCRIPTDIR/$@ && make) || die "Can't build $@"
}
clean() {
	(echo "****************\nCleaning $@!" && cd $SCRIPTDIR/$@ && make clean) || die "Can't clean $@"
}
if [ "$1" = "clean" ] || [ "$1" = "rebuild" ] ; then
	clean libbroadway
	clean libnewintf
	clean libdiskmii
	clean libhextwelve
	clean libthreads
	clean mini
fi
if [ "$1" != "clean" ] ; then
	buildinstall libbroadway
	buildinstall libnewintf
	buildinstall libdiskmii
	buildinstall libhextwelve
	buildinstall libthreads
	build mini
fi
(cd $SCRIPTDIR/examples && ./makeall.sh $1) || die "Can't build examples"
cd $SCRIPTDIR

