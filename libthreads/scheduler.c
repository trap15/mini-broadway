/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>
#include <lists.h>
#include <malloc.h>
#include <string.h>

#include <threads.h>

u32* saved_regs = (u32*)PHYS_TO_VIRT(0x2000);
u32 lwarx_inval[1];

/* Stupid timeslice algorithm using 2^prio * 10000.
 * Will make this less stupid eventually...
 */
static s64 timeslice_table[11] = {
	/* negatives */
	313, 625, 1250, 2500, 5000,
	/* 0 */
	10000,
	/* positives */
	20000, 40000, 80000, 160000, 320000
};

static s64 scheduler_make_timeslice(thread_t* thread)
{
	if((thread->priority < -5) || (thread->priority > 5))
		return 0;
	return timeslice_table[thread->priority + 5];
}

thread_t* scheduler_run(thread_t* threads)
{
	int i;
	thread_t* oldthread = threads;
	/* Save old thread's information */
	for(i = 0; i < 32; i++)
		threads->gprs[i] = saved_regs[i];
	threads->cr = saved_regs[0x20];
	threads->xer = saved_regs[0x21];
	threads->lr = saved_regs[0x22];
	threads->ctr = saved_regs[0x23];
	threads->pc = saved_regs[0x24];
	threads->msr = saved_regs[0x25];
	/* 26 and 27 are dar and dsisr, which don't apply here */
	/* 28 is the top half of the FPR with FPSCR in it. Doesn't matter. */
	threads->fpscr = saved_regs[0x29];
	for(i = 0; i < 32; i++)
		threads->fprs[i] = ((vu64*)saved_regs)[0x100/8 + i];
	/* Old thread saved, now it's time to switch in our new thread */
	do {
		threads = threads->next;
	} while(threads->state != THREAD_RUNNABLE || !scheduler_make_timeslice(threads));
	threads->curr_timeslice += scheduler_make_timeslice(threads);
	/* If this is the same thread as we were running before, don't bother to swap things out. */
	if(threads != oldthread) {
		for(i = 0; i < 32; i++)
			saved_regs[i] = threads->gprs[i];
		saved_regs[0x20] = threads->cr;
		saved_regs[0x21] = threads->xer;
		saved_regs[0x22] = threads->lr;
		saved_regs[0x23] = threads->ctr;
		saved_regs[0x24] = threads->pc;
		saved_regs[0x25] = threads->msr;
		/* 26 and 27 are dar and dsisr, which don't apply here */
		/* 28 is the top half of the FPR with FPSCR in it. Doesn't matter. */
		saved_regs[0x29] = threads->fpscr;
		for(i = 0; i < 32; i++)
			((vu64*)saved_regs)[0x100/8 + i] = threads->fprs[i];
		/* We need to invalidate any lwarx's */
		asm volatile(
			"stwcx.	0, 0, %0\n"
			: : "r"(lwarx_inval));
	}
	return threads;
}

