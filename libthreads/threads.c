/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>
#include <malloc.h>

#include <threads.h>

/* Doubly-linked list */
static thread_t* threads;
thread_t* scheduler_run(thread_t* threads);
void thread_exit(void) __attribute__ ((noreturn));
extern void* _stack_top;
static volatile int _global_pid;

void _threads_handler(int unused)
{
	(void)unused;
	threads->curr_timeslice -= DEC_TIMER;
	if(threads->curr_timeslice <= 0) {
		threads = scheduler_run(threads);
		mtspr(SPR_DEC, DEC_TIMER);
		thread_exit();
	}
	mtspr(SPR_DEC, DEC_TIMER);
}

static void threads_print(thread_t* t)
{
	int i;
	gecko_printf("\n Ptr: %p\n PID: %d\n Priority: %d\n", t, t->pid, t->priority);
	gecko_printf(" State: %d\n Timeslice: %lld\n Stack: %p\n", t->state, t->curr_timeslice, t->stack);
	gecko_printf("\n R0..R7    R8..R15  R16..R23  R24..R31\n");
	for(i = 0; i < 8; i++) {
		gecko_printf("%08lX  %08lX  %08lX  %08lX\n", t->gprs[i+0], t->gprs[i+8], t->gprs[i+16], t->gprs[i+24]);
	}
	gecko_printf("\n CR/XER    LR/CTR  SRR0/SRR1\n");
	gecko_printf("%08lX  %08lX  %08lX\n", t->cr, t->lr, t->pc);
	gecko_printf("%08lX  %08lX  %08lX\n", t->xer, t->ctr, t->msr);
	gecko_printf("\n FPR0..FPR7        FPR8..FPR15       FPR16..FPR23      FPR24..FPR31     \n");
	for(i = 0; i < 8; i++) {
		gecko_printf("%016llX  %016llX  %016llX  %016llX\n", t->fprs[i+0], t->fprs[i+8], t->fprs[i+16], t->fprs[i+24]);
	}
	gecko_printf("\n FPSCR    \n");
	gecko_printf("%08lX  \n", t->fpscr);
	usleep(50000);
}

int threads_initialize(void)
{
	_global_pid = 0;
	/* This thread's setup */
	threads = malloc(sizeof(thread_t));
	if(threads == NULL)
		return 0;
	threads->state = 0;
	threads->priority = 0;
	threads->curr_timeslice = 0;
	threads->pid = _global_pid++;
	asm volatile("mflr %0\n" : "=r"(threads->pc) );
	threads->stack = _stack_top;
	threads->next = threads;
	threads->prev = threads;
	exception_handler_table[9] = _threads_handler;
	mtspr(SPR_DEC, DEC_TIMER);
	return 1;
}

int threads_shutdown(void)
{
	/* Free all the thread objects */
	thread_t* othread = threads;
	thread_t* nthread;
	while(threads != NULL) {
		nthread = threads->next;
		free(threads);
		threads = nthread;
		if(threads == othread)
			break;
	}
	exception_handler_table[9] = NULL;
	return 1;
}

int threads_create_adv(int priority, thread_state_t state, void* stack, void* pc,
	u32 msr, u32 ctr, u32 xer, u32 cr, u32 lr, u32 fpscr,
	u32 gprs[32], u64 fprs[32])
{
	int i;
	thread_t* thread = malloc(sizeof(thread_t));
	if(thread == NULL)
		return 0;
	thread->priority = priority;
	thread->stack = stack;
	thread->state = state;
	thread->pc = (u32)pc;
	thread->msr = msr;
	thread->ctr = ctr;
	thread->xer = xer;
	thread->cr = cr;
	thread->lr = lr;
	thread->fpscr = fpscr;
	thread->curr_timeslice = 0;
	thread->exit_code = 0;
	for(i = 0; i < 32; i++) {
		thread->gprs[i] = gprs[i];
		thread->fprs[i] = fprs[i];
	}
	int oldfrz = threads_freeze();
	thread->pid = _global_pid++;
	thread->prev = threads;
	thread->next = threads->next;
	threads->next->prev = thread;
	threads->next = thread;
/*	gecko_printf("%d %d %08X %08X %08X %08X %08X %08X %08X %08X\n", thread->priority,
		thread->state, thread->stack, thread->pc, thread->msr, thread->ctr,
		thread->xer, thread->cr, thread->lr, thread->fpscr);*/
	threads_unfreeze(oldfrz);
	return thread->pid;
}

int threads_create(int priority, thread_state_t state, void* stack, u32 stacksize, void* pc)
{
	u32 r2, r13, msr;
	int i;
	thread_t t;
	int oldfrz = threads_freeze();
	asm volatile(
		"stw	2, %0\n"
		"stw	13, %1\n"
		"mfmsr	%2\n"
		: "=m"(r2), "=m"(r13), "=r"(msr));
	threads_unfreeze(oldfrz);
	for(i = 0; i < 32; i++)
		t.gprs[i] = t.fprs[i] = 0;
	t.gprs[1] = (u32)stack + stacksize - 4;
	t.gprs[2] = r2;
	t.gprs[13] = r13;
	t.ctr = t.xer = t.cr = 0;
	t.msr = msr;
	t.lr = (u32)threads_exit;
	return threads_create_adv(priority, state, stack, pc,
			t.msr, t.ctr, t.xer, t.cr, t.lr, t.fpscr,
			t.gprs, t.fprs);
}

int threads_get_pid(void)
{
	return threads->pid;
}

int threads_destroy(int pid)
{
	int oldfrz = threads_freeze();
	int i;
	thread_t* t = threads;
	int hit = 0;
	for(i = 0, t = threads; (t != threads) || (i == 0); t = t->next, i = 1) {
		if(t->pid == pid) {
			hit = 1;
			t->prev->next = t->next;
			t->next->prev = t->prev;
			free(t);
			break;
		}
	}
	threads_unfreeze(oldfrz);
	return hit;
}

void threads_exit(int code)
{
	int oldfrz = threads_freeze();
	threads->state = THREAD_EXITED;
	threads->exit_code = code;
	threads_unfreeze(oldfrz);
	threads_yield();
	for(;;);
}

int threads_state(int pid)
{
	int ostate = -3;
	int i;
	int oldfrz = threads_freeze();
	thread_t* t = threads;
	for(i = 0, t = threads; (t != threads) || (i == 0); t = t->next, i = 1) {
		if(t->pid == pid) {
			ostate = t->state;
			break;
		}
	}
	threads_unfreeze(oldfrz);
	return ostate;
}

int threads_exitcode(int pid)
{
	int ocode = -1;
	int i;
	int oldfrz = threads_freeze();
	thread_t* t = threads;
	for(i = 0, t = threads; (t != threads) || (i == 0); t = t->next, i = 1) {
		if(t->pid == pid) {
			ocode = t->exit_code;
			break;
		}
	}
	threads_unfreeze(oldfrz);
	return ocode;
}

void threads_yield(void)
{
	int oldfrz = threads_freeze();
	threads->curr_timeslice = 0;
	threads_unfreeze(oldfrz);
	nap_mode();
	power_save();
}

int threads_freeze(void)
{
	if(exception_handler_table[9] == NULL)
		return 0;
	exception_handler_table[9] = NULL;
	/* Clear EE */
	asm volatile(
		"mfmsr	0		\n"
		"ori	0, 0, 0x8000	\n"
		"xori	0, 0, 0x8000	\n"
		"mtmsr	0		\n"
		::: "0");
	return 1;
}

void threads_unfreeze(int frz)
{
	if(frz) {
		exception_handler_table[9] = _threads_handler;
		mtspr(SPR_DEC, 0xFFFFFFFF);
		/* Set EE */
		asm volatile(
			"mfmsr	0		\n"
			"ori	0, 0, 0x8000	\n"
			"mtmsr	0		\n"
			::: "0");
		/* Have to reset the timer! */
		mtspr(SPR_DEC, 2);
	}
}

