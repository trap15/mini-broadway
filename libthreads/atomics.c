/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>

#include <atomics.h>

u32 atomics_read(u32* v)
{
	u32 val;
	asm volatile(
		"lwarx	%0, 0, %1\n"
		"sync\n"
		: "=r"(val)
		: "r"(v)
		);
	return val;
}

void atomics_set(u32* v, u32 i)
{
	u32 val;
	asm volatile(
		"retry_atomset:\n"
		"	lwarx	%0, 0, %1\n"
		"	ori	%0, %2, 0\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atomset\n"
		"	sync\n"
		: "=&r"(val), "+r"(v)
		: "r"(i)
		);
}

int atomics_add(u32* v, u32 i, atomic_compare_t cmp)
{
	u32 val;
	asm volatile(
		"retry_atomadd:\n"
		"	lwarx	%0, 0, %1\n"
		"	add	%0, %0, %2\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atomadd\n"
		"	sync\n"
		: "=&r"(val), "+r"(v)
		: "r"(i)
		);
	switch(cmp) {
		case ATOMIC_COMPARE_NONE:
			return 0;
		case ATOMIC_COMPARE_ZERO:
			return val == 0;
		case ATOMIC_COMPARE_NEGATIVE:
			return (val & 0x80000000) ? 1 : 0;
		default:
			return 1;
	}
}

int atomics_sub(u32* v, u32 i, atomic_compare_t cmp)
{
	u32 val;
	asm volatile(
		"retry_atomsub:\n"
		"	lwarx	%0, 0, %1\n"
		"	sub	%0, %0, %2\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atomsub\n"
		"	sync\n"
		: "=&r"(val), "+r"(v)
		: "r"(i)
		);
	switch(cmp) {
		case ATOMIC_COMPARE_NONE:
			return 0;
		case ATOMIC_COMPARE_ZERO:
			return val == 0;
		case ATOMIC_COMPARE_NEGATIVE:
			return (val & 0x80000000) ? 1 : 0;
		default:
			return 1;
	}
}

int atomics_inc(u32* v, atomic_compare_t cmp)
{
	u32 val;
	asm volatile(
		"retry_atominc:\n"
		"	lwarx	%0, 0, %1\n"
		"	addi	%0, %0, 1\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atominc\n"
		"	sync\n"
		: "=&r"(val), "+r"(v)
		);
	switch(cmp) {
		case ATOMIC_COMPARE_NONE:
			return 0;
		case ATOMIC_COMPARE_ZERO:
			return val == 0;
		case ATOMIC_COMPARE_NEGATIVE:
			return (val & 0x80000000) ? 1 : 0;
		default:
			return 1;
	}
}

int atomics_dec(u32* v, atomic_compare_t cmp)
{
	u32 val;
	asm volatile(
		"retry_atomdec:\n"
		"	lwarx	%0, 0, %1\n"
		"	subi	%0, %0, 1\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atomdec\n"
		"	sync\n"
		: "=&r"(val), "+r"(v)
		);
	switch(cmp) {
		case ATOMIC_COMPARE_NONE:
			return 0;
		case ATOMIC_COMPARE_ZERO:
			return val == 0;
		case ATOMIC_COMPARE_NEGATIVE:
			return (val & 0x80000000) ? 1 : 0;
		default:
			return 1;
	}
}

int atomics_bitset(u32* v, u32 bit)
{
	u32 val, ov;
	u32 m = 1 << bit;
	asm volatile(
		"retry_atombitset:\n"
		"	lwarx	%0, 0, %1\n"
		"	and	%2, %0, %3\n"
		"	or	%0, %0, %3\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atombitset\n"
		"	sync\n"
		: "=&r"(val), "+r"(v), "=&r"(ov)
		: "r"(m)
		);
	return ov ? 1 : 0;
}

int atomics_bitclear(u32* v, u32 bit)
{
	u32 val, ov;
	u32 m = 1 << bit;
	u32 m2 = ~m;
	asm volatile(
		"retry_atombitclr:\n"
		"	lwarx	%0, 0, %1\n"
		"	and	%2, %0, %4\n"
		"	and	%0, %0, %3\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atombitclr\n"
		"	sync\n"
		: "=&r"(val), "+r"(v), "=&r"(ov)
		: "r"(m2), "r"(m)
		);
	return ov ? 1 : 0;
}

int atomics_bitchange(u32* v, u32 bit)
{
	u32 val, ov;
	u32 m = 1 << bit;
	asm volatile(
		"retry_atombitchg:\n"
		"	lwarx	%0, 0, %1\n"
		"	and	%2, %0, %3\n"
		"	xor	%0, %0, %3\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atombitchg\n"
		"	sync\n"
		: "=&r"(val), "+r"(v), "=&r"(ov)
		: "r"(m)
		);
	return ov ? 1 : 0;
}

int atomics_bitcheck(u32* v, u32 bit)
{
	u32 val, ov;
	u32 m = 1 << bit;
	asm volatile(
		"retry_atombitchk:\n"
		"	lwarx	%0, 0, %1\n"
		"	and	%2, %0, %3\n"
		"	stwcx.	%0, 0, %1\n"
		"	bne-	retry_atombitchk\n"
		"	sync\n"
		: "=&r"(val), "+r"(v), "=&r"(ov)
		: "r"(m)
		);
	return ov ? 1 : 0;
}

