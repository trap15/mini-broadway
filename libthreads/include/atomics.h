/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Atomic instruction wrappers
 */

#ifndef __ATOMICS_H__
#define __ATOMICS_H__

/*! An enumeration of the types of atomic comparisons available */
typedef enum {
	ATOMIC_COMPARE_NONE = 0, /*!< Don't compare */
	ATOMIC_COMPARE_ZERO, /*!< If the result is 0, return 1. Otherwise return 0. */
	ATOMIC_COMPARE_NEGATIVE, /*!< If the result is negative, return 1. Otherwise return 0. */
} atomic_compare_t;

/*! \brief Atomically read an integer
 *
 * \param v a pointer to an integer to read atomically.
 * \return The value of the integer.
 */
u32 atomics_read(u32* v);
/*! \brief Atomically set the value of an integer
 *
 * \param v a pointer to an integer to set atomically.
 * \param i the value to set the integer to.
 */
void atomics_set(u32* v, u32 i);
/*! \brief Atomically add an integer
 *
 * \param v a pointer to an integer to add to atomically.
 * \param i the value to add to the integer.
 * \param cmp the type of comparison to do with the result.
 * \return The result of the comparison.
 */
int atomics_add(u32* v, u32 i, atomic_compare_t cmp);
/*! \brief Atomically subtract an integer
 *
 * \param v a pointer to an integer to subtract from atomically.
 * \param i the value to subtract the integer.
 * \param cmp the type of comparison to do with the result.
 * \return The result of the comparison.
 */
int atomics_sub(u32* v, u32 i, atomic_compare_t cmp);
/*! \brief Atomically increase an integer by 1
 *
 * \param v a pointer to an integer to increase atomically.
 * \param cmp the type of comparison to do with the result.
 * \return The result of the comparison.
 */
int atomics_inc(u32* v, atomic_compare_t cmp);
/*! \brief Atomically decrease an integer by 1
 *
 * \param v a pointer to an integer to decrease atomically.
 * \param cmp the type of comparison to do with the result.
 * \return The result of the comparison.
 */
int atomics_dec(u32* v, atomic_compare_t cmp);
/*! \brief Atomically set a bit in an integer
 *
 * \param v a pointer to an integer to set a bit in atomically.
 * \param bit the bit index to set. 0 is least significant bit.
 * \return The old value of the bit. If set, result is 1.
 */
int atomics_bitset(u32* v, u32 bit);
/*! \brief Atomically clear a bit in an integer
 *
 * \param v a pointer to an integer to clear a bit in atomically.
 * \param bit the bit index to clear. 0 is least significant bit.
 * \return The old value of the bit. If set, result is 1.
 */
int atomics_bitclear(u32* v, u32 bit);
/*! \brief Atomically switch a bit in an integer
 *
 * \param v a pointer to an integer to switch a bit in atomically.
 * \param bit the bit index to switch. 0 is least significant bit.
 * \return The old value of the bit. If set, result is 1.
 */
int atomics_bitchange(u32* v, u32 bit);
/*! \brief Atomically check a bit in an integer
 *
 * \param v a pointer to an integer to check a bit in atomically.
 * \param bit the bit index to check. 0 is least significant bit.
 * \return The old value of the bit. If set, result is 1.
 */
int atomics_bitcheck(u32* v, u32 bit);

#endif

