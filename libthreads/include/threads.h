/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * A threading API written specifically for the Wii for mini-broadway.
 */

#ifndef __THREADS_H__
#define __THREADS_H__

#define TICK2US(x)	(((x) * 4) / 243)
#define US2TICK(x)	(((x) * 243) / 4)
#define DEC_TIMER	(100)

typedef enum {
	THREAD_EXITED		= -2,
	THREAD_UNRUNNABLE	= -1,
	THREAD_RUNNABLE		= 0,
} thread_state_t;

/*! Thread structure. No touchy. */
typedef struct thread_t {
	struct thread_t* next;
	struct thread_t* prev;
	volatile thread_state_t state; /* -2 exited, -1 unrunnable, 0 runnable, >0 stopped */
	/* Stack data */
	void* stack;
	/* Register states */
	u32 gprs[32];
	u64 fprs[32];
	u32 fpscr, lr, cr, xer, ctr, pc, msr;
	/* Scheduling */
	int priority;
	volatile s64 curr_timeslice;
	/* Misc. */
	int pid;
	int exit_code;
} thread_t;

/*! \brief Initialize the threading subsystem
 *
 * Initializes the threading subsystem, registers the handler for the decrementer
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa threads_shutdown()
 */
int threads_initialize(void);
/*! \brief Shut down the threading subsystem
 *
 * Shuts down the threading subsystem, unregisters the handler for the decrementer
 * and performs all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa threads_initialize()
 */
int threads_shutdown(void);
/*! \brief Create a thread
 *
 * A simple method to create a thread.
 * \param priority the priority level of the thread. Range from -5 to 5.
 * \param state the beginning state of the thread.
 * \param stack a pointer to a section of memory to be used as the thread's stack.
 * \param stacksize the size of the stack for the thread.
 * \param pc a function to be executed by the thread.
 * \return The PID of the new thread, or 0 if thread creation failed.
 * \sa threads_create_adv()
 */
int threads_create(int priority, thread_state_t state, void* stack, u32 stacksize, void* pc);
/*! \brief Create a thread
 *
 * A complex method to create a thread.
 * \param priority the priority level of the thread. Range from -5 to 5.
 * \param state the beginning state of the thread.
 * \param stack a pointer to a section of memory to be used as the thread's stack.
 * \param pc a function to be executed by the thread.
 * \param msr the MSR of the new thread.
 * \param ctr the CTR of the new thread.
 * \param xer the XER of the new thread.
 * \param cr the CR of the new thread.
 * \param lr the LR of the new thread.
 * \param fpscr the FPSCR of the new thread.
 * \param gprs the GPRs of the new thread. You must have r1 set to the stack, as
 *                 this function will not automatically set it for you.
 * \param fprs the FPRs of the new thread.
 * \return The PID of the new thread, or 0 if thread creation failed.
 * \sa threads_create()
 */
int threads_create_adv(int priority, thread_state_t state, void* stack, void* pc,
	u32 msr, u32 ctr, u32 xer, u32 cr, u32 lr, u32 fpscr,
	u32 gprs[32], u64 fprs[32]);
/*! \brief Get's the PID of the current thread
 *
 * \return The PID of the current thread.
 */
int threads_get_pid(void);
/*! \brief Destroy a thread
 *
 * Ends execution of a thread with specified PID, and removes it from the queue.
 * \param pid the PID of the thread to destroy.
 * \return If thread was destroyed, returns 1. Otherwise 0.
 * \sa threads_create(), threads_create_adv()
 */
int threads_destroy(int pid);
/*! \brief Exits the current thread
 *
 * Ends execution of the current thread, and disables it from running. The thread
 * still will remain in the queue however, so make sure to destroy it later with
 * threads_destroy().
 * \param code the exit code for the thread to go out on.
 * \sa threads_create(), threads_create_adv(), threads_destroy()
 */
void threads_exit(int code);
/*! \brief Get the state of a thread
 *
 * Returns the state of the thread with the specified PID.
 * \param pid the PID of the thread to get the state of.
 * \return State of the thread if found. Otherwise -3.
 * \sa threads_create(), threads_create_adv()
 */
int threads_state(int pid);
/*! \brief Get the exit code of a thread
 *
 * Returns the exit code of the thread with the specified PID.
 * If the thread has not yet exited, this value will be 0.
 * \param pid the PID of the thread to get the exit code of.
 * \return Exit code of the thread if found. Otherwise -1.
 * \sa threads_create(), threads_create_adv(), threads_exit()
 */
int threads_exitcode(int pid);
/*! \brief Yield the current thread to the scheduler
 *
 * The current thread suspends execution and gives the remainder of its time up
 * to the scheduler and other threads.
 */
void threads_yield(void);
/* BE FUCKING CAREFUL WITH THESE, YOU FUCKTARD */
/*! \brief Freeze the scheduler
 *
 * <b>BE EXTREMELY CAREFUL WITH THIS FUNCTION, AS IMPROPER USE <i>WILL</i> LEAD TO
 * A COMPLETE LOCKUP.</b>
 * Freeze the scheduler and disable external interrupts in order to provide a safe
 * area for no scheduler disruptions.
 * \return A cookie to be passed to threads_unfreeze() at the end of the block.
 * \sa threads_unfreeze()
 */
int threads_freeze(void);
/*! \brief Unfreeze the scheduler
 *
 * Unfreeze the scheduler and re-enable external interrupts if they were as such
 * before the freeze that the cookie corresponds to.
 * \param frz the cookie returned from the corresponding threads_freeze() call.
 * \sa threads_freeze()
 */
void threads_unfreeze(int frz);

#endif

