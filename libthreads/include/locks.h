/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Locking API for libthreads.
 * \todo Semaphores and mutexes should have a queue of threads that are waiting
 *       for a lock, then wake one whenever the lock is unlocked. (thanks tueidj)
 */

#ifndef __LOCKS_H__
#define __LOCKS_H__

/*! Semaphore structure. No touchy. */
typedef struct {
	u32 curr_locks;
	u32 lockcount;
} semaphore_t;

/*! \brief Creates a semaphore
 *
 * \param count how many locks the semaphore may take before waiting.
 * \return A semaphore object with the requested specification.
 */
semaphore_t semaphores_create(int count);
/*! \brief Locks a semaphore
 *
 * This call is <i>BLOCKING</i>, using sleep mode. Do not use this function inside of
 * an interrupt handler, nor within a threads_freeze() block.
 * \param s a pointer to the semaphore to lock.
 * \sa semaphores_lock_noblock(), semaphores_unlock()
 */
void semaphores_lock(semaphore_t* s);
/*! \brief Locks a semaphore
 *
 * This call is nonblocking, thus it is safe to use within an interrupt handler
 * or a threads_freeze() block. It will return no matter whether it succeeded
 * or failed.
 * \param s a pointer to the semaphore to lock.
 * \return 1 if the semaphore was able to be locked, 0 if it was not.
 * \sa semaphores_lock(), semaphores_unlock()
 */
int semaphores_lock_noblock(semaphore_t* s);
/*! \brief Unlocks a semaphore
 *
 * Unlocks a semaphore by a single level.
 * \param s a pointer to the semaphore to unlock.
 * \sa semaphores_lock_noblock(), semaphores_lock()
 */
void semaphores_unlock(semaphore_t* s);

/*! Mutex structure. No touchy. */
typedef struct {
	u32 locked; /* b31: locked b30~b0: pid */
} mutex_t;

/*! \brief Creates a mutex
 *
 * \return A mutex object.
 */
mutex_t mutexes_create(void);
/*! \brief Locks a mutex
 *
 * This call is <i>BLOCKING</i>, using sleep mode. Do not use this function inside of
 * an interrupt handler, nor within a threads_freeze() block.
 * \param m a pointer to the mutex to lock.
 * \sa mutexes_lock_noblock(), mutexes_unlock()
 */
void mutexes_lock(mutex_t* m);
/*! \brief Locks a mutex
 *
 * This call is nonblocking, thus it is safe to use within an interrupt handler
 * or a threads_freeze() block. It will return no matter whether it succeeded
 * or failed.
 * \param m a pointer to the mutex to lock.
 * \return 1 if the mutex was able to be locked, 0 if it was not.
 * \sa mutexes_lock(), mutexes_unlock()
 */
int mutexes_lock_noblock(mutex_t* m);
/*! \brief Unlocks a mutex
 *
 * \param m a pointer to the mutex to unlock.
 * \sa mutexes_lock_noblock(), mutexes_lock()
 */
void mutexes_unlock(mutex_t* m);

#endif

