/*
	libthreads - a threading API for Wii

Copyright (C) 2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>

#include <threads.h>
#include <atomics.h>
#include <locks.h>

semaphore_t semaphores_create(int count)
{
	semaphore_t sema4;
	sema4.curr_locks = 0;
	sema4.lockcount = count;
	return sema4;
}

void semaphores_lock(semaphore_t* s)
{
	while(!semaphores_lock_noblock(s))
		threads_yield();
}

int semaphores_lock_noblock(semaphore_t* s)
{
	int frz = threads_freeze();
	if(s->curr_locks < s->lockcount) {
		s->curr_locks++;
		threads_unfreeze(frz);
		return 1;
	}
	threads_unfreeze(frz);
	return 0;
}

void semaphores_unlock(semaphore_t* s)
{
	int frz = threads_freeze();
	if(s->curr_locks > 0) {
		s->curr_locks--;
	}
	threads_unfreeze(frz);
}

mutex_t mutexes_create(void)
{
	mutex_t mutex;
	mutex.locked = 0;
	return mutex;
}

void mutexes_lock(mutex_t* m)
{
	while(!mutexes_lock_noblock(m))
		threads_yield();
}

int mutexes_lock_noblock(mutex_t* m)
{
	int frz = threads_freeze();
	if(m->locked == 0) {
		m->locked = threads_get_pid() | 0x80000000;
		threads_unfreeze(frz);
		return 1;
	}
	threads_unfreeze(frz);
	return 0;
}

void mutexes_unlock(mutex_t* m)
{
	int frz = threads_freeze();
	if(m->locked == (threads_get_pid() | 0x80000000)) {
		m->locked = 0;
	}
	threads_unfreeze(frz);
}

