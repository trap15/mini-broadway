/*
	mini - a Free Software replacement for the Nintendo/BroadOn IOS.
	inter-processor communications

Copyright (C) 2008, 2009	Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2008, 2009	Sven Peter <svenpeter@gmail.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef __IPC_H__
#define __IPC_H__

#include "types.h"

#include <ipc_common.h>

#define IPC_IN_SIZE	32
#define IPC_OUT_SIZE	32
#define IPC_SLOW_SIZE	128

typedef struct {
	union {
		struct {
			u8 flags;
			u8 device;
			u16 req;
		};
		u32 code;
	};
	u32 tag;
	u32 args[6];
} ipc_request;

typedef const struct {
	char magic[3];
	char version;
	void *mem2_boundary;
	volatile ipc_request *ipc_in;
	u32 ipc_in_size;
	volatile ipc_request *ipc_out;
	u32 ipc_out_size;
} ipc_infohdr;

void ipc_irq(void);

void ipc_initialize(void);
void ipc_shutdown(void);
void ipc_post(u32 code, u32 tag, u32 num_args, ...);
void ipc_flush(void);
u32  ipc_process_slow(void);

// Enqueues a request in the slow in_queue, use this in IRQ context only.
void ipc_enqueue_slow(u8 device, u16 req, u32 num_args, ...);

#endif

