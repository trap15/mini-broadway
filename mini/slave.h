/*
	mini - a Free Software replacement for the Nintendo/BroadOn IOS.
	ARM slaving support

Copyright (C) 2010		Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef __SLAVE_H__
#define __SLAVE_H__

#include "ipc.h"

int slave_load(void* code, u32 size);
int slave_execute(void);
void slave_params(void* data, u32 size);
void* slave_results(void);
int slave_executing(void);
void slave_ipc(volatile ipc_request *req);
void slave_execute_do(void);
void slave_watchdog(void);

#endif

