/*
	mini - a Free Software replacement for the Nintendo/BroadOn IOS.
	ARM slaving support

Copyright (C) 2010-2011		Alex Marshall "trap15" <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include "types.h"
#include "memory.h"
#include "slave.h"
#include "powerpc.h"
#include "powerpc_elf.h"
#include "hollywood.h"
#include "utils.h"
#include "string.h"
#include "start.h"
#include "gecko.h"
#include "slavehead.i"

/* 20ms to a tick. 200ms for watchdog count-down */
#define SLAVE_WATCHDOG_START (200/20)

u8 slavecode[0x40000] MEM2_BSS;
u32 slavesize = 0;
u8 slaveresults[0x8000] MEM2_BSS;
u8 slaveparams[0x8000] MEM2_BSS;
u32 slaveparamsize = 0;
int slave_ready = 1;
int slave_fire = 0;
int _slave_watchdog = SLAVE_WATCHDOG_START;

void swi_handler(u32 swi, u32* regs)
{
	switch(swi) {
		case swiMemset: /* memset */
			memset((void*)regs[0], regs[1], regs[2]);
			break;
		case swiMemcpy: /* memcpy */
			memcpy((void*)regs[0], (void*)regs[1], regs[2]);
			break;
		case swiMemcmp: /* memcmp */
			regs[0] = memcmp((void*)regs[0], (void*)regs[1], regs[2]);
			break;
		case swiWatchdogKick: /* Kick the watchdog */
			_slave_watchdog = SLAVE_WATCHDOG_START;
			break;
		case swiEnd: /* End execution */
			slave_fire = 0;
			slave_ready = 1;
			break;
		case swiGeckoPrint: /* Gecko print */
			gecko_printf("SLAVE (%08X): %s\n", regs[0], (char*)regs[0]);
			break;
		default:
			break;
	}
}

void slave_watchdog(void)
{
	_slave_watchdog--;
	/* Need to actually make this exit the code instead of just telling it
	 * to stop executing the mainloop...
	 */
	if(_slave_watchdog == 0) {
		slave_fire = 0;
		slave_ready = 1;
		_slave_watchdog = SLAVE_WATCHDOG_START;
	}
}

int slave_load(void* code, u32 size)
{
	if(size > 0x40000)
		return -1;
	slavesize = size;
	memcpy(slavecode, code, size);
	return 0;
}

int slave_execute(void)
{
	int ret;
	int (*slave)(void* indata, u32 insize, void* outdata, void* entrypoint);
	slave = slavecode;
	slave_ready = 0;
	ret = slave(slaveparams, slaveparamsize, slaveresults, slave);
	slave_ready = 1;
	return ret;
}

void slave_params(void* data, u32 size)
{
	if(size > 0x8000)
		return;
	slaveparamsize = size;
	memcpy(slaveparams, data, size);
}

void* slave_results(void)
{
	return slaveresults;
}

void slave_execute_do(void)
{
	if(slave_fire && slave_ready) {
		slave_execute();
	}else{
		_slave_watchdog = SLAVE_WATCHDOG_START;
	}
}

int slave_executing(void)
{
	return slave_fire;
}

void slave_ipc(volatile ipc_request *req)
{
	switch (req->req) {
	case IPC_SLAVE_LOAD:
		if(req->args[0]) {
			// Enqueued from ARM side, do not invalidate mem nor ipc_post
			slave_load((void*)req->args[1], req->args[2]);
		}else{
			dc_invalidaterange((void*)req->args[1], req->args[2]);
			int res = slave_load((void*)req->args[1], req->args[2]);
			ipc_post(req->code, req->tag, 1, res);
		}
		break;

	case IPC_SLAVE_PARAMS:
		if(req->args[0]) {
			// Enqueued from ARM side, do not invalidate mem nor ipc_post
			slave_params((void*)req->args[1], req->args[2]);
		}else{
			dc_invalidaterange((void*)req->args[1], slaveparamsize);
			slave_params((void*)req->args[1], req->args[2]);
			ipc_post(req->code, req->tag, 0);
		}
		break;

	case IPC_SLAVE_EXECUTE:
		if(req->args[0]) {
			// Enqueued from ARM side, do not invalidate mem nor ipc_post
			while(!slave_ready);
			slave_execute();
		}else{
			while(!slave_ready);
			slave_fire = 1;
			ipc_post(req->code, req->tag, 0);
		}
		break;

	case IPC_SLAVE_RESULTS:
		if(req->args[0]) {
			// Enqueued from ARM side, do nothing
		}else{
			memcpy((void*)req->args[0], slave_results(), 0x8000);
			dc_flushrange((void*)req->args[0], 0x8000);
			ipc_post(req->code, req->tag, 0);
		}
		break;

	case IPC_SLAVE_EXECUTING:
		if(req->args[0]) {
			// Enqueued from ARM side, do nothing
		}else{
			ipc_post(req->code, req->tag, 1, slave_executing());
		}
		break;

	default:
		gecko_printf("IPC: unknown SLOW Slave request %04X\n", req->req);
	}
}

