################################################################################

RM       = rm -rf
CC       = gcc
CXX      = g++

CFLAGS   = -O2 -fno-strict-aliasing -ICommon -IDSP
CXXFLAGS = $(CFLAGS) -fno-exceptions
LDFLAGS  = -lpthread

COREOBJS = Common/FileUtil.o Common/Hash.o \
           Common/MsgHandler.o Common/MemoryUtil.o Common/Misc.o \
           Common/StringUtil.o Common/Timer.o
DSPOBJS  = DSP/assemble.o DSP/disassemble.o DSP/DSPAccelerator.o \
           DSP/DSPAnalyzer.o DSP/DSPCodeUtil.o DSP/DSPCore.o \
           DSP/DSPHWInterface.o DSP/DSPMemoryMap.o DSP/DSPStacks.o \
           DSP/DSPTables.o DSP/LabelMap.o
OBJS     = $(COREOBJS) $(DSPOBJS) DSPTool.o

TARGET   = dsptool

.PHONY: all clean install

all: $(TARGET)

clean:
	@echo "  CLEANING"
	@$(RM) *.o DSP/*.o Common/*.o $(TARGET)

install: $(TARGET)
	@echo "  INSTALLING    $<"
	@cp $< $(WIIDEV)/bin

%.o : %.c
	@echo "  COMPILING     $<"
	@$(CC) $(CFLAGS) -o $@ -c $<
%.o : %.cpp
	@echo "  COMPILING     $<"
	@$(CXX) $(CXXFLAGS) -o $@ -c $<

dsptool: $(OBJS)
	@echo "  LINKING       $@"
	@$(CXX) $(LDFLAGS) -o $@ $(OBJS)

