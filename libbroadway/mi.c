/*
	libbroadway - A general purpose library to control the Wii.
	MI support

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <string.h>
#include <types.h>
#include <broadway.h>

static irq_handler_t	irq_mi_handler_table[IRQ_MI_MAX];

static int _irq_mi_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data)
{
	irq_mi_handler_table[irqn % IRQ_MI_MAX].exec = exec;
	irq_mi_handler_table[irqn % IRQ_MI_MAX].data = data;
	return 1;
}

static irq_handler_t _irq_mi_get_handler(u32 irqn)
{
	return irq_mi_handler_table[irqn % IRQ_MI_MAX];
}

static int _mi_irq_handler(u32 irq, void* data)
{
	int i;
	irq_handler_t handler;
	u16 flags = read16(MI_IRQFLAG);
	u16 enabled = read16(MI_IRQMASK);
	u16 useflags = flags & enabled;
	
	write16(MI_UNKNOWN1, 0); /*?*/
	if(useflags == 0) {
		/* Spurious interrupt. Ignore */
		return 1;
	}
	for(i = 0; i < IRQ_MI_MAX; i++) {
		if(useflags & IRQF(i)) { 
			handler = _irq_mi_get_handler(i);
			if(handler.exec) {
				(handler.exec)(i, handler.data);
				useflags &= ~IRQF(i);
			}
			write16(MI_IRQFLAG, IRQF(i));
		}
	}
	(void)irq;
	(void)data;
	return 1;
}

int mi_initialize(void)
{
	int i;
	for(i = 0; i < 4; i++) {
		mi_unprotect_region(i);
		irq_mi_disable(i);
	}
	_irq_mi_register_handler(IRQ_MI_MEMADDR, NULL, NULL);
	if(!irq_bw_register_handler(IRQ_BW_MEM, _mi_irq_handler, NULL))
		return 0;
	write16(MI_REG_BASE + 40, 2);
	irq_mi_enable(IRQ_MI_MEMADDR);
	irq_bw_enable(IRQ_BW_MEM);
	return 1;
}

int mi_shutdown(void)
{
	irq_bw_disable(IRQ_BW_MEM);
	if(!irq_bw_register_handler(IRQ_BW_MEM, NULL, NULL))
		return 0;
	return 1;
}

int mi_protect_region(void* addr, int size, BOOL r, BOOL w, int (*exec)(u32 irq, void* data), void* data)
{
	u16 prot = r | (w << 1);
	int rgn;
	u16 cprot = read16(MI_PROT_TYPE);
	u16 fpage = virt_to_phys(addr) >> 10;
	u16 epage = (virt_to_phys(addr) + size) >> 10;
	if(size % 1024)
		epage++;
	for(rgn = 0; rgn < 4; rgn++) {
		if((((cprot >> (rgn << 1)) & 3) == 3) || (!_irq_mi_get_handler(rgn).exec)) {
			/* If the region is R/W, it's free */
			/* Also if it has no handler registered it's free */
			break;
		}
	}
	if(rgn == 4) {
		/* No available protection regions! */
		return -1;
	}
	sync_after_write((void*)(fpage << 10), (epage - fpage) << 10);
	gecko_printf("f: %04X e: %04X exec: %08X prot: %04X\n", fpage, epage, exec, prot << (rgn << 1));
	write16(MI_UNKNOWN1, 0); /*?*/
	_irq_mi_register_handler(rgn, exec, data);
/*	mask16(MI_PROT_TYPE, 3 << (rgn << 1), prot << (rgn << 1)); */
	write16(MI_PROT_TYPE,        prot << (rgn << 1));
	write16(MI_PROT_RGNTOP(rgn), fpage);
	write16(MI_PROT_RGNBOT(rgn), epage);
	gecko_printf("proto: %04X fo: %04X eo: %04X\n", read16(MI_PROT_TYPE), read16(MI_PROT_RGNTOP(rgn)), read16(MI_PROT_RGNBOT(rgn)));
	return rgn;
}

int mi_unprotect_region(int rgn)
{
	write16(MI_UNKNOWN1, 0); /*?*/
	set16(MI_PROT_TYPE, 3 << (rgn << 1));
	write16(MI_PROT_RGNTOP(rgn), 0);
	write16(MI_PROT_RGNBOT(rgn), 0);
	return _irq_mi_register_handler(rgn, NULL, NULL);
}

void irq_mi_enable(u32 irq)
{
	set16(MI_IRQMASK, IRQF(irq));
	set16(MI_IRQFLAG, IRQF(irq));
}

void irq_mi_disable(u32 irq)
{
	clear16(MI_IRQMASK, IRQF(irq));
	set16(  MI_IRQFLAG, IRQF(irq));
}

