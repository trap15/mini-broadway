/*
	libbroadway - A general purpose library to control the Wii.
	AI support

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <string.h>
#include <types.h>
#include <broadway.h>

#define AI_FLAG_RATE		(1 << 6)
#define AI_FLAG_SCRESET		(1 << 5)
#define AI_FLAG_AIINTVLD	(1 << 4)
#define AI_FLAG_AIINT		(1 << 3)
#define AI_FLAG_AIINTMSK	(1 << 2)
#define AI_FLAG_AFR		(1 << 1)
#define AI_FLAG_PLAY		(1 << 0)

static BOOL _ai_init = FALSE;
static BOOL _ai_playing = FALSE;

static irq_handler_t _ai_irq_cb;

static int _ai_irq_handler(u32 irq, void* data)
{
	int ctrl = read32(AI_CONTROL);
	if(ctrl & AI_FLAG_AIINT)
		if(_ai_irq_cb.exec)
			(_ai_irq_cb.exec)(0, _ai_irq_cb.data);
	(void)irq;
	(void)data;
	return 1;
}

int ai_initialize(void)
{
	if(!irq_bw_register_handler(IRQ_BW_AI, _ai_irq_handler, NULL))
		return 0;
	/* Reset the AI to be:
	 * 48KHz Samples
	 * Reset Sample Counter
	 * AI_AIIT matching AI_AISCNT interrupts
	 * AI Interrupt is reset
	 * AI interrupt is masked
	 * Not playing
	 * Muted volume
	 * AI Int on farthest sample.
	 */
	write32(AI_CONTROL, AI_FLAG_SCRESET | AI_FLAG_AIINT);
	write32(AI_VOLUME, 0);
	write32(AI_AIIT, 0xFFFFFFFF);
	irq_bw_enable(IRQ_BW_AI);
	_ai_init = TRUE;
	return 1;
}

int ai_shutdown(void)
{
	if(!_ai_init)
		return 0;
	write32(AI_VOLUME, 0);
	write32(AI_AIIT, 0xFFFFFFFF);
	write32(AI_CONTROL, AI_FLAG_SCRESET | AI_FLAG_AIINT);
	irq_bw_disable(IRQ_BW_AI);
	if(!irq_bw_register_handler(IRQ_BW_AI, NULL, NULL))
		return 0;
	_ai_playing = FALSE;
	return 1;
}

void ai_set_sample_rate(ai_sample_rate_t rate)
{
	if(!_ai_init)
		return;
	if(!_ai_playing)
		mask32(AI_CONTROL, AI_FLAG_AFR | AI_FLAG_RATE, 
			(rate == AI_SAMPLE_RATE_32000) ? (AI_FLAG_AFR | AI_FLAG_RATE) : 0);
}

ai_sample_rate_t ai_get_sample_rate(void)
{
	if(!_ai_init)
		return -1;
	return (read32(AI_CONTROL) & AI_FLAG_RATE) ? AI_SAMPLE_RATE_32000 : AI_SAMPLE_RATE_48000;
}

void ai_enable_irq(int on_sample, int (*exec)(u32 irq, void* data), void* data)
{
	if(!_ai_init)
		return;
	write32(AI_AIIT, on_sample);
	_ai_irq_cb.exec = exec;
	_ai_irq_cb.data = data;
	set32(AI_CONTROL, AI_FLAG_AIINTMSK | AI_FLAG_AIINTVLD | AI_FLAG_AIINT);
}

void ai_disable_irq(void)
{
	if(!_ai_init)
		return;
	mask32(AI_CONTROL, AI_FLAG_AIINTMSK | AI_FLAG_AIINTVLD, AI_FLAG_AIINT);
	_ai_irq_cb.exec = NULL;
	_ai_irq_cb.data = NULL;
}

void ai_set_volume(u8 left, u8 right)
{
	if(!_ai_init)
		return;
	write32(AI_VOLUME, left | (right << 8));
}

void ai_get_volume(u8* left, u8* right)
{
	if(!_ai_init)
		return;
	u32 vol = read32(AI_VOLUME);
	*left  = (vol     ) & 0xFF;
	*right = (vol >> 8) & 0xFF;
}

u32 ai_samples_played(void)
{
	if(!_ai_init)
		return 0xFFFFFFFF;
	return read32(AI_AISCNT);
}

void ai_reset_sample_counter(void)
{
	if(!_ai_init)
		return;
	set32(AI_CONTROL, AI_FLAG_SCRESET);
}

int ai_is_playing(void)
{
	if(!_ai_init)
		return -1;
	return (read32(AI_CONTROL) & AI_FLAG_PLAY) ? 1 : 0;
}

int ai_play(int play)
{
	int playing;
	if(!_ai_init)
		return -1;
	playing = ai_is_playing();
	if(play) {
		set32(AI_CONTROL, 1);
	}else{
		clear32(AI_CONTROL, 1);
	}
	return playing;
}

