/*
	libbroadway - A general purpose library to control the Wii.
	DSP support

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <types.h>
#include <broadway.h>
#include <string.h>

#define DSP_FLAG_RESETDSP	(1 << 11)
#define DSP_FLAG_DSPDMAINT	(1 <<  9)
#define DSP_FLAG_DSPINTMASK	(1 <<  8)
#define DSP_FLAG_DSPINT		(1 <<  7)
#define DSP_FLAG_ARAMINTMASK	(1 <<  6)
#define DSP_FLAG_ARAMINT	(1 <<  5)
#define DSP_FLAG_AIINTMASK	(1 <<  4)
#define DSP_FLAG_AIINT		(1 <<  3)
#define DSP_FLAG_HALTDSP	(1 <<  2)
#define DSP_FLAG_PIINT		(1 <<  1)
#define DSP_FLAG_RESET		(1 <<  0)

#define DSP_UCODE_FEED		(0x8071FEED)
#define DSP_UCODE_IRAM_SRC	(0x80F3A001)
#define DSP_UCODE_IRAM_DST	(0x80F3C002)
#define DSP_UCODE_IRAM_LEN	(0x80F3A002)
#define DSP_UCODE_DRAM_SRC	(0x80F3B001)
#define DSP_UCODE_DRAM_DST	(0x80F3C001)
#define DSP_UCODE_DRAM_LEN	(0x80F3B002)
#define DSP_UCODE_START_PC	(0x80F3D001)

/* TODO: Disassemble this. */
static u32 _dsp_initcode[] ATTRIBUTE_ALIGN(32) =
{
	0x029F0010,0x029F0033,0x029F0034,0x029F0035,
	0x029F0036,0x029F0037,0x029F0038,0x029F0039,
	0x12061203,0x12041205,0x00808000,0x0088FFFF,
	0x00841000,0x0064001D,0x02180000,0x81001C1E,
	0x00441B1E,0x00840800,0x00640027,0x191E0000,
	0x00DEFFFC,0x02A08000,0x029C0028,0x16FC0054,
	0x16FD4348,0x002102FF,0x02FF02FF,0x02FF02FF,
	0x02FF02FF,0x00000000,0x00000000,0x00000000
};

irq_handler_t _irq_dsp_handler = { NULL, NULL };

static int _dsp_irq_handler(u32 irq, void *data)
{
	/* ACK */
	if(_irq_dsp_handler.exec)
		_irq_dsp_handler.exec(0, _irq_dsp_handler.data);
	set16(DSP_STATUS, DSP_FLAG_DSPINT);
	(void)irq;
	(void)data;
	return 1;
}

int irq_dsp_register_handler(int (*exec)(u32 irq, void* data), void* data)
{
	_irq_dsp_handler.exec = exec;
	_irq_dsp_handler.data = data;
	return 1;
}

irq_handler_t irq_dsp_get_handler(void)
{
	return _irq_dsp_handler;
}

void irq_dsp_enable(void)
{
	set16(DSP_STATUS, DSP_FLAG_DSPINTMASK);
}

void irq_dsp_disable(void)
{
	clear16(DSP_STATUS, DSP_FLAG_DSPINTMASK);
}

void dsp_send_mail(u32 data)
{
	gecko_printf("Sending %04X %04X\n", data >> 16, data & 0xFFFF);
	write16(DSP_MAILBOX_IN_H, data >> 16);
	write16(DSP_MAILBOX_IN_L, data & 0xFFFF);
}

u32 dsp_recv_mail(void)
{
	u32 data;
	data  = read16(DSP_MAILBOX_OUT_H) << 16;
	data |= read16(DSP_MAILBOX_OUT_L);
	return data;
}

u32 dsp_read_outbox(void)
{
	u32 data;
	data  = read16(DSP_MAILBOX_IN_H) << 16;
	data |= read16(DSP_MAILBOX_IN_L);
	return data;
}

BOOL dsp_check_inbox(void)
{
	return (read16(DSP_MAILBOX_OUT_H) & 0x8000) ? TRUE : FALSE;
}

BOOL dsp_check_outbox(void)
{
	return (read16(DSP_MAILBOX_IN_H) & 0x8000) ? TRUE : FALSE;
}

#define WAITTIME 0x100000
#define DO_DSPWAIT usleep(500); for(waitcount = 0; \
	dsp_check_outbox() && (waitcount < WAITTIME); \
	waitcount++); if(waitcount >= WAITTIME) gecko_printf("NORECV\n")
#define DO_INBOXWAIT usleep(500); for(waitcount = 0; \
	!dsp_check_inbox() && (waitcount < (WAITTIME * 0x10)); \
	waitcount++); if(waitcount >= (WAITTIME * 0x10)) gecko_printf("NORECV\n")


int dsp_upload_ucode(dsp_ucode_t ucode)
{
	int waitcount;
	u32 mail;
	u16 flags = read16(DSP_STATUS);
	if(flags & DSP_FLAG_HALTDSP)	/* We can't upload when it's halted. */
		return 0;
	DO_INBOXWAIT;
	mail = dsp_recv_mail();
	if(mail != DSP_UCODE_FEED) {
		gecko_printf("Uhh, what? We wanted the feed command, not %08X\n", mail);
	}
	dsp_send_mail(DSP_UCODE_IRAM_SRC);
	DO_DSPWAIT;
	dsp_send_mail(virt_to_phys(ucode.iram_src) & ~0x1F);	/* Gotta make sure it's aligned */
	DO_DSPWAIT;
	dsp_send_mail(DSP_UCODE_IRAM_DST);
	DO_DSPWAIT;
	dsp_send_mail(ucode.iram_dst);
	DO_DSPWAIT;
	dsp_send_mail(DSP_UCODE_IRAM_LEN);
	DO_DSPWAIT;
	dsp_send_mail(ucode.iram_len & ~0x1F);	/* Gotta make sure it's aligned */
	DO_DSPWAIT;
#if 1
	dsp_send_mail(DSP_UCODE_DRAM_SRC);
	DO_DSPWAIT;
	dsp_send_mail(virt_to_phys(ucode.dram_src) & ~0x1F);	/* Gotta make sure it's aligned */
	DO_DSPWAIT;
	dsp_send_mail(DSP_UCODE_DRAM_DST);
	DO_DSPWAIT;
	dsp_send_mail(ucode.dram_dst);
	DO_DSPWAIT;
#endif
	dsp_send_mail(DSP_UCODE_DRAM_LEN);
	DO_DSPWAIT;
	dsp_send_mail(ucode.dram_len & ~0x1F);	/* Gotta make sure it's aligned */
	DO_DSPWAIT;
	
	dsp_send_mail(DSP_UCODE_START_PC);
	DO_DSPWAIT;
	dsp_send_mail(ucode.pc);
	DO_DSPWAIT;
	
	return 1;
}

BOOL dsp_halt(void)
{
	u16 flags = read16(DSP_STATUS);
	/* Halt, fiend! */
	set16(DSP_STATUS, DSP_FLAG_HALTDSP);
	return (flags & DSP_FLAG_HALTDSP) ? TRUE : FALSE;
}

BOOL dsp_unhalt(void)
{
	u16 flags = read16(DSP_STATUS);
	clear16(DSP_STATUS, DSP_FLAG_HALTDSP);
	return (flags & DSP_FLAG_HALTDSP) ? TRUE : FALSE;
}

void dsp_reset(void)
{
	mask16(DSP_STATUS, DSP_FLAG_AIINT | DSP_FLAG_ARAMINT | DSP_FLAG_DSPINT, DSP_FLAG_RESETDSP | DSP_FLAG_RESET);
	while(read16(DSP_STATUS) & DSP_FLAG_RESET);
}

int dsp_initialize(void)
{
	char tmp[128];
	
	while(!(read16(DSP_ARAM_MODE) & 1));
	
	memcpy(tmp, phys_to_virt(0x01000000), 128);
	memcpy(phys_to_virt(0x01000000), _dsp_initcode, 128);
	/* TODO: Clean this up! */
	gecko_printf("ARAM Size\n");
	write16(DSP_ARAM_SIZE, 0x43);
	
	/* Reset DSP and all interrupts */
	gecko_printf("Reset1\n");
	write16(DSP_STATUS, DSP_FLAG_HALTDSP | DSP_FLAG_RESETDSP | DSP_FLAG_AIINT | DSP_FLAG_ARAMINT | DSP_FLAG_DSPINT);
	set16(DSP_STATUS, DSP_FLAG_RESET);
	while(read16(DSP_STATUS) & DSP_FLAG_RESET);
	
	gecko_printf("Mailbox\n");
	write16(DSP_MAILBOX_IN_H, 0);
	while(dsp_recv_mail() & 0x80000000);
	
	gecko_printf("ARAM Copy\n");
	set16(DSP_STATUS, DSP_FLAG_ARAMINT);
	write32(DSP_ARAM_DMA_MEMADDR_H, 0x01000000);
	write32(DSP_ARAM_DMA_ARAMADDR_H, 0x00000000);
	write32(DSP_ARAM_DMA_SIZE_H, 32);
	while((read16(DSP_STATUS) & DSP_FLAG_ARAMINT));
	tickdelay(2195); /* maybe we can change this to something else? */

	gecko_printf("ARAM Copy2\n");
	set16(DSP_STATUS, DSP_FLAG_ARAMINT);
	write32(DSP_ARAM_DMA_MEMADDR_H, 0x01000000);
	write32(DSP_ARAM_DMA_ARAMADDR_H, 0x00000000);
	write32(DSP_ARAM_DMA_SIZE_H, 32);
	while((read16(DSP_STATUS) & DSP_FLAG_ARAMINT));
	
	gecko_printf("Reset2\n");
	clear16(DSP_STATUS, DSP_FLAG_RESETDSP);
	while(read16(DSP_STATUS) & (1 << 10));	/* wat? */

	gecko_printf("Unhalt DSP\n");
	clear16(DSP_STATUS, DSP_FLAG_HALTDSP);
	while(read16(DSP_STATUS) & (1 << 15));	/* wat? */
	
	gecko_printf("Reset3\n");
	set16(DSP_STATUS, DSP_FLAG_HALTDSP);
	write16(DSP_STATUS, DSP_FLAG_HALTDSP | DSP_FLAG_RESETDSP | DSP_FLAG_AIINT | DSP_FLAG_ARAMINT | DSP_FLAG_DSPINT);
	set16(DSP_STATUS, DSP_FLAG_RESET);
	while(read16(DSP_STATUS) & DSP_FLAG_RESET);
	
	memcpy(phys_to_virt(0x01000000), tmp, 128);
	while(!(read16(DSP_ARAM_MODE) & 1));
	if(!irq_bw_register_handler(IRQ_BW_DSP, _dsp_irq_handler, NULL))
		return 0;
	set16(DSP_STATUS, DSP_FLAG_RESET);
	dsp_unhalt();
	irq_dsp_enable();
	irq_bw_enable(IRQ_BW_DSP);
	return 1;
}

int dsp_shutdown(void)
{
	dsp_halt();
	irq_dsp_disable();
	irq_bw_disable(IRQ_BW_DSP);
	if(!irq_bw_register_handler(IRQ_BW_DSP, NULL, NULL))
		return 0;
	return 1;
}

