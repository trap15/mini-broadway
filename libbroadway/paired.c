/*
	libbroadway - A general purpose library to control the Wii.
	Paired Singles manipulation

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <types.h>
#include <broadway.h>

#define mfgqr(g, t) do { \
	switch(g) { \
		case 0: \
			t = mfspr(SPR_GQR0); \
			break; \
		case 1: \
			t = mfspr(SPR_GQR1); \
			break; \
		case 2: \
			t = mfspr(SPR_GQR2); \
			break; \
		case 3: \
			t = mfspr(SPR_GQR3); \
			break; \
		case 4: \
			t = mfspr(SPR_GQR4); \
			break; \
		case 5: \
			t = mfspr(SPR_GQR5); \
			break; \
		case 6: \
			t = mfspr(SPR_GQR6); \
			break; \
		case 7: \
			t = mfspr(SPR_GQR7); \
			break; \
	} \
} while(0)

#define mtgqr(g, t) do { \
	switch(g) { \
		case 0: \
			mtspr(SPR_GQR0, t); \
			break; \
		case 1: \
			mtspr(SPR_GQR1, t); \
			break; \
		case 2: \
			mtspr(SPR_GQR2, t); \
			break; \
		case 3: \
			mtspr(SPR_GQR3, t); \
			break; \
		case 4: \
			mtspr(SPR_GQR4, t); \
			break; \
		case 5: \
			mtspr(SPR_GQR5, t); \
			break; \
		case 6: \
			mtspr(SPR_GQR6, t); \
			break; \
		case 7: \
			mtspr(SPR_GQR7, t); \
			break; \
	} \
} while(0)

void paired_set_gqr(int gqr, int mode, gqr_type_t type, int scale)
{
	u32 gqr_val = 0;
	mode %= 2;
	mode <<= 4;
	type %= 8; /* 8 types */
	gqr %= 8; /* Only 8 GQRs */
	mfgqr(gqr, gqr_val);
	gqr_val &= ~(0x3F07 << mode); /* Mask out the current value */
	gqr_val |= type << mode;
	scale &= 0x3F;
	gqr_val |= scale << (mode + 8);
	mtgqr(gqr, gqr_val);
}

void paired_get_gqr(int gqr, int mode, gqr_type_t *type, int *scale)
{
	u32 gqr_val = 0;
	mode %= 2;
	mode <<= 4;
	gqr %= 8; /* Only 8 GQRs */
	mfgqr(gqr, gqr_val);
	*type = (gqr_val & (7 << mode)) >> mode;
	*scale = (gqr_val & (0x3F00 << mode)) >> (mode + 8);
	*scale = ((*scale & 0x3F) ^ 0x20) - 0x20; /* Sign extend the 6-bit # */
}

