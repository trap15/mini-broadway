/*
	libbroadway - A general purpose library to control the Wii.
	GPIO handling

Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2009		bLAStY <blasty@bootmii.org>
Copyright (C) 2009-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>
#include <string.h>

static int _gpio1_irq_handler(u32 irq, void* data)
{
	u32 irq_flag = read32(HW_GPIO1_INTFLAG);
	
	write32(HW_GPIO1B_INTFLAG, irq_flag);
	(void)irq;
	(void)data;
	return 1;
}

int gpio_initialize(void)
{
	/* Set all GPIO1 owners to PowerPC */
	set32(HW_GPIO1_OWNER,           0xFFFFFF);
	irq_hw_register_handler(IRQ_HW_GPIO1B, _gpio1_irq_handler, NULL);
	irq_hw_enable(IRQ_HW_GPIO1B);
	/* Enable interrupts for GPIO1 input pins */
	set32(HW_GPIO1B_INTENABLE,      0xFFFFFF);
	set32(HW_GPIO1B_INTLVL,         0xFFFFFF);
	return 1;
}

int gpio_shutdown(void)
{
	irq_hw_disable(IRQ_HW_GPIO1B);
	irq_hw_register_handler(IRQ_HW_GPIO1B, NULL, NULL);
	return 1;
}

gpio_pin_t gpio_read(void)
{
	return read32(HW_GPIO1B_IN);
}

void gpio_write(gpio_pin_t gpios)
{
	return write32(HW_GPIO1B_OUT, gpios);
}

void gpio_set(gpio_pin_t gpios)
{
	gpio_write(gpio_read() | gpios);
}

void gpio_clear(gpio_pin_t gpios)
{
	gpio_write(gpio_read() & ~gpios);
}

void gpio_mask(gpio_pin_t setgpios, gpio_pin_t clrgpios)
{
	gpio_write((gpio_read() & ~clrgpios) | setgpios);
}

void gpio_switch(gpio_pin_t gpios)
{
	gpio_write(gpio_read() ^ gpios);
}

int gpio_direction(gpio_pin_t pin, int dir)
{
	u32 oldreg = read32(HW_GPIO1B_DIR) & pin;
	int olddir = 0;
	if(oldreg == pin)
		olddir = 1;
	else
		olddir = -1;
	switch(dir) {
		case -1:
			clear32(HW_GPIO1B_DIR, pin);
			break;
		case 0:
			break;
		case 1:
			set32(HW_GPIO1B_DIR, pin);
			break;
	}
	return olddir;
}


