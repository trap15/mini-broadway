/*
	libbroadway - A general purpose library to control the Wii.
	C startup code

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdlib.h>
#include <string.h>
#include <types.h>

#include <broadway.h>
#include <input.h>

BOOL newlib_init(void);
void newlib_fini(void);
int main(int argc, char *argv[]);

int cstart(int argc, char *argv[])
{
	int ret;
	if(!newlib_init()) {
		return EXIT_FAILURE;
	}
	exception_init();
	dsp_reset();
	irq_initialize();
	ipc_initialize();
	ipc_slowping();
	exi_initialize();
	gecko_initialize();
	gecko_printf("Booting PPC now!\n");
	input_initialize();
	irq_bw_enable(IRQ_BW_HW);
	irq_hw_enable(IRQ_HW_PPCIPC);

	ret = main(argc, argv);

	irq_hw_disable(IRQ_HW_PPCIPC);
	irq_bw_disable(IRQ_BW_HW);
	input_shutdown();
	gecko_shutdown();
	exi_shutdown();
	ipc_shutdown();
	irq_shutdown();
	newlib_fini();
	return ret;
}

