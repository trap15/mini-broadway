/*
	libbroadway - A general purpose library to control the Wii.
	Visual text console

Copyright (C) 2009		bLAStY <blasty@bootmii.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <types.h>
#include <broadway.h>
#include <video_low.h>
#include <console.h>
#include <string.h>
#include <malloc.h>
#include "../libnewintf/newintf.h"

#include <stdarg.h>

typedef struct {
        u32	x, y;
        u32	width, height;
        u8*	raw_data;
        u32**	yuv_data;  
        u32	active;
        u8	has_alpha;
} gfx_rect;

typedef struct {
	int	con_x;
	int	con_y;
	int	con_w;
	int	con_h;
	int	fnt_w;
	int	fnt_h;
	u8*	rawfnt;
	u32**	fnt[256];
	u32*	fb;
	int	y_add;
	int	x, y;
	int	fgc;
	int	bgc;
} console_t;

static console_t _current_console;
extern unsigned char console_font_8x16[];

/* xterm's color scheme. */
static int _colors[16 * 3] = {
	0x00, 0x00, 0x00,	/* Black */
	0xCD, 0x00, 0x00,	/* Red */
	0x00, 0xCD, 0x00,	/* Green */
	0xCD, 0xCD, 0x00,	/* Yellow */
	0x00, 0x00, 0xEE,	/* Blue */
	0xCD, 0x00, 0xCD,	/* Magenta */
	0x00, 0xCD, 0xCD,	/* Cyan */
	0xE5, 0xE5, 0xE5,	/* White */
	/* Bright */
	0x7F, 0x7F, 0x7F,	/* Black */
	0xFF, 0x00, 0x00,	/* Red */
	0x00, 0xFF, 0x00,	/* Green */
	0xFF, 0xFF, 0x00,	/* Yellow */
	0x5C, 0x5C, 0xFF,	/* Blue */
	0xFF, 0x00, 0xFF,	/* Magenta */
	0x00, 0xFF, 0xFF,	/* Cyan */
	0xFF, 0xFF, 0xFF,	/* White */
};

void console_set_framebuffer(void* fb)
{
	_current_console.fb = fb;
}

long _console_write(struct _reent* r, int fd, const void* buf, size_t cnt)
{
	size_t i;
	(void)r;
	(void)fd;
	print_str(buf, cnt);
	for(i = 0; i < cnt; i++) {
		gecko_printf("%c", ((char*)buf)[i]);
	}
	return cnt;
}

newlib_device_t console_device = {
	0, 0,
	"stdout", /* Tag */
	TRUE, /* Is TTY */
	NULL, /* open */
	NULL, /* close */
	NULL, /* lseek */
	NULL, /* read */
	_console_write, /* write */
	NULL, /* stat */
	NULL, /* fstat */
	NULL, /* link */
	NULL, /* unlink */
};

static void omemcpy32(u32 *dst, u32 *src, u32 count)
{
	while(count--) {
		*dst = *src;
		sync_after_write((const void *)dst, 4);
		
		dst++;
		src++;
	}
}

static void omemset32(u32 *dst, u32 setval, u32 count)
{
	while(count--) {
		*dst = setval;
		sync_after_write((const void *)dst, 4);
		
		dst++;
	}
}

static void font_to_yuv(u8* f, int w, int h, u32** out[256], int fgc, int bgc)
{
	int i, x, y;
	u8 lr,lg,lb, rr,rg,rb;
	
	for(i = 0; i < 256; i++) {
		out[i] = malloc(h * sizeof(u32*));
		
		for(y = 0; y < h; y++) {
			out[i][y] = malloc(w * 4);
			for(x = 0; x < w; x += 2) {
				if((f[(i * h) + y] >> (w - 1 - x)) & 0x01) {
					lr = _colors[fgc * 3 + 0];
					lg = _colors[fgc * 3 + 1];
					lb = _colors[fgc * 3 + 2];
				}else{
					lr = _colors[bgc * 3 + 0];
					lg = _colors[bgc * 3 + 1];
					lb = _colors[bgc * 3 + 2];
				}
				
				if((f[(i * h) + y] >> (w - (x + 1))) & 0x01) {
					rr = _colors[fgc * 3 + 0];
					rg = _colors[fgc * 3 + 1];
					rb = _colors[fgc * 3 + 2];
				}else{
					rr = _colors[bgc * 3 + 0];
					rg = _colors[bgc * 3 + 1];
					rb = _colors[bgc * 3 + 2];
				}
				
				out[i][y][x>>1] = make_yuv(lr, lg, lb, rr, rg, rb);
			}
		}
	}
}

void console_initialize(int vmode, void* fb, int x, int y, int w, int h, int fw, int fh, u8 font[])
{
	u32* f = fb;
	_current_console.rawfnt = font;
	_current_console.fgc = 15;
	_current_console.bgc = 0;
	font_to_yuv(_current_console.rawfnt, fw, fh, _current_console.fnt,
		    _current_console.fgc, _current_console.bgc);
	
	_current_console.con_x = x;
	_current_console.con_y = y;
	_current_console.con_w = w;
	_current_console.con_h = h;
	_current_console.fnt_w = fw + 2;
	_current_console.fnt_h = fh + 1;

	switch(vmode) {
		default:
		case VIDEO_640X480_NTSCi_YUV16:
		case VIDEO_640X480_PAL60_YUV16:
		case VIDEO_640X480_NTSCp_YUV16:
			_current_console.y_add = 0;
			break;
			
		case VIDEO_640X528_PAL50_YUV16:
			_current_console.y_add = 48;
			break;
	}
	_current_console.x = 0;
	_current_console.y = 0;
	_current_console.fb = fb;
	
	if((_current_console.con_w * _current_console.fnt_w) > (640 - _current_console.x)) {
		_current_console.con_w = (640 - _current_console.x) / _current_console.fnt_w;
	}
	if((_current_console.con_h * _current_console.fnt_h) > (480 - _current_console.y + _current_console.y_add)) {
		_current_console.con_h = (480 - _current_console.y + _current_console.y_add) / _current_console.fnt_h;
	}
	f += _current_console.con_x >> 1; /* Offset the X for simplification. */
	/* Precalc copy width */
	w = _current_console.con_w * _current_console.fnt_w;
	f += _current_console.con_y * 320;
	for(y = 0; y < ((_current_console.con_h + 1) * _current_console.fnt_h); y++) {
		omemset32(f, make_yuv(0,0,0, 0,0,0), w >> 1);
		f += 640 >> 1;
	}
	if(!newlib_add_device(console_device)) {
		return;
	}
}

void console_simple_init(int vmode, void* fb)
{
	console_initialize(vmode, fb, 16, 20, 76, 25, 8, 16, console_font_8x16);
}

static void gfx_draw_rect(gfx_rect *d_rect)
{
        u32 y;
        u32 *fb = _current_console.fb;

        fb += (d_rect->y * (640 >> 1));
        fb += (d_rect->x >> 1);

        for(y = 0; y < d_rect->height; y++) {
                omemcpy32(fb, d_rect->yuv_data[y], d_rect->width >> 1);
                fb += (640 >> 1);
        }
}

static void scroll(void)
{
	int y;
	unsigned int w, startoff;
	u32 *fb = _current_console.fb;
	fb += _current_console.con_x >> 1; /* Offset the X for simplification. */
	/* Precalc copy width */
	w = _current_console.con_w * _current_console.fnt_w;
	startoff = _current_console.fnt_h * 640/2;
	
	fb += startoff;
	fb += (_current_console.con_y) * 320;
	
	for(y = 0; y < (_current_console.con_h * _current_console.fnt_h); y++) {
		omemcpy32(fb - startoff, fb, w >> 1);
		fb += 640 >> 1;
	}
	for(y = 0; y < _current_console.fnt_h; y++) {
		omemset32(fb - startoff, make_yuv(0,0,0, 0,0,0), w >> 1);
		fb += 640 >> 1;
	}
	/* Just sync the whole thing. */
	sync_after_write(_current_console.fb, 640 * (480 + _current_console.y_add));
}

static void _screen_attribs(int p, int params[])
{
	int light = 0;
	int i;
	if(p == 0) {
		_current_console.fgc = 15;
		_current_console.bgc =  0;
	}
	for(i = 0; i < p; i++) {
		switch(params[i]) {
			case 0:
				_current_console.fgc = 15;
				_current_console.bgc =  0;
				light = 0;
				break;
			case 1:
				light = 1;
				break;
			case 22:
				light = 0;
				break;
			case 30:
			case 31:
			case 32:
			case 33:
			case 34:
			case 35:
			case 36:
			case 37:
				_current_console.fgc = (params[i] - 30) + (light ? 8 : 0);
				break;
			case 39:
				_current_console.fgc = 15;
				break;
			case 40:
			case 41:
			case 42:
			case 43:
			case 44:
			case 45:
			case 46:
			case 47:
				_current_console.bgc = (params[i] - 40) + (light ? 8 : 0);
				break;
			case 49:
				_current_console.bgc = 0;
				break;
		}
	}
	font_to_yuv(_current_console.rawfnt, _current_console.fnt_w - 1, _current_console.fnt_h - 1,
		    _current_console.fnt, _current_console.fgc, _current_console.bgc);
}

static void _escape_code(unsigned int *i, const char* data)
{
#define PARAM_COUNT	(3)	/* We shouldn't need more than 3 params, yeah? */
	int params[PARAM_COUNT];
	int p;
	int diff = 0;
	char ch;
	int tmp;
	u32* fb = _current_console.fb;
	params[0] = 1;
	params[1] = 1;
	params[2] = 1;
	for(p = 0; p < PARAM_COUNT; p++) {
		while((data[diff] >= '0') && (data[diff] <= '9')) {
			params[p] *= 10;
			params[p] += data[diff++] - '0';
		}
		if(data[diff] == ';') {	/* Delimiter */
			diff++;
		}
		if((data[diff] < '0') || (data[diff] > '9')) /* Out of params */
			break;
	}
	ch = data[diff++];
	switch(ch) {
		case 'A':
			_current_console.y -= params[0];
			break;
		case 'B':
			_current_console.y += params[0];
			break;
		case 'C':
			_current_console.x += params[0];
			break;
		case 'D':
			_current_console.x -= params[0];
			if(_current_console.x < 0)
				_current_console.x = 0;
			break;
		case 'E':
			_current_console.y += params[0];
			_current_console.x = 0;
			break;
		case 'F':
			_current_console.y -= params[0];
			if(_current_console.y < 0)
				_current_console.y = 0;
			_current_console.x = 0;
			break;
		case 'G':
			if(p == 0)
				break;
			_current_console.x = params[0];
			break;
		case 'J':
//			if(params[0] != 2)
//				break;
			for(tmp = 0; tmp < ((_current_console.con_h + 1) * _current_console.fnt_h); tmp++) {
				omemset32(fb, make_yuv(0,0,0, 0,0,0), (_current_console.con_w * _current_console.fnt_w) >> 1);
				fb += 640 >> 1;
			}
			_current_console.x = 0;
			_current_console.y = 0;
			break;
		case 'f':
		case 'H':
			_current_console.y = params[0] - 1;
			_current_console.x = params[1] - 1;
			break;
		case 'S':
			scroll();
			_current_console.x = 0;
			break;
		case 'm':
			_screen_attribs(p, params);
			break;
	}
	if(_current_console.y < 0)
		_current_console.y = 0;
	if(_current_console.y >= _current_console.con_h)
		_current_console.y = _current_console.con_h - 1;
	if(_current_console.x < 0)
		_current_console.x = 0;
	if(_current_console.x >= _current_console.con_w)
		_current_console.x = _current_console.con_w - 1;
	*i += diff;
}

void print_str(const char *str, size_t len)
{
	unsigned int i;
	gfx_rect d_char;

	d_char.width  = _current_console.fnt_w - 2;
	d_char.height = _current_console.fnt_h - 1;

	for (i = 0; i < len; i++) {
		if(_current_console.x > _current_console.con_w) {
			_current_console.y++;
			_current_console.x = 0;
		}
		if(_current_console.y > _current_console.con_h) {
			scroll();
			_current_console.y--;
		}
		switch(str[i]) {
			case '\n':
				_current_console.y++;
				_current_console.x = 0;
				break;
			case '\t':
				_current_console.x %= 8;
				_current_console.x += 8;
				break;
			case '\r':
				/* Hurr CR */
				break;
			case '\x1B':
				if((i + 1) < len) {
					if(str[i + 1] == '[') {
						i++;
						_escape_code(&i, &(str[i + 1]));
					}
				}
				break;
			default:
				d_char.y = _current_console.con_y + (_current_console.y * _current_console.fnt_h);
				d_char.x = _current_console.con_x + (_current_console.x * _current_console.fnt_w);
				d_char.yuv_data = _current_console.fnt[(int)(str[i])];
				gfx_draw_rect(&d_char);
				_current_console.x++;
				break;
		}
	}
}

__attribute__((deprecated)) int gfx_printf(const char *fmt, ...)
{
	va_list args;
	char *buffer;
	int i;

	va_start(args, fmt);
	i = vasprintf(&buffer, fmt, args);
	va_end(args);

	print_str(buffer, i);
	gecko_printf("%s", buffer);

	return i;
}

