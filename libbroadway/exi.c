/*
	libbroadway - A general purpose library to control the Wii.
	EXI support

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <string.h>
#include <types.h>
#include <broadway.h>

typedef struct {
	irq_handler_t	handler[3];
	u32		flags;
} _exi_t;

static _exi_t _exis[3];

void irq_exi_enable(u32 irq)
{
	int chan;
	irq %= IRQ_EXI_MAX;
	chan = irq / 3;
	irq %= 3;
	/* Clear previous int status */
	set32(EXI_CSR(chan), 3 << ((irq == 0) ? 0 : ((irq == 1) ? 2 : 10)));
}

void irq_exi_disable(u32 irq)
{
	int chan;
	irq %= IRQ_EXI_MAX;
	chan = irq / 3;
	irq %= 3;
	clear32(EXI_CSR(chan), 1 << ((irq == 0) ? 0 : ((irq == 1) ? 2 : 10)));
}

int irq_exi_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data)
{
	int chan;
	irqn %= IRQ_EXI_MAX;
	chan = irqn / 3;
	irqn %= 3;
	_exis[chan].handler[irqn].exec = exec;
	_exis[chan].handler[irqn].data = data;
	return 1;
}

irq_handler_t irq_exi_get_handler(u32 irqn)
{
	int chan;
	irqn %= IRQ_EXI_MAX;
	chan = irqn / 3;
	irqn %= 3;
	return _exis[chan].handler[irqn];
}

static void _exi_exi_handler(int chan, int dev)
{
	if(_exis[chan].handler[0].exec)
		(_exis[chan].handler[0].exec)(chan * 3 + dev, _exis[chan].handler[0].data);
}

static void _exi_tc_handler(int chan, int dev)
{
	if(_exis[chan].handler[1].exec)
		(_exis[chan].handler[1].exec)(chan * 3 + dev, _exis[chan].handler[1].data);
}

static void _exi_ext_handler(int chan, int dev)
{
	if(_exis[chan].handler[2].exec)
		(_exis[chan].handler[2].exec)(chan * 3 + dev, _exis[chan].handler[2].data);
}

static void _exichan_irq_handler(int chan)
{
	u32 csr;
	csr = read32(EXI_CSR(chan));
	if((csr & (1 << 1)) && (csr & (1 << 0))) {
		set32(EXI_CSR(chan), 1 << 1);
		_exi_exi_handler(chan, (csr >> 7) & 7);
	}
	if((csr & (1 << 3)) && (csr & (1 << 2))) {
		set32(EXI_CSR(chan), 1 << 3);
		_exi_tc_handler(chan, (csr >> 7) & 7);
	}
	if((csr & (1 << 11)) && (csr & (1 << 10))) {
		set32(EXI_CSR(chan), 1 << 11);
		_exi_ext_handler(chan, (csr >> 7) & 7);
	}
}

static int _exi_irq_handler(u32 irq, void* data)
{
	int i;
	for(i = 0; i < 3; i++) {
		_exichan_irq_handler(i);
	}
	(void)irq;
	(void)data;
	return 1;
}

int exi_initialize(void)
{
	write32(EXI_CSR(0), 0x2000); /* Turn off ROM de-scramble logic */
	write32(EXI_CSR(0), 0);
	write32(EXI_CSR(1), 0);
	write32(EXI_CSR(2), 0);
	if(!irq_bw_register_handler(IRQ_BW_EXI, _exi_irq_handler, NULL))
		return 0;
	irq_bw_enable(IRQ_BW_EXI);
	return 1;
}

int exi_shutdown(void)
{
	irq_bw_disable(IRQ_BW_EXI);
	if(!irq_bw_register_handler(IRQ_BW_EXI, NULL, NULL))
		return 0;
	return 1;
}

BOOL exi_set_lock(BOOL locked)
{
	BOOL waslock;
	waslock = (read32(SI_EXI_LOCK) & (1 << 31)) ? TRUE : FALSE;
	write32(SI_EXI_LOCK, locked << 31);
	return waslock;
}

BOOL exi_set_clock(int chan, exi_clock_t clk)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	mask32(EXI_CSR(chan), 7 << 4, clk << 4);
	return TRUE;
}

BOOL exi_device_select(int chan, int dev)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	if((dev < 0) || (dev > 2)) return FALSE;
	set32(EXI_CSR(chan), 1 << (7 + dev));
	return TRUE;
}

BOOL exi_device_deselect(int chan, int dev)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	if((dev < 0) || (dev > 2)) return FALSE;
	clear32(EXI_CSR(chan), 1 << (7 + dev));
	return TRUE;
}

BOOL exi_device_connected(int chan)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	return (read32(EXI_CSR(chan)) & (1 << 12)) ? TRUE : FALSE;
}

BOOL exi_set_dma_address(int chan, u32 addr)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	if(addr & 0x1F) return FALSE;
	addr &= ~0xFC000000;
	write32(EXI_MAR(chan), addr);
	return TRUE;
}

BOOL exi_set_dma_length(int chan, u32 len)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	if(len & 0xFC00001F) return FALSE;
	write32(EXI_LENGTH(chan), len);
	return TRUE;
}

BOOL exi_set_immediate_length(int chan, int len)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	if((len < 1) || (len > 4)) return FALSE;
	mask32(EXI_CR(chan), 3 << 4, (len - 1) << 4);
	return TRUE;
}

BOOL exi_transfer(int chan, exi_transfer_t type, BOOL dma)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	mask32(EXI_CR(chan), 3 << 2, type << 4);
	mask32(EXI_CR(chan), 1 << 1, (dma & 1) << 1);
	set32(EXI_CR(chan), 1);
	return TRUE;
}

BOOL exi_transfer_ended(int chan)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	return (read32(EXI_CR(chan)) & 1) ? FALSE : TRUE;
}

u32 exi_immediate_recv(int chan)
{
	if((chan < 0) || (chan > 2)) return 0;
	return read32(EXI_DATA(chan));
}

BOOL exi_immediate_send(int chan, u32 data)
{
	if((chan < 0) || (chan > 2)) return FALSE;
	write32(EXI_DATA(chan), data);
	return TRUE;
}


