/*
	libbroadway - A general purpose library to control the Wii.
	Exception handler

Copyright (C) 2008		Segher Boessenkool <segher@kernel.crashing.org>
Copyright (C) 2009		Bernhard Urban <lewurm@gmx.net>
Copyright (C) 2009		Sebastian Falbesoner <sebastian.falbesoner@gmail.com>
Copyright (C) 2009-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>

#include <broadway.h>
#include <console.h>
#include <string.h>

extern char exception_2200_start, exception_2200_end;

void exception_restorer(void) __attribute__ ((noreturn));
void exception_finish(void) __attribute__ ((noreturn));
void exception_handler(int exception) __attribute__ ((noreturn));

void irq_handler(void);
volatile except_handler exception_handler_table[0x10] = {
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};

typedef struct _framerec {
	struct _framerec *up;
	void *lr;
} frame_rec, *frame_rec_t;

void exception_handler(int exception)
{
	if((exception & ~0xF00) == 0) {
		if(exception_handler_table[(exception >> 8) & 0xF] != NULL) {
			exception_handler_table[(exception >> 8) & 0xF](exception);
			exception_finish();
		}
	}
	switch(exception) {
		case 0x500: /* External IRQ */
			irq_handler();
			break;
		case 0x900: /* Decrementer */
			break;
		default: {
			u32 *x;
			u64 *dx;
			u32 i;
			for(i = 0; i < 25; i++)
				printf("\n");
			printf("\nException %04X occurred!\n", exception);
			x = (u32 *)0x80002000;
			void *pc, *lr, *r1;
			r1 = (void*)(x[1]); lr = (void*)(x[0x22]); pc = (void*)(x[0x24]);
			printf("\n R0..R7    R8..R15  R16..R23  R24..R31\n");
			for(i = 0; i < 8; i++) {
				printf("%08lX  %08lX  %08lX  %08lX\n", x[0], x[8], x[16], x[24]);
				x++;
			}

			x = (u32 *)0x80002080;
			printf("\n CR/XER    LR/CTR  SRR0/SRR1 DAR/DSISR\n");
			for(i = 0; i < 2; i++) {
				printf("%08lX  %08lX  %08lX  %08lX\n", x[0], x[2], x[4], x[6]);
				x++;
			}

			dx = (u64 *)0x80002100;
			printf("\n FPR0..FPR7        FPR8..FPR15       FPR16..FPR23      FPR24..FPR31     \n");
			for(i = 0; i < 8; i++) {
				printf("%016llX  %016llX  %016llX  %016llX\n", dx[0], dx[8], dx[16], dx[24]);
				x++;
			}

			x = (u32 *)0x800020A0;
			printf("\n FPSCR    \n");
			printf("%08lX  \n", x[1]);

			register frame_rec_t l, p;
			l = (frame_rec_t)lr;
			p = r1;
#define STACK_DUMP_LEN 16
			printf("\nStack Dump:");
			for(i = 0; i < STACK_DUMP_LEN-1 && p->up; p = p->up, i++) {
				if(i % 4) printf(" --> ");
				else {
					if(i>0) printf(" -->\n\t");
					else printf("\n\t");
				}

				switch(i) {
					case 0:
						if(pc) printf("%p", pc);
						break;
					case 1:
						printf("%p", (void*)l);
						break;
					default:
						printf("%p", (void*)(p->up->lr));
						break;
				}
			}
			printf("\n");
			// Hang.
			for(;;);
		}
	}
	
	exception_finish();
	for(;;); /* Never going to happen, just stops GCC from complaining. */
}

void exception_init(void)
{
	u32 vector;
	u32 len_2200;

	for(vector = 0x100; vector < 0x2000; vector += 0x10) {
		u32 *insn = (u32 *)(0x80000000 + vector);
		insn[0] = 0xBC002000;			// stmw	0, 0x2000(0)
		insn[1] = 0x38600000 | (u32)vector;	// li	3, vector
		insn[2] = 0x48002202;			// ba	0x2200
		insn[3] = 0;
	}
	sync_before_exec((void *)0x80000100, 0x1F00);

	len_2200 = &exception_2200_end - &exception_2200_start;
	memcpy((void *)0x80002200, &exception_2200_start, len_2200);
	sync_before_exec((void *)0x80002200, len_2200);
}

