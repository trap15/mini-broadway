/*
	libbroadway - A general purpose library to control the Wii.
	Input handling

Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2009		bLAStY <blasty@bootmii.org>
Copyright (C) 2009-2011		Alex Marshall <trap15@raidenii.net>
Copyright (C) 2010		Ian Callaghan "unrom" <ian@unrom.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <broadway.h>
#include <input.h>
#include <string.h>

#define PAD_CMDWORD_MAKE(cmd, arg0, arg1)	((cmd) << 16 | (arg0) << 8 | (arg1))
#define SI_STATUS_SEND				(1 << 31)
#define SI_STATUS_BUSY				(1 << 29)

static gc_pad_t _pad;

static void gcpad_init(void)
{
	write32(SI_OUTBUF_0,  PAD_CMDWORD_MAKE(0x40, 0x03, 0x00)); /* Enable read on all GC pads */
	write32(SI_OUTBUF_1,  PAD_CMDWORD_MAKE(0x40, 0x03, 0x00));
	write32(SI_OUTBUF_2,  PAD_CMDWORD_MAKE(0x40, 0x03, 0x00));
	write32(SI_OUTBUF_3,  PAD_CMDWORD_MAKE(0x40, 0x03, 0x00));
	write32(SI_POLL,      0x000701f0); // enable poll chan 1, X = 7, Y = 1
	write32(SI_STATUS,    SI_STATUS_SEND); // transfer all buffer
	while(!(read32(SI_STATUS) & SI_STATUS_BUSY));
}

int input_initialize(void)
{
	memset(&_pad, 0, sizeof(gc_pad_t));
	gcpad_init();
	return 1;
}

int input_shutdown(void)
{
	/* Nothing to do here. */
	return 1;
}

u16 pad_read(gc_pad_t *pad, int chan)
{
	u32 pdata, pdata2;
	u16 btns = 0;
	if(chan == GCPAD_ALL) {
		btns   |= pad_read(&_pad, GCPAD_3);
		btns   |= pad_read(&_pad, GCPAD_2);
		btns   |= pad_read(&_pad, GCPAD_1);
		btns   |= pad_read(&_pad, GCPAD_0);
		if(pad) {
			u16 prev = pad->btns_held;
			
			pad->btns_held = btns;
			pad->btns_up = prev & ~btns;
			pad->btns_down = btns & (btns ^ prev);
			/* Just give pad 0's values... */
			pad->x = _pad.x;
			pad->y = _pad.y;
			
			pad->cx = _pad.cx;
			pad->cy = _pad.cy;
			pad->l = _pad.l;
			pad->r = _pad.r;
			
			return pad->btns_down;
		}
	}else{
		pdata = read32(SI_INBUFA(chan));
		pdata2 = read32(SI_INBUFB(chan));
		btns = pdata >> 16;
		if(pad) {
			u16 prev = pad->btns_held;
			
			pad->btns_held = btns;
			pad->btns_up = prev & ~btns;
			pad->btns_down = btns & (btns ^ prev);
			
			pad->x = 128 + ((pdata >> 8) & 0xff);
			pad->y = 128 - (pdata & 0xff);
			
			pad->cx = 128 + (pdata2 >> 24);
			pad->cy = 128 - ((pdata2 >> 16) & 0xff);
			pad->l = (pdata2 >> 8) & 0xff;
			pad->r = pdata2 & 0xff;
			
			return pad->btns_down;
		}
	}
	return btns;
}

void pad_rumble(int chan, BOOL on)
{
	write32(SI_OUTBUF(chan), PAD_CMDWORD_MAKE(0x40, 0x00, on ? 0x01 : 0x00));
	write32(SI_STATUS,       SI_STATUS_SEND);
	while(!(read32(SI_STATUS) & SI_STATUS_BUSY));
}

u16 input_wait(void)
{
	u16 res;

	do {
		udelay(2000);
		res = pad_read(&_pad, GCPAD_ALL);
	} while(!(res & PAD_ANY));

	return res;
}

