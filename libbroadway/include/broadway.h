/*
	libbroadway - A general purpose library to control the Wii.
	General include

Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \mainpage mini-broadway documentation
 *
 * \section intro_sec Introduction
 *
 * mini-broadway is an SDK (Software Development Kit) for the Nintendo Wii. It
 * relies upon MINI (MINI Is Not IOS) for several services, and as such only works
 * under BootMii (IOS or boot2, doesn't matter).
 *
 * \section install_sec Installation
 *
 * Make sure to set $WIIDEV to somewhere meaningful (/opt/wiidev recommended),
 * then run <pre>./buildit.sh all</pre>
 * After this has completed, run <pre>./makeall.sh</pre>
 * Your installation is now complete! Go write some apps!
 *
 * \section doc_categories API Documentation
 * 
 * \subsection libbroadway_doc libbroadway documentation
 * <ul>
 * <li><a href="./ai_8h.html">Audio Interface</a></li>
 * <li><a href="./di_8h.html">Drive Interface</a></li>
 * <li><a href="./dsp_8h.html">DSP Interface</a></li>
 * <li><a href="./exi_8h.html">EXI Interface</a></li>
 * <li><a href="./gpio_8h.html">GPIO Services</a></li>
 * <li><a href="./irq_8h.html">Interrupt Request Services</a></li>
 * <li><a href="./mi_8h.html">Memory Interface</a></li>
 * <li><a href="./paired_8h.html">Paired Singles wrappers</a></li>
 * <li><a href="./bootmii__ppc_8h.html">Low-level PowerPC Services</a></li>
 * <li><a href="./hollywood_8h.html">Hardware Register Definitions</a></li>
 * <li><a href="./console_8h.html">On-screen Console</a></li>
 * <li><a href="./gecko_8h.html">USBGecko Interface</a></li>
 * <li><a href="./input_8h.html">Input Handling</a></li>
 * <li><a href="./mini__ipc_8h.html">MINI Communication</a></li>
 * <li><a href="./lists_8h.html">Double-linked Lists Services</a></li>
 * <li><a href="./sha1_8h.html">SHA-1 Services</a></li>
 * <li><a href="./video__low_8h.html">Low-level Video Interface</a></li>
 * </ul>
 * 
 * \subsection libdiskmii_doc libdiskmii documentation
 * <ul>
 * <li><a href="./diskmii_8h.html">libdiskmii Interface</a></li>
 * <li><a href="./nandfs_8h.html">NAND FS Interface</a></li>
 * </ul>
 * 
 * \subsection libthreads_doc libthreads documentation
 * <ul>
 * <li><a href="./threads_8h.html">Threading Services</a></li>
 * <li><a href="./atomics_8h.html">Atomic Operation Services</a></li>
 * <li><a href="./locks_8h.html">Semaphore and Mutex Services</a></li>
 * </ul>
 * 
 * \subsection libnewintf_doc libnewintf documentation
 * <ul>
 * <li><a href="./newintf_8h.html">Newlib Interface</a></li>
 * </ul>
 * 
 * \section other_pages Other pages
 * <ul>
 * <li><a href="./bug.html">Bugs List</a></li>
 * <li><a href="./todo.html">TODO List</a></li>
 * </ul>
 */

/*! \file
 * Includes all of libbroadway for your convenience, as well as defines a nice
 * alignment macro.
 */

#ifndef __LIBBROADWAY_H__
#define __LIBBROADWAY_H__

#include <types.h>

#include <broadway/bootmii_ppc.h>
#include <broadway/hollywood.h>
#include <broadway/irq.h>
#include <broadway/ipc.h>
#include <broadway/mini_ipc.h>
#include <broadway/mi.h>
#include <broadway/exi.h>
#include <broadway/ai.h>
#include <broadway/di.h>
#include <broadway/paired.h>
#include <broadway/dsp.h>
#include <broadway/gpio.h>
#include <console.h>
#include <gecko.h>
#include <input.h>
#include <ipc_common.h>
#include <lists.h>
#include <sha1.h>
#include <video_low.h>

/*! \brief Aligns a part of memory
 *
 * \param n the byte-mark to align to.
 */
#define ATTRIBUTE_ALIGN(n)	__attribute__((aligned (n)))

#endif

