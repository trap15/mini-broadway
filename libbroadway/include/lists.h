/*
	libbroadway - A general purpose library to control the Wii.
	Double-linked lists implementation

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An efficient and expandable double-linked list implementation for any usage.
 */

#ifndef __LISTS_H__
#define __LISTS_H__

/*! The most basic linked-list node.
 *
 * All of your linked-lists used by the lists.h functions must look like this
 * (with extra data following)
 */
typedef struct linked_node_t {
	struct linked_node_t*	next;	/*!< Next element in list */
	struct linked_node_t*	prev;	/*!< Previous element in list */
} linked_node_t;

/*! \brief Walks a linked list
 *
 * Walks a linked list, checking for a condition, and saving the result in walk.
 * \param list the list to walk.
 * \param condition the condition to check each node for. Use walk as the node
 *                  name.
 * \return A node named walk must be existant in the function this is called from.
 *         The node that matches the condition will be stored in walk, or NULL if
 *         no matching node could be found.
 */
#define linked_walk(list, condition) do { \
	if((list) == NULL) { walk = NULL; break; } \
	for(walk = (list); (walk->next != NULL) && (condition); walk = walk->next); \
	if(!(condition)) walk = NULL; /* Our condition never happened, so we have no result */ \
} while(0)

/*! \brief Creates a list node
 *
 * Allocates a piece of memory with a specified size, and returns it as a node.
 * \param size the size of the nodes data (something like the size of your node
 *             might be a good idea).
 * \return A linked_node with the memory size specified.
 */
linked_node_t* linked_make(size_t size);
/*! \brief Adds a node to the beginning of a list
 *
 * \param l the list for the node to be added to.
 * \param n the node to be added to the list.
 * \return The added node.
 */
void* linked_add_first(void* l, void *n);
/*! \brief Adds a node to the end of a list
 *
 * \param l the list for the node to be added to.
 * \param n the node to be added to the list.
 * \return The added node.
 */
void* linked_add_last(void* l, void *n);
/*! \brief Adds a node before another in a list
 *
 * \param l the list for the node to be added to.
 * \param n the node to be added to the list.
 * \param node the node of which n should come after.
 * \return The added node.
 */
void* linked_add_before(void* l, void *n, void* node);
/*! \brief Adds a node after another in a list
 *
 * \param l the list for the node to be added to.
 * \param n the node to be added to the list.
 * \param node the node of which n should come before.
 * \return The added node.
 */
void* linked_add_after(void* l, void *n, void* node);
/*! \brief Deletes a node from a list
 *
 * \param l the list for the node to be added to.
 * \param node the node to be removed from the list.
 */
void linked_del(void* l, void* node);

#endif

