/*
	libbroadway - A general purpose library to control the Wii.
	Low-level video support

Copyright (C) 2008, 2009	Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2009		Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009		Sven Peter <svenpeter@gmail.com>
Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt

Some routines and initialization constants originally came from the
"GAMECUBE LOW LEVEL INFO" document and sourcecode released by Titanik
of Crazy Nation and the GC Linux project.
*/

/*! \file
 * A collection of low-level video functions allowing direct access to the XFB
 * (eXternal FrameBuffer) and to set up the rest of the video for output.
 * \todo Fix WaitVSync to actually do things properly.
 * \todo Change function names to not be so libogc-esque.
 */

#ifndef __VIDEO_LOW_H__
#define __VIDEO_LOW_H__

/*! An enumeration for the video modes supported
 */
typedef enum {
	VIDEO_640X480_NTSCi_YUV16 = 0,	/*!< Interlaced: YES, Video Type: NTSC, Refresh Rate: 60Hz, Size: 640x480 */
	VIDEO_640X528_PAL50_YUV16,	/*!< Interlaced: YES, Video Type: PAL,  Refresh Rate: 50Hz, Size: 640x528 */
	VIDEO_640X480_PAL60_YUV16,	/*!< Interlaced: YES, Video Type: PAL,  Refresh Rate: 60Hz, Size: 640x480 */
	VIDEO_640X480_NTSCp_YUV16,	/*!< Interlaced: NO,  Video Type: NTSC, Refresh Rate: 60Hz, Size: 640x480 */
} vmode_t;

/*! \brief Sets up the video system
 *
 * \param vmode the video mode being used.
 */
void VIDEO_Init(vmode_t vmode);
/*! \brief Sets the used External Framebuffer
 *
 * Sets the video system to use the specified framebuffer as the External Framebuffer
 * (XFB).
 * \param fb the framebuffer to use.
 */
void VIDEO_SetFrameBuffer(void *fb);
/*! \brief Waits for a VSync
 */
void VIDEO_WaitVSync(void);
/*! \brief Blacks out the screen
 *
 * This is only reversable by calling VIDEO_Init again.
 */
void VIDEO_BlackOut(void);
/*! \brief Shuts down the video system
 *
 * This is only reversable by calling VIDEO_Init again.
 */
void VIDEO_Shutdown(void);
/*! \brief Sets up the Video Encoder
 */
void VISetupEncoder(void);
/*! \brief Converts two RGB values into a YUV value
 *
 * The value is suitable for direct writing to the framebuffer.
 * \param r1 Red value of the left pixel.
 * \param g1 Green value of the left pixel.
 * \param b1 Blue value of the left pixel.
 * \param r2 Red value of the right pixel.
 * \param g2 Green value of the right pixel.
 * \param b2 Blue value of the right pixel.
 * \return The YUV value.
 */
u32 make_yuv(u8 r1, u8 g1, u8 b1, u8 r2, u8 g2, u8 b2);

#endif

