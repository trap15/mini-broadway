/*
	libbroadway - A general purpose library to control the Wii.
	USBGecko functions

Copyright (c) 2008		Nuke - <wiinuke@gmail.com>
Copyright (C) 2008, 2009	Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Simple USBGecko abstraction.
 */

#ifndef __GECKO_H__
#define __GECKO_H__

#include <types.h>
#include <broadway.h>

/*! \brief Initialize the USBGecko subsystem
 *
 * Initializes the USBGecko subsystem, registers the necessary IRQ handlers
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa gecko_shutdown()
 */
int gecko_initialize(void);
/*! \brief Shut down the USBGecko subsystem
 *
 * Shuts down the USBGecko subsystem, unregisters the IRQ handlers and performs
 * all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa gecko_initialize()
 */
int gecko_shutdown(void);

/*! \brief printf to a USB Gecko
 */
int gecko_printf(const char *fmt, ...);

/*! \brief Send a command to the USB Gecko
 *
 */
u32 gecko_command(u32 command);

/*! \brief Get the USB Gecko's ID.
 *
 */
u32 gecko_getid(void);

/*! \brief Send a byte through the USB Gecko
 *
 */
u32 gecko_sendbyte(char sendbyte);

/*! \brief Check if the USB Gecko is alive
 *
 */
int gecko_isalive(void);

#endif

