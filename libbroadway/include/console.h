/*
	libbroadway - A general purpose library to control the Wii.
	Visual text console

Copyright (C) 2009		bLAStY <blasty@bootmii.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Functions to manipulate a visual text console using functions from video_low.h
 */

#ifndef __CONSOLE_H__
#define __CONSOLE_H__

#include <types.h>

/*! \brief Initializes a console (and maps to printf)
 *
 * Initializes a console using specified parameters.
 * \param vmode the video mode being used.
 * \param fb the YUV framebuffer to draw the console to.
 * \param x the X offset of the framebuffer in pixels.
 * \param y the Y offset of the framebuffer in pixels.
 * \param w the width of the framebuffer in characters.
 * \param h the height of the framebuffer in characters.
 * \param fw the width of each font character.
 * \param fh the height of each font character.
 * \param font the font in a packed 1-bit format.
 */
void console_initialize(int vmode, void* fb, int x, int y, int w, int h, int fw, int fh, u8 font[]);
/*! \brief Initializes the default console
 *
 * Initializes a console using default values.
 * \param vmode the video mode being used.
 * \param fb the YUV framebuffer to draw the console to.
 */
void console_simple_init(int vmode, void* fb);

/*! \brief Changes the active framebuffer YUV pointer.
 *
 * \param fb the new YUV framebuffer to draw the console to.
 */
void console_set_framebuffer(void* fb);

/*! \brief Prints a string to the screen
 *
 * \param str the string.
 * \param len how many characters the string contains.
 */
void print_str(const char *str, size_t len);
/*! \brief Prints a formatted string to the screen. (DEPRECATED)
 *
 * This function is deprecated. Please print directly to stdout instead.
 * \param fmt the formatted string.
 * \param ... the formatting data.
 */
int gfx_printf(const char *fmt, ...) __attribute__((deprecated));

#endif

