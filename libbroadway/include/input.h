/*
	libbroadway - A general purpose library to control the Wii.
	Input handling

Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2009		bLAStY <blasty@bootmii.org>
Copyright (C) 2009-2011		Alex Marshall <trap15@raidenii.net>
Copyright (C) 2010		Ian Callaghan "unrom" <ian@unrom.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Performs all input management (front panel buttons and GC pad)
 */

#ifndef __INPUT_H__
#define __INPUT_H__

#define PAD_LEFT	(1 <<  0)	/*!< Left D-Pad button */
#define PAD_RIGHT	(1 <<  1)	/*!< Right D-Pad button */
#define PAD_DOWN	(1 <<  2)	/*!< Down D-Pad button */
#define PAD_UP		(1 <<  3)	/*!< Up D-Pad button */
#define PAD_Z		(1 <<  4)	/*!< Z button */
#define PAD_RS		(1 <<  5)	/*!< Right shoulder button */
#define PAD_LS		(1 <<  6)	/*!< Left shoulder button */
//    unused		(1 <<  7)
#define PAD_A		(1 <<  8)	/*!< A button */
#define PAD_B		(1 <<  9)	/*!< B button */
#define PAD_X		(1 << 10)	/*!< X button */
#define PAD_Y		(1 << 11)	/*!< Y button */
#define PAD_START	(1 << 12)	/*!< Start button */
/*! A mask that matches all buttons */
#define PAD_ANY		(PAD_LEFT | PAD_RIGHT | PAD_DOWN | PAD_UP | \
			 PAD_Z    | PAD_RS    | PAD_LS   | PAD_START | \
			 PAD_A    | PAD_B     | PAD_X    | PAD_Y)

#define GCPAD_0		(0)		/*!< First GC pad */
#define GCPAD_1		(1)		/*!< Second GC pad */
#define GCPAD_2		(2)		/*!< Third GC pad */
#define GCPAD_3		(3)		/*!< Fourth GC pad */
#define GCPAD_ALL	(4)		/*!< All GC pads */

/*! Holds all the data for a GameCube pad.
 * \sa pad_read()
 */
typedef struct {
        u16	btns_held;		/*!< Buttons held */
        u16	btns_up;		/*!< Buttons not pressed */
        u16	btns_down;		/*!< Buttons pressed */
	s8	x;			/*!< Stick X */
	s8	y;			/*!< Stick Y */
	s8	cx;			/*!< C-Stick X */
	s8	cy;			/*!< C-Stick Y */
	u8	l;			/*!< Left shoulder distance */
	u8	r;			/*!< Right shoulder distance */
} gc_pad_t;

/*! \brief Initialize the input subsystem
 *
 * Initializes the input subsystem, registers necessary IRQs, and all other 
 * required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa input_shutdown()
 */
int input_initialize(void);

/*! \brief Shut down the input subsystem
 *
 * Shuts down the input subsystem, unregisters the IRQs, and performs all other
 * required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa input_initialize()
 */
int input_shutdown(void);

/*! \brief Reads pad data
 *
 * Reads data from the specified GC pad, and places it into a struct.
 * \param pad pointer to the gc_pad_t for the pad data to go.
 * \param chan which GC pad should be read.
 * \return The button state. (the same as pad->btns_held)
 */
u16 pad_read(gc_pad_t *pad, int chan);

/*! \brief Turns on/off rumble on GC pad
 *
 * \param chan which GC pad should have rumble status changed.
 * \param on if TRUE, turns on rumble. If FALSE, turns off rumble.
 */
void pad_rumble(int chan, BOOL on);

/*! \brief Waits until a button is pressed on any pad
 *
 * \return The combined button states of all pads.
 */
u16 input_wait(void);

#endif

