/*
	libbroadway - A general purpose library to control the Wii.
	General types definition

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef __TYPES_H__
#define __TYPES_H__

#include <stdint.h>
#include <stddef.h>

typedef volatile uint8_t	vuint8_t;
typedef volatile uint16_t	vuint16_t;
typedef volatile uint32_t	vuint32_t;
typedef volatile uint64_t	vuint64_t;

typedef volatile int8_t		vint8_t;
typedef volatile int16_t	vint16_t;
typedef volatile int32_t	vint32_t;
typedef volatile int64_t	vint64_t;

typedef uint8_t			u8;
typedef uint16_t		u16;
typedef uint32_t		u32;
typedef uint64_t		u64;
typedef int8_t			s8;
typedef int16_t			s16;
typedef int32_t			s32;
typedef int64_t			s64;

typedef vuint8_t		vu8;
typedef vuint16_t		vu16;
typedef vuint32_t		vu32;
typedef vuint64_t		vu64;
typedef vint8_t			vs8;
typedef vint16_t		vs16;
typedef vint32_t		vs32;
typedef vint64_t		vs64;

/* These types must be 16-bit, 32-bit or larger integer */
typedef s32			INT;
typedef u32			UINT;

/* These types must be 8-bit integer */
typedef s8			CHAR;
typedef u8			UCHAR;
typedef u8			BYTE;

/* These types must be 16-bit integer */
typedef s16			SHORT;
typedef u16			USHORT;
typedef u16			WORD;
typedef u16			WCHAR;

/* These types must be 32-bit integer */
typedef s32			LONG;
typedef u32			ULONG;
typedef u32			DWORD;

/* Boolean type */
#ifndef FALSE
#define FALSE			(0)
#endif
#ifndef TRUE
#define TRUE			(1)
#endif
#ifndef BOOL
#define BOOL			int
#endif
/*typedef enum { FALSE = 0, TRUE } BOOL;*/

#endif

