/*
	libbroadway - A general purpose library to control the Wii.
	Inter-processor communication

Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2008, 2009	Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2008, 2009	Sven Peter <svenpeter@gmail.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * Various IPC functions to communicate with MINI.
 */

#ifndef __IPC_H__
#define __IPC_H__

#include <ipc_common.h>

/* Nothing over here used this, so in order to make the doxygen look nicer
 * I've removed the union/struct deal.
 */
#if 0
typedef struct {
	union {
		struct {
			u8 flags;
			u8 device;
			u16 req;
		};
		u32 code;
	};
	u32 tag;
	u32 args[6];
} ipc_request;
#endif

/*! The structure for a request to MINI
 */
typedef struct {
	u32 code;	/*!< The request code */
	u32 tag;	/*!< Tag */
	u32 args[6];	/*!< Arguments for the request */
} ipc_request_t;

extern void *mem2_boundary;

/*! \brief Initializes the IPC subsystem
 *
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa ipc_shutdown()
 */
int ipc_initialize(void);
/*! \brief Shuts down the IPC subsystem
 *
 * \sa ipc_initialize()
 */
void ipc_shutdown(void);

/*! \brief Posts an IPC request
 *
 * \param code the request code.
 * \param tag the tag.
 * \param num_args how many arguments to pass.
 * \param ... the arguments.
 * \sa ipc_receive(), ipc_exchange()
 */
void ipc_post(u32 code, u32 tag, u32 num_args, ...);

/*! \brief Flushes all IPC requests
 */
void ipc_flush(void);

/*! \brief Receive an IPC request
 *
 * \return The ipc_request with all information given.
 */
ipc_request_t *ipc_receive(void);
/*! \brief Receive an IPC request
 *
 * \param code the request code to recieve a request from.
 * \param tag the tag the request has.
 * \return The ipc_request with all information given.
 */
ipc_request_t *ipc_receive_tagged(u32 code, u32 tag);

/*! \brief Send and receive an IPC request
 *
 * \param code the request code to send.
 * \param num_args how many arguments to pass.
 * \param ... the arguments.
 * \return The ipc_request with all information given.
 */
ipc_request_t *ipc_exchange(u32 code, u32 num_args, ...);

static inline void ipc_sys_write32(u32 addr, u32 x)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_WRITE32), 0, 2, addr, x);
}
static inline void ipc_sys_write16(u32 addr, u16 x)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_WRITE16), 0, 2, addr, x);
}
static inline void ipc_sys_write8(u32 addr, u8 x)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_WRITE8), 0, 2, addr, x);
}

static inline u32 ipc_sys_read32(u32 addr)
{
	return ipc_exchange(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_READ32), 1, addr)->args[0];
}
static inline u16 ipc_sys_read16(u32 addr)
{
	return ipc_exchange(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_READ16), 1, addr)->args[0];
}
static inline u8 ipc_sys_read8(u32 addr)
{
	return ipc_exchange(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_READ8), 1, addr)->args[0];
}

static inline void ipc_sys_set32(u32 addr, u32 set)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_SET32), 0, 2, addr, set);
}
static inline void ipc_sys_set16(u32 addr, u16 set)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_SET16), 0, 2, addr, set);
}
static inline void ipc_sys_set8(u32 addr, u8 set)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_SET8), 0, 2, addr, set);
}

static inline void ipc_sys_clear32(u32 addr, u32 clear)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_CLEAR32), 0, 2, addr, clear);
}
static inline void ipc_sys_clear16(u32 addr, u16 clear)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_CLEAR16), 0, 2, addr, clear);
}
static inline void ipc_sys_clear8(u32 addr, u8 clear)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_CLEAR8), 0, 2, addr, clear);
}

static inline void ipc_sys_mask32(u32 addr, u32 clear, u32 set)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_MASK32), 0, 3, addr, clear, set);
}
static inline void ipc_sys_mask16(u32 addr, u16 clear, u32 set)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_MASK16), 0, 3, addr, clear, set);
}
static inline void ipc_sys_mask8(u32 addr, u8 clear, u32 set)
{
	ipc_post(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_MASK8), 0, 3, addr, clear, set);
}

static inline void ipc_ping(void)
{
	ipc_exchange(IPC_CODE(IPC_FAST, IPC_DEV_SYS, IPC_SYS_PING), 0);
}

static inline void ipc_slowping(void)
{
	ipc_exchange(IPC_CODE(IPC_SLOW, IPC_DEV_SYS, IPC_SYS_PING), 0);
}

static inline u32 ipc_getvers(void)
{
	return ipc_exchange(IPC_CODE(IPC_SLOW, IPC_DEV_SYS, IPC_SYS_GETVERS), 0)->args[0];
}

#endif

