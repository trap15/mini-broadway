/*
	libbroadway - A general purpose library to control the Wii.
	Hollywood register definitions

Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2008, 2009	Sven Peter <svenpeter@gmail.com>
Copyright (C) 2008, 2009	Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2008, 2009	John Kelley <wiidev@kelley.ca>
Copyright (C) 2009-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * This file contains all the hardware register defines. Due to the way
 * libbroadway is written, you can almost always mix register banging and
 * libbroadway functions with no issues. All the definitions are to be used
 * with the functions in bootmii_ppc.h (write32, read32, etc.).
 */

#ifndef __HOLLYWOOD_H__
#define __HOLLYWOOD_H__

/* Hollywood Registers */

#define		HW_GC_REG_BASE			0xC000000			/*!< Base for GC compatible registers */
#define		HW_PPC_REG_BASE			0xD000000			/*!< Base for PPC specific registers */
#define		HW_REG_BASE			0xD800000			/*!< Base for most hardware */

// The PPC can only see the first three IPC registers
#define		HW_IPC_PPCMSG			(HW_REG_BASE + 0x000)		/*!< PowerPC Message IPC register */
#define		HW_IPC_PPCCTRL			(HW_REG_BASE + 0x004)		/*!< PowerPC Control IPC register */
#define		HW_IPC_ARMMSG			(HW_REG_BASE + 0x008)		/*!< ARM Message IPC register */
#define		HW_IPC_ARMCTRL			(HW_REG_BASE + 0x00C)		/*!< ARM Control IPC register */

#define		HW_TIMER			(HW_REG_BASE + 0x010)		/*!< Starlet timer register */
#define		HW_ALARM			(HW_REG_BASE + 0x014)		/*!< Starlet alarm register */

#define		HW_PPCIRQFLAG			(HW_REG_BASE + 0x030)		/*!< PowerPC IRQs fired flag */
#define		HW_PPCIRQMASK			(HW_REG_BASE + 0x034)		/*!< PowerPC IRQ mask */

#define		HW_ARMIRQFLAG			(HW_REG_BASE + 0x038)		/*!< ARM IRQs fired flag */
#define		HW_ARMIRQMASK			(HW_REG_BASE + 0x03C)		/*!< ARM IRQ mask */

#define		HW_MEMMIRR			(HW_REG_BASE + 0x060)		/*!< Memory Mirroring? */

// something to do with PPCBOOT
// and legacy DI it seems ?!?
#define		HW_EXICTRL			(HW_REG_BASE + 0x070)		/*!< EXI Control? */
#define		EXICTRL_ENABLE_EXI		1				/*!< Enable EXI bit */

// PPC side of GPIO1 (Starlet can access this too)
// Output state
#define		HW_GPIO1B_OUT			(HW_REG_BASE + 0x0C0)		/*!< Output state of PPC side of GPIO1 */
// Direction (1=output)
#define		HW_GPIO1B_DIR			(HW_REG_BASE + 0x0C4)		/*!< Direction of PPC side of GPIO1, 0 = in, 1 = out */
// Input state
#define		HW_GPIO1B_IN			(HW_REG_BASE + 0x0C8)		/*!< Input state of PPC side of GPIO1 */
// Interrupt level
#define		HW_GPIO1B_INTLVL		(HW_REG_BASE + 0x0CC)		/*!< Interrupt level of PPC side of GPIO1 */
// Interrupt flags (write 1 to clear)
#define		HW_GPIO1B_INTFLAG		(HW_REG_BASE + 0x0D0)		/*!< Interrupt flags of PPC side of GPIO1 */
// Interrupt propagation enable
// Do these interrupts go anywhere???
#define		HW_GPIO1B_INTENABLE		(HW_REG_BASE + 0x0D4)		/*!< Interrupt enable of PPC side of GPIO1 */
//??? seems to be a mirror of inputs at some point... power-up state?
#define		HW_GPIO1B_INMIR			(HW_REG_BASE + 0x0D8)		/*!< Input mirror of PPC side of GPIO1 */
// 0xFFFFFF by default, if cleared disables respective outputs. Top bits non-settable.
#define		HW_GPIO1_ENABLE			(HW_REG_BASE + 0x0DC)		/*!< GPIO1 enable */

#define		HW_GPIO1_SLOT			0x000020			/*!< Slot LED bit of GPIO1 */
#define		HW_GPIO1_DEBUG			0xFF0000
#define		HW_GPIO1_DEBUG_SH		16

// Starlet side of GPIO1
// Output state
#define		HW_GPIO1_OUT			(HW_REG_BASE + 0x0E0)		/*!< Output state of ARM side of GPIO1 */
// Direction (1=output)
#define		HW_GPIO1_DIR			(HW_REG_BASE + 0x0E4)		/*!< Direction of ARM side of GPIO1, 0 = in, 1 = out */
// Input state
#define		HW_GPIO1_IN			(HW_REG_BASE + 0x0E8)		/*!< Input state of ARM side of GPIO1 */
// Interrupt level
#define		HW_GPIO1_INTLVL			(HW_REG_BASE + 0x0EC)		/*!< Interrupt level of ARM side of GPIO1 */
// Interrupt flags (write 1 to clear)
#define		HW_GPIO1_INTFLAG		(HW_REG_BASE + 0x0F0)		/*!< Interrupt flags of ARM side of GPIO1 */
// Interrupt propagation enable (interrupts go to main interrupt 0x800)
#define		HW_GPIO1_INTENABLE		(HW_REG_BASE + 0x0F4)		/*!< Interrupt enable of ARM side of GPIO1 */
//??? seems to be a mirror of inputs at some point... power-up state?
#define		HW_GPIO1_INMIR			(HW_REG_BASE + 0x0F8)		/*!< Input mirror of ARM side of GPIO1 */
// Owner of each GPIO bit. If 1, GPIO1B registers assume control. If 0, GPIO1 registers assume control.
#define		HW_GPIO1_OWNER			(HW_REG_BASE + 0x0FC)		/*!< Owner of each bit of GPIO1. GPIO1B for set, GPIO1 for clear */

// ????
#define		HW_DIFLAGS			(HW_REG_BASE + 0x180)		/*!< DI flags? */
#define		DIFLAGS_BOOT_CODE		0x100000

// maybe a GPIO???
#define		HW_RESETS			(HW_REG_BASE + 0x194)		/*!< Resets parts of the machine */

#define		HW_CLOCKS			(HW_REG_BASE + 0x1B4)		/*!< Some clock stuff? */

#define		HW_GPIO2_OUT			(HW_REG_BASE + 0x1C8)
#define		HW_GPIO2_DIR			(HW_REG_BASE + 0x1CC)
#define		HW_GPIO2_IN			(HW_REG_BASE + 0x1D0)

#define		HW_OTP_CMD			(HW_REG_BASE + 0x1EC)		/*!< OTP command */
#define		HW_OTP_DATA			(HW_REG_BASE + 0x1F0)		/*!< OTP data */
#define		HW_VERSION			(HW_REG_BASE + 0x214)		/*!< Hollywoods version */

/* SI Registers */

#define		SI_REG_BASE			(HW_PPC_REG_BASE + 0x06400)	/*!< Base for SI registers */
#define		SI_OUTBUF(x)			(SI_REG_BASE + ((x) * 0xC))	/*!< Output buffers */
#define		SI_OUTBUF_0			SI_OUTBUF(0)
#define		SI_OUTBUF_1			SI_OUTBUF(1)
#define		SI_OUTBUF_2			SI_OUTBUF(2)
#define		SI_OUTBUF_3			SI_OUTBUF(3)
#define		SI_INBUFA(x)			(SI_REG_BASE + 0x4 + ((x) * 0xC))	/*!< Input buffers (part 1) */
#define		SI_INBUFA_0			SI_INBUFA(0)
#define		SI_INBUFA_1			SI_INBUFA(1)
#define		SI_INBUFA_2			SI_INBUFA(2)
#define		SI_INBUFA_3			SI_INBUFA(3)
#define		SI_INBUFB(x)			(SI_REG_BASE + 0x8 + ((x) * 0xC))	/*!< Input buffers (part 2) */
#define		SI_INBUFB_0			SI_INBUFB(0)
#define		SI_INBUFB_1			SI_INBUFB(1)
#define		SI_INBUFB_2			SI_INBUFB(2)
#define		SI_INBUFB_3			SI_INBUFB(3)
#define		SI_POLL				(SI_REG_BASE + 0x30)		/*!< Polling register */
#define		SI_CONTROL			(SI_REG_BASE + 0x34)		/*!< Communication Control register */
#define		SI_STATUS			(SI_REG_BASE + 0x38)		/*!< Status register */
#define		SI_EXI_LOCK			(SI_REG_BASE + 0x3C)		/*!< Lock/unlock 32MHz EXI setting (why on SI bus?) */
#define		SI_IO_BUF			(SI_REG_BASE + 0x80)		/*!< I/O buffer */

/* NAND Registers */

#define		NAND_REG_BASE			(HW_PPC_REG_BASE + 0x10000)	/*!< Base for NAND registers */

#define		NAND_CMD			(NAND_REG_BASE + 0x000)		/*!< NAND Control */
#define		NAND_STATUS			NAND_CMD			/*!< NAND Status */
#define		NAND_CONF			(NAND_REG_BASE + 0x004)		/*!< NAND Configuration? */
#define		NAND_ADDR0			(NAND_REG_BASE + 0x008)		/*!< Part of address */
#define		NAND_ADDR1			(NAND_REG_BASE + 0x00C)		/*!< Part of address */
#define		NAND_DATA			(NAND_REG_BASE + 0x010)		/*!< Buffer for data being read */
#define		NAND_ECC			(NAND_REG_BASE + 0x014)		/*!< Buffer for ECC data being read */
#define		NAND_UNK1			(NAND_REG_BASE + 0x018)
#define		NAND_UNK2			(NAND_REG_BASE + 0x01C)

/* AES Registers */

#define		AES_REG_BASE			(HW_PPC_REG_BASE + 0x20000)	/*!< Base for AES registers */

#define		AES_CMD				(AES_REG_BASE + 0x000)		/*!< AES Control and Status */
#define		AES_SRC				(AES_REG_BASE + 0x004)		/*!< AES source memory address */
#define		AES_DEST			(AES_REG_BASE + 0x008)		/*!< AES destination memory address */
#define		AES_KEY				(AES_REG_BASE + 0x00C)		/*!< AES Key FIFO */
#define		AES_IV				(AES_REG_BASE + 0x010)		/*!< AES IV FIFO */

/* SHA-1 Registers */

#define		SHA_REG_BASE			(HW_PPC_REG_BASE + 0x30000)	/*!< Base for SHA-1 registers */

#define		SHA_CMD				(SHA_REG_BASE + 0x000)		/*!< SHA-1 Control and Status */
#define		SHA_SRC				(SHA_REG_BASE + 0x004)		/*!< SHA-1 source memory address */
#define		SHA_H0				(SHA_REG_BASE + 0x008)		/*!< H0 hash value */
#define		SHA_H1				(SHA_REG_BASE + 0x00C)		/*!< H1 hash value */
#define		SHA_H2				(SHA_REG_BASE + 0x010)		/*!< H2 hash value */
#define		SHA_H3				(SHA_REG_BASE + 0x014)		/*!< H3 hash value */
#define		SHA_H4				(SHA_REG_BASE + 0x018)		/*!< H4 hash value */

/* EHCI Registers */
#define 	EHCI_REG_BASE			(HW_PPC_REG_BASE + 0x40000)	/*!< Base for EHCI registers */

/* stolen from mikep2 patched linux kernel: drivers/usb/host/ohci-mipc.c */
#define		EHCI_CTL			(EHCI_REG_BASE + 0xCC)
/* oh0 interrupt enable */
#define		EHCI_CTL_OH0INTE		(1 << 11)
/* oh1 interrupt enable */
#define		EHCI_CTL_OH1INTE		(1 << 12)

/* OHCI0 Registers */
#define 	OHCI0_REG_BASE			(HW_PPC_REG_BASE + 0x50000)	/*!< Base for OHCI0 registers */

/* OHCI1 Registers */
#define 	OHCI1_REG_BASE			(HW_PPC_REG_BASE + 0x60000)	/*!< Base for OHCI1 registers */

#define 	OHCI_HC_REVISION		0x00
#define 	OHCI_HC_CONTROL			0x04
#define 	OHCI_HC_COMMAND_STATUS		0x08
#define 	OHCI_HC_INT_STATUS		0x0C

#define 	OHCI_HC_INT_ENABLE		0x10
#define 	OHCI_HC_INT_DISABLE		0x14
#define 	OHCI_HC_HCCA 			0x18
#define 	OHCI_HC_PERIOD_CURRENT_ED 	0x1C

#define 	OHCI_HC_CTRL_HEAD_ED 		0x20
#define 	OHCI_HC_CTRL_CURRENT_ED 	0x24
#define 	OHCI_HC_BULK_HEAD_ED 		0x28
#define 	OHCI_HC_BULK_CURRENT_ED 	0x2C

#define 	OHCI_HC_DONE_HEAD 		0x30
#define 	OHCI_HC_FM_INTERVAL 	 	0x34
#define 	OHCI_HC_FM_REMAINING 	 	0x38
#define 	OHCI_HC_FM_NUMBER		0x3C

#define 	OHCI_HC_PERIODIC_START 		0x40
#define 	OHCI_HC_LS_THRESHOLD 	 	0x44
#define 	OHCI_HC_RH_DESCRIPTOR_A		0x48
#define 	OHCI_HC_RH_DESCRIPTOR_B  	0x4C

#define 	OHCI_HC_RH_STATUS 		0x50
#define 	OHCI_HC_RH_PORT_STATUS_1 	0x54
#define 	OHCI_HC_RH_PORT_STATUS_2 	0x58


/* SD Host Controller Registers */
#define		SDHC_REG_BASE			(HW_PPC_REG_BASE + 0x70000)	/*!< Base for SD Host Controller registers */


/* Drive Interface registers */
#define		DI_REG_BASE			(HW_REG_BASE + 0x06000)		/*!< Base for Drive Interface registers */

#define		DI_STATUS			(DI_REG_BASE + 0x00)		/*!< Drive Interface status */
#define		DI_COVER			(DI_REG_BASE + 0x04)		/*!< Drive cover status */
#define		DI_COMMAND			(DI_REG_BASE + 0x08)		/*!< Drive command */
#define		DI_CMD_OFFSET			(DI_REG_BASE + 0x0C)		/*!< Drive command offset in 32bit words */
#define		DI_CMD_LEN			(DI_REG_BASE + 0x10)		/*!< Drive command length */
#define		DI_DMA_ADDR			(DI_REG_BASE + 0x14)		/*!< DMA address (main memory) */
#define		DI_DMA_LEN			(DI_REG_BASE + 0x18)		/*!< DMA length */
#define		DI_CONTROL			(DI_REG_BASE + 0x1C)		/*!< Control Register */
#define		DI_IMM_BUF			(DI_REG_BASE + 0x20)		/*!< Immediate data buffer */
#define		DI_CONFIG			(DI_REG_BASE + 0x24)		/*!< Drive configuration */


/* EXI Registers */
#define		EXI_REG_BASE			(HW_REG_BASE + 0x06800)		/*!< Base for EXI registers */

#define		EXI_BOOT_BASE			(EXI_REG_BASE + 0x040)		/*!< ? */

#define		EXI_CSR(x)			(EXI_REG_BASE + (0x14 * (x)) + 0x00)	/*!< Control/Status EXI registers */
#define		EXI_MAR(x)			(EXI_REG_BASE + (0x14 * (x)) + 0x04)	/*!< Memory Address EXI registers */
#define		EXI_LENGTH(x)			(EXI_REG_BASE + (0x14 * (x)) + 0x08)	/*!< DMA length EXI registers */
#define		EXI_CR(x)			(EXI_REG_BASE + (0x14 * (x)) + 0x0C)	/*!< Control EXI registers */
#define		EXI_DATA(x)			(EXI_REG_BASE + (0x14 * (x)) + 0x10)	/*!< Immediate data EXI registers */

/* EXI0 Registers */
#define		EXI0_REG_BASE			EXI_CSR(0)			/*!< Base for EXI Channel 0 registers */

#define		EXI0_CSR			EXI_CSR(0)
#define		EXI0_MAR			EXI_MAR(0)
#define		EXI0_LENGTH			EXI_LENGTH(0)
#define		EXI0_CR				EXI_CR(0)
#define		EXI0_DATA			EXI_DATA(0)

/* EXI1 Registers */
#define		EXI1_REG_BASE			EXI_CSR(1)			/*!< Base for EXI Channel 1 registers */

#define		EXI1_CSR			EXI_CSR(1)
#define		EXI1_MAR			EXI_MAR(1)
#define		EXI1_LENGTH			EXI_LENGTH(1)
#define		EXI1_CR				EXI_CR(1)
#define		EXI1_DATA			EXI_DATA(1)

/* EXIT2 Registers */
#define		EXI2_REG_BASE			EXI_CSR(2)			/*!< Base for EXI Channel 2 registers */

#define		EXI2_CSR			EXI_CSR(2)
#define		EXI2_MAR			EXI_MAR(2)
#define		EXI2_LENGTH			EXI_LENGTH(2)
#define		EXI2_CR				EXI_CR(2)
#define		EXI2_DATA			EXI_DATA(2)


/* Audio Interface registers */
#define		AI_REG_BASE			(HW_REG_BASE + 0x06C00)		/*!< Base for Audio Interface registers */

#define		AI_CONTROL			(AI_REG_BASE + 0x00)		/*!< Audio Interface control */
#define		AI_VOLUME			(AI_REG_BASE + 0x04)		/*!< Audio Interface volume */
#define		AI_AISCNT			(AI_REG_BASE + 0x08)		/*!< Amount of samples played so far */
#define		AI_AIIT				(AI_REG_BASE + 0x0C)		/*!< AI Interrupt Timing. When AI_AISCNT matches this, fire an IRQ */

/* MEMORY CONTROLLER Registers */
#define		MEM_REG_BASE			(HW_REG_BASE + 0xB4000)		/*!< Base for Memory Controller registers */

#define		MEM_PROT			(MEM_REG_BASE + 0x20A)		/*!< MEM2 protection enable */
#define		MEM_PROT_START			(MEM_REG_BASE + 0x20C)		/*!< MEM2 protection low address */
#define		MEM_PROT_END			(MEM_REG_BASE + 0x20E)		/*!< MEM2 protection high address */
#define		MEM_FLUSHREQ			(MEM_REG_BASE + 0x228)		/*!< AHB flush request */
#define		MEM_FLUSHACK			(MEM_REG_BASE + 0x22A)		/*!< AHB flush ack */


/*! \brief Converts a 32bit integer to "GX32bit"
 *
 * \param x the integer to convert.
 * \return A 32bit integer in "GX32bit" format (CCDDAABB byte-order)
 */
#define		GX32BIT(x)			({ typeof(x) _x = (x); \
						 ((_x & 0xFFFF0000) >> 16) | \
						 ((_x & 0x0000FFFF) << 16); })

/* Command Processor registers */
#define		CP_REG_BASE			(HW_GC_REG_BASE + 0x00000)	/*!< Base for Command Processor registers */

#define		CP_STATUS			(CP_REG_BASE + 0x00)		/*!< Status Register */
#define		CP_CONTROL			(CP_REG_BASE + 0x02)		/*!< Control Register */
#define		CP_CLEAR			(CP_REG_BASE + 0x04)		/*!< Clears FIFO *flows */
#define		CP_TOKEN			(CP_REG_BASE + 0x0E)		/*!< Current Token */
#define		CP_BOUNDING_BOX_L		(CP_REG_BASE + 0x10)		/*!< Bounding Box left */
#define		CP_BOUNDING_BOX_R		(CP_REG_BASE + 0x12)		/*!< Bounding Box right */
#define		CP_BOUNDING_BOX_T		(CP_REG_BASE + 0x14)		/*!< Bounding Box top */
#define		CP_BOUNDING_BOX_B		(CP_REG_BASE + 0x16)		/*!< Bounding Box bottom */
#define		CP_PIPE_BASE			(CP_REG_BASE + 0x20)		/*!< Base of the pipe (GX32bit) */
#define		CP_PIPE_END			(CP_REG_BASE + 0x24)		/*!< End of the pipe (GX32bit) */
#define		CP_PIPE_HI_WATERMARK		(CP_REG_BASE + 0x28)		/*!< Pipe high watermark (GX32bit) */
#define		CP_PIPE_LO_WATERMARK		(CP_REG_BASE + 0x2C)		/*!< Pipe low watermark (GX32bit) */
#define		CP_PIPE_DISTANCE		(CP_REG_BASE + 0x30)		/*!< Pipe Read/Write distance (GX32bit) */
#define		CP_PIPE_WRITE_PTR		(CP_REG_BASE + 0x34)		/*!< Pipe Write pointer (GX32bit) */
#define		CP_PIPE_READ_PTR		(CP_REG_BASE + 0x38)		/*!< Pipe Read pointer (GX32bit) */
#define		CP_PIPE_BP			(CP_REG_BASE + 0x3C)		/*!< Pipe Break pointer? (GX32bit) */

/* Pixel Engine registers */
#define		PE_REG_BASE			(HW_GC_REG_BASE + 0x01000)	/*!< Base for Pixel Engine registers */

#define		PE_Z_CONFIG			(PE_REG_BASE + 0x00)		/*!< Z Configuration register */
#define		PE_ALPHA_CONFIG			(PE_REG_BASE + 0x02)		/*!< Alpha Configuration register */
#define		PE_DEST_ALPHA			(PE_REG_BASE + 0x04)		/*!< Destination Alpha control */
#define		PE_ALPHA_MODE			(PE_REG_BASE + 0x06)		/*!< Alpha mode and threshold */
#define		PE_ALPHA_READ			(PE_REG_BASE + 0x08)		/*!< Alpha mode? */
#define		PE_IRQ_FLAG			(PE_REG_BASE + 0x0A)		/*!< Interrupt Status register */
#define		PE_TOKEN			(PE_REG_BASE + 0x0E)		/*!< Same as CP_TOKEN ? */

/* Video Interface registers */
#define		VI_REG_BASE			(HW_GC_REG_BASE + 0x02000)	/*!< Base for Video Interface registers */

#define		VI_VTIMING			(VI_REG_BASE + 0x00)		/*!< Vertical timing */
#define		VI_STATUS			(VI_REG_BASE + 0x02)		/*!< Status register */
#define		VI_HTIMING0			(VI_REG_BASE + 0x04)		/*!< Horizontal Timing 0 */
#define		VI_HTIMING1			(VI_REG_BASE + 0x08)		/*!< Horizontal Timing 1 */
#define		VI_ODD_VTIMING			(VI_REG_BASE + 0x0C)		/*!< Odd Field Vertical timing */
#define		VI_EVEN_VTIMING			(VI_REG_BASE + 0x10)		/*!< Even Field Vertical timing */
#define		VI_ODD_BBLANK_INTRVL		(VI_REG_BASE + 0x14)		/*!< Odd Field Burst Blanking interval */
#define		VI_EVEN_BBLANK_INTRVL		(VI_REG_BASE + 0x18)		/*!< Even Field Burst Blanking interval */
#define		VI_FRAMEBUFFER_0		(VI_REG_BASE + 0x1C)		/*!< Framebuffer 0 */
#define		VI_FRAMEBUFFER_0_R		(VI_REG_BASE + 0x20)		/*!< Framebuffer 0 Right (3D mode) */
#define		VI_FRAMEBUFFER_1		(VI_REG_BASE + 0x24)		/*!< Framebuffer 1 */
#define		VI_FRAMEBUFFER_1_R		(VI_REG_BASE + 0x28)		/*!< Framebuffer 1 Right (3D mode) */
#define		VI_VPOS				(VI_REG_BASE + 0x2C)		/*!< Vertical Raster position */
#define		VI_HPOS				(VI_REG_BASE + 0x2E)		/*!< Horizontal Raster position */
#define		VI_DISP_IRQ0			(VI_REG_BASE + 0x30)		/*!< Display IRQ 0 */
#define		VI_DISP_IRQ1			(VI_REG_BASE + 0x34)		/*!< Display IRQ 1 */
#define		VI_DISP_IRQ2			(VI_REG_BASE + 0x38)		/*!< Display IRQ 2 */
#define		VI_DISP_IRQ3			(VI_REG_BASE + 0x3C)		/*!< Display IRQ 3 */
#define		VI_DISP_LATCH0			(VI_REG_BASE + 0x40)		/*!< Display Latch 0 */
#define		VI_DISP_LATCH1			(VI_REG_BASE + 0x44)		/*!< Display Latch 1 */
#define		VI_HSCALE_WIDTH			(VI_REG_BASE + 0x48)		/*!< Horizontal scale width */
#define		VI_HSCALE_STEP			(VI_REG_BASE + 0x4A)		/*!< Horizontal scale step */
#define		VI_FILTER_COEFF0		(VI_REG_BASE + 0x4C)		/*!< Filter Coefficient Table 0 */
#define		VI_FILTER_COEFF1		(VI_REG_BASE + 0x50)		/*!< Filter Coefficient Table 1 */
#define		VI_FILTER_COEFF2		(VI_REG_BASE + 0x54)		/*!< Filter Coefficient Table 2 */
#define		VI_FILTER_COEFF3		(VI_REG_BASE + 0x58)		/*!< Filter Coefficient Table 3 */
#define		VI_FILTER_COEFF4		(VI_REG_BASE + 0x5C)		/*!< Filter Coefficient Table 4 */
#define		VI_FILTER_COEFF5		(VI_REG_BASE + 0x60)		/*!< Filter Coefficient Table 5 */
#define		VI_FILTER_COEFF6		(VI_REG_BASE + 0x64)		/*!< Filter Coefficient Table 6 */
#define		VI_CLOCK			(VI_REG_BASE + 0x6C)		/*!< Clock Select */
#define		VI_DTV_STATUS			(VI_REG_BASE + 0x6E)		/*!< DTV Status */
#define		VI_UNKNOWN			(VI_REG_BASE + 0x70)
#define		VI_BORDER_HBLANK_END		(VI_REG_BASE + 0x72)		/*!< Border HBlank End */
#define		VI_BORDER_HBLANK_START		(VI_REG_BASE + 0x74)		/*!< Border HBlank Start */


/* Processor Interface registers */
#define		PI_REG_BASE			(HW_GC_REG_BASE + 0x03000)	/*!< Base for Processor Interface registers */

#define		PI_INT_CAUSE			(PI_REG_BASE + 0x00)		/*!< IRQ flags */
#define		PI_INT_MASK			(PI_REG_BASE + 0x04)		/*!< IRQ masks */
#define		PI_PIPE_START			(PI_REG_BASE + 0x0C)		/*!< Write-gather pipe start */
#define		PI_PIPE_END			(PI_REG_BASE + 0x10)		/*!< Write-gather pipe end */
#define		PI_PIPE_WRITE_PTR		(PI_REG_BASE + 0x14)		/*!< Write-gather pipe write pointer */
#define		PI_RESET			(PI_REG_BASE + 0x24)		/*!< Reset? */
#define		PI_CONSOLE_TYPE			(PI_REG_BASE + 0x2C)		/*!< Console type */


/* Memory Interface registers */
#define		MI_REG_BASE			(HW_GC_REG_BASE + 0x04000)	/*!< Base for Memory Interface registers */

#define		MI_PROT_RGNTOP(x)		(MI_REG_BASE + (4 * (x)))	/*!< The first protected page */
#define		MI_PROT_RGNBOT(x)		(MI_REG_BASE + (4 * (x)) + 2)	/*!< The last protected page */
#define		MI_PROT_RGN0T			MI_PROT_RGNTOP(0)		/*!< The first protected page for rgn 0 */
#define		MI_PROT_RGN1T			MI_PROT_RGNTOP(1)		/*!< The first protected page for rgn 1 */
#define		MI_PROT_RGN2T			MI_PROT_RGNTOP(2)		/*!< The first protected page for rgn 2 */
#define		MI_PROT_RGN3T			MI_PROT_RGNTOP(3)		/*!< The first protected page for rgn 3 */
#define		MI_PROT_RGN0B			MI_PROT_RGNBOT(0)		/*!< The last protected page for rgn 0 */
#define		MI_PROT_RGN1B			MI_PROT_RGNBOT(1)		/*!< The last protected page for rgn 1 */
#define		MI_PROT_RGN2B			MI_PROT_RGNBOT(2)		/*!< The last protected page for rgn 2 */
#define		MI_PROT_RGN3B			MI_PROT_RGNBOT(3)		/*!< The last protected page for rgn 3 */
#define		MI_PROT_TYPE			(MI_REG_BASE + 0x10)		/*!< The protection types */
#define		MI_IRQMASK			(MI_REG_BASE + 0x1C)		/*!< The IRQ mask */
#define		MI_IRQFLAG			(MI_REG_BASE + 0x1E)		/*!< The IRQs fired */

/* Don't know what this does */
/* Clear it in IRQ please */
#define		MI_UNKNOWN1			(MI_REG_BASE + 0x20)

/* Address protection exception occurred on */
#define		MI_PROT_ADDRLO			(MI_REG_BASE + 0x22)		/*!< Address protection exception address low */
#define		MI_PROT_ADDRHI			(MI_REG_BASE + 0x24)		/*!< Address protection exception address high */

/* What are these timers? */
/* Writing anything clears them */
#define		MI_PROT_TIMER0H			(MI_REG_BASE + 0x32)		/*!< Unknown timers? */
#define		MI_PROT_TIMER0L			(MI_REG_BASE + 0x34)		/*!< Unknown timers? */
#define		MI_PROT_TIMER1H			(MI_REG_BASE + 0x36)		/*!< Unknown timers? */
#define		MI_PROT_TIMER1L			(MI_REG_BASE + 0x38)		/*!< Unknown timers? */
#define		MI_PROT_TIMER2H			(MI_REG_BASE + 0x3A)		/*!< Unknown timers? */
#define		MI_PROT_TIMER2L			(MI_REG_BASE + 0x3C)		/*!< Unknown timers? */
#define		MI_PROT_TIMER3H			(MI_REG_BASE + 0x3E)		/*!< Unknown timers? */
#define		MI_PROT_TIMER3L			(MI_REG_BASE + 0x40)		/*!< Unknown timers? */
#define		MI_PROT_TIMER4H			(MI_REG_BASE + 0x42)		/*!< Unknown timers? */
#define		MI_PROT_TIMER4L			(MI_REG_BASE + 0x44)		/*!< Unknown timers? */
#define		MI_PROT_TIMER5H			(MI_REG_BASE + 0x46)		/*!< Unknown timers? */
#define		MI_PROT_TIMER5L			(MI_REG_BASE + 0x48)		/*!< Unknown timers? */
#define		MI_PROT_TIMER6H			(MI_REG_BASE + 0x4A)		/*!< Unknown timers? */
#define		MI_PROT_TIMER6L			(MI_REG_BASE + 0x4C)		/*!< Unknown timers? */
#define		MI_PROT_TIMER7H			(MI_REG_BASE + 0x4E)		/*!< Unknown timers? */
#define		MI_PROT_TIMER7L			(MI_REG_BASE + 0x50)		/*!< Unknown timers? */
#define		MI_PROT_TIMER8H			(MI_REG_BASE + 0x52)		/*!< Unknown timers? */
#define		MI_PROT_TIMER8L			(MI_REG_BASE + 0x54)		/*!< Unknown timers? */
#define		MI_PROT_TIMER9H			(MI_REG_BASE + 0x56)		/*!< Unknown timers? */
#define		MI_PROT_TIMER9L			(MI_REG_BASE + 0x58)		/*!< Unknown timers? */

/* Judging by this being with the timers, possibly a timer controller? */
#define		MI_UNKNOWN2			(MI_REG_BASE + 0x5A)		/*!< Timer controller? */

/* DSP registers */
#define		DSP_REG_BASE			(HW_GC_REG_BASE + 0x05000)	/*!< Base for DSP registers */

#define		DSP_MAILBOX_IN_H		(DSP_REG_BASE + 0x00)		/*!< Top word of the DSP Mailbox In */
#define		DSP_MAILBOX_IN_L		(DSP_REG_BASE + 0x02)		/*!< Bottom word of the DSP Mailbox In */
#define		DSP_MAILBOX_OUT_H		(DSP_REG_BASE + 0x04)		/*!< Top word of the DSP Mailbox Out */
#define		DSP_MAILBOX_OUT_L		(DSP_REG_BASE + 0x06)		/*!< Bottom word of the DSP Mailbox Out */

#define		DSP_STATUS			(DSP_REG_BASE + 0x0A)		/*!< DSP Status and Control */
#define		DSP_IRQ_CTRL			(DSP_REG_BASE + 0x10)		/*!< DSP IRQ control */

/* DSP ARAM interface */
#define		DSP_ARAM_SIZE			(DSP_REG_BASE + 0x12)		/*!< DSP ARAM size? */
#define		DSP_ARAM_MODE			(DSP_REG_BASE + 0x16)		/*!< DSP ARAM mode? */
#define		DSP_ARAM_REFRESH		(DSP_REG_BASE + 0x1A)		/*!< DSP ARAM refresh rate in MHz? */
#define		DSP_ARAM_DMA_MEMADDR_H		(DSP_REG_BASE + 0x20)		/*!< Top word of DMA start address in main memory */
#define		DSP_ARAM_DMA_MEMADDR_L		(DSP_REG_BASE + 0x22)		/*!< Bottom word of DMA start address in main memory */
#define		DSP_ARAM_DMA_ARAMADDR_H		(DSP_REG_BASE + 0x24)		/*!< Top word of DMA start address in ARAM */
#define		DSP_ARAM_DMA_ARAMADDR_L		(DSP_REG_BASE + 0x26)		/*!< Bottom word of DMA start address in ARAM */
#define		DSP_ARAM_DMA_SIZE_H		(DSP_REG_BASE + 0x28)		/*!< Direction and top word of DMA length */
#define		DSP_ARAM_DMA_SIZE_L		(DSP_REG_BASE + 0x2A)		/*!< Bottom word of DMA length */

/* Audio buffer interface */
#define		DSP_DMA_START_H			(DSP_REG_BASE + 0x30)		/*!< Top word of DMA start address */
#define		DSP_DMA_START_L			(DSP_REG_BASE + 0x32)		/*!< Bottom word of DMA start address */
#define		DSP_DMA_SIZE			(DSP_REG_BASE + 0x36)		/*!< DMA control and 32sample count */
#define		DSP_DMA_LEFT			(DSP_REG_BASE + 0x3A)		/*!< How many DMA bytes are left */

#endif

