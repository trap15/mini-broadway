/*
	libbroadway - A general purpose library to control the Wii.
	GPIO support

Copyright (C) 2008, 2009	Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009		John Kelley <wiidev@kelley.ca>
Copyright (C) 2009		bLAStY <blasty@bootmii.org>
Copyright (C) 2009-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * General Purpose I/O control
 */

#ifndef __GPIO_H__
#define __GPIO_H__

#include <types.h>
#include <broadway.h>

typedef enum {
	GPIO_POWER	= (1 <<  0),	/*!< Power button */
	GPIO_SHUTDOWN	= (1 <<  1),	/*!< Shuts down system */
	GPIO_FAN_SPEED	= (1 <<  2),	/*!< Set fan high or low */
	GPIO_DC_DC	= (1 <<  3),	/*!< DC/DC converter power (powers Broadway?). When off, yellow power LED */
	GPIO_DI_SPIN	= (1 <<  4),	/*!< DI spinup disable. */
	GPIO_SLOT_LED	= (1 <<  5),	/*!< Blue disc slot LED */
	GPIO_EJECT	= (1 <<  6),	/*!< Eject button */
	GPIO_SLOT_IN	= (1 <<  7),	/*!< Slot-in button */
	GPIO_SENSOR_BAR	= (1 <<  8),	/*!< Sensor bar power */
	GPIO_DI_EJECT	= (1 <<  9),	/*!< Trigger DI eject */
	GPIO_EEP_CS	= (1 << 10),	/*!< SEEPROM chip select */
	GPIO_EEP_CLK	= (1 << 11),	/*!< SEEPROM clock */
	GPIO_EEP_MOSI	= (1 << 12),	/*!< Data to SEEPROM */
	GPIO_EEP_MISO	= (1 << 13),	/*!< Data from SEEPROM */
	GPIO_AVE_CLK	= (1 << 14),	/*!< A/V Encoder I2C clock */
	GPIO_AVE_DATA	= (1 << 15),	/*!< A/V Encoder I2C data */
	GPIO_DEBUG0	= (1 << 16),	/*!< Debug Testpoint TP221 */
	GPIO_DEBUG1	= (1 << 17),	/*!< Debug Testpoint TP222 */
	GPIO_DEBUG2	= (1 << 18),	/*!< Debug Testpoint TP223 */
	GPIO_DEBUG3	= (1 << 19),	/*!< Debug Testpoint TP224 */
	GPIO_DEBUG4	= (1 << 20),	/*!< Debug Testpoint TP225 */
	GPIO_DEBUG5	= (1 << 21),	/*!< Debug Testpoint TP226 */
	GPIO_DEBUG6	= (1 << 22),	/*!< Debug Testpoint TP219 */
	GPIO_DEBUG7	= (1 << 23),	/*!< Debug Testpoint TP220 */
} gpio_pin_t;

/*! \brief Initialize the GPIO subsystem
 *
 * Initializes the GPIO subsystem, registers the handler for the GPIO interrupt
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa gpio_shutdown()
 */
int gpio_initialize(void);
/*! \brief Shut down the GPIO subsystem
 *
 * Shuts down the GPIO subsystem, unregisters the handler for the GPIO interrupt
 * and performs all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa gpio_initialize()
 */
int gpio_shutdown(void);

/*! \brief Reads GPIO data
 *
 * Reads data from the GPIOs.
 * \return The GPIO state.
 */
gpio_pin_t gpio_read(void);

/*! \brief Writes GPIO output data
 *
 * \param gpios The GPIO state to output.
 */
void gpio_write(gpio_pin_t gpios);

/*! \brief Sets GPIO output data bits
 *
 * \param gpios The GPIO states to set.
 */
void gpio_set(gpio_pin_t gpios);

/*! \brief Clears GPIO output data bits
 *
 * \param gpios The GPIO states to clear.
 */
void gpio_clear(gpio_pin_t gpios);

/*! \brief Masks GPIO output data bits
 *
 * \param setgpios The GPIO states to set.
 * \param clrgpios The GPIO states to clear.
 */
void gpio_mask(gpio_pin_t setgpios, gpio_pin_t clrgpios);

/*! \brief Switches GPIO output data bits
 *
 * \param gpios The GPIO states to switch.
 */
void gpio_switch(gpio_pin_t gpios);

/*! \brief Sets GPIO direction
 *
 * \param dir the new GPIO direction. -1 is input, 1 is output, 0 is no change.
 * \return The old GPIO direction.
 */
int gpio_direction(gpio_pin_t pin, int dir);

/*! \brief Turn the slot light on/off
 */
static inline void slotled_change(void)
{
	flip32(HW_GPIO1B_OUT, GPIO_SLOT_LED);
}

/*! \brief Turn the slot light on
 */
static inline void slotled_on(void)
{
	set32(HW_GPIO1B_OUT, GPIO_SLOT_LED);
}

/*! \brief Turn the slot light off
 */
static inline void slotled_off(void)
{
	clear32(HW_GPIO1B_OUT, GPIO_SLOT_LED);
}

/*! \brief Shut down the system
 */
static inline void shutdown(void)
{
	set32(HW_GPIO1_OUT, GPIO_SHUTDOWN);
}

#endif

