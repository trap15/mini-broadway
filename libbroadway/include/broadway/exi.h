/*
	libbroadway - A general purpose library to control the Wii.
	EXI support

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of the External Interface (EXI). Fairly low-level, so you need
 * to know what you're doing (though if you didn't, why would you be touching
 * this file).
 */

#ifndef __EXI_H__
#define __EXI_H__

#include <types.h>
#include <broadway.h>

/*! Clock speeds for an EXI channel to run at.
 */
typedef enum {
	EXI_CLOCK_1MHz = 0,		/*!< 1MHz clock */
	EXI_CLOCK_2MHz,			/*!< 2MHz clock */
	EXI_CLOCK_4MHz,			/*!< 4MHz clock */
	EXI_CLOCK_8MHz,			/*!< 8MHz clock */
	EXI_CLOCK_16MHz,		/*!< 16MHz clock */
	EXI_CLOCK_32MHz,		/*!< 32MHz clock. EXI clock lock must be disabled */
} exi_clock_t;

/*! Transfer modes for an EXI channel to use.
 */
typedef enum {
	EXI_TRANSFER_READ = 0,		/*!< Read */
	EXI_TRANSFER_WRITE,		/*!< Write */
	EXI_TRANSFER_READ_WRITE,	/*!< Read and Write (invalid for DMA) */
} exi_transfer_t;

/*! \brief Enables a specific EXI IRQ
 *
 * \param irq the IRQ to enable
 * \sa irq_exi_disable()
 */
void irq_exi_enable(u32 irq);
/*! \brief Disables a specific EXI IRQ
 *
 * \param irq the IRQ to disable
 * \sa irq_exi_enable()
 */
void irq_exi_disable(u32 irq);
/*! \brief Register an EXI IRQ handler
 *
 * \param irqn the IRQ whose handle is to be registered.
 * \param exec the callback to be called when the IRQ fires. Pass NULL to
 *             unregister the IRQ.
 * \param data the data to be passed to the IRQ when it fires.
 * \return Returns 1 on success, 0 on failure.
 * \sa irq_exi_get_handler()
 */
int irq_exi_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data);
/*! \brief Gets an EXI IRQ handler
 *
 * Gets the IRQ handler information for a EXI IRQ.
 * \param irqn the IRQ whose handler is to be returned.
 * \return The irq_handler_t structure containing information about the
 *         handler.
 * \sa irq_exi_register_handler()
 */
irq_handler_t irq_exi_get_handler(u32 irqn);

/*! \brief Initialize the EXI subsystem
 *
 * Initializes the EXI subsystem, registers the handler for the EXI interrupt
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa exi_shutdown()
 */
int exi_initialize(void);
/*! \brief Shut down the EXI subsystem
 *
 * Shuts down the EXI subsystem, unregisters the handler for the EXI interrupt
 * and performs all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa exi_initialize()
 */
int exi_shutdown(void);
/*! \brief Turn on/off EXI clock lock
 *
 * \param locked TRUE to lock EXI clock, FALSE to unlock it.
 * \return Returns the old EXI clock lock status.
 */
BOOL exi_set_lock(BOOL locked);
/*! \brief Set EXI clock frequency
 *
 * \param chan EXI channel to set the clock frequency of. (0 ~ 2)
 * \param freq exi_clock_t enumeration specifying the clock to operate at.
 * \return TRUE if setting was successful, FALSE otherwise.
 * \sa exi_set_lock(), exi_device_select()
 */
BOOL exi_set_clock(int chan, exi_clock_t clk);
/*! \brief Select EXI device
 *
 * \param chan EXI channel to select a device for. (0 ~ 2)
 * \param dev device number to select. (0 ~ 2)
 * \return TRUE if setting was successful, FALSE otherwise.
 * \sa exi_set_clock(), exi_device_deselect()
 */
BOOL exi_device_select(int chan, int dev);
/*! \brief Deselect EXI device
 *
 * \param chan EXI channel to deselect a device for. (0 ~ 2)
 * \param dev device number to deselect. (0 ~ 2)
 * \return TRUE if setting was successful, FALSE otherwise.
 * \sa exi_set_clock(), exi_device_select()
 */
BOOL exi_device_deselect(int chan, int dev);
/*! \brief Checks if a device is connected
 *
 * \param chan EXI channel to check for a device on. (0 ~ 2)
 * \return TRUE if there is a device, FALSE if there is not a device
 */
BOOL exi_device_connected(int chan);
/*! \brief Sets DMA start address
 *
 * \param chan EXI channel to set the DMA start address for. (0 ~ 2)
 * \param addr address to set as the start address for DMA. (32byte aligned)
 * \return TRUE if setting was successful, FALSE otherwise.
 * \sa exi_set_dma_length()
 */
BOOL exi_set_dma_address(int chan, u32 addr);
/*! \brief Sets DMA length
 *
 * \param chan EXI channel to set the DMA length for. (0 ~ 2)
 * \param len length of the DMA. (32byte aligned)
 * \return TRUE if setting was successful, FALSE otherwise.
 * \sa exi_set_dma_address(), exi_set_immediate_length()
 */
BOOL exi_set_dma_length(int chan, u32 len);
/*! \brief Sets Immediate data length
 *
 * \param chan EXI channel to set the length of immediate data. (0 ~ 2)
 * \param len length of immediate data. (1 ~ 4)
 * \return TRUE if setting was successful, FALSE otherwise.
 */
BOOL exi_set_immediate_length(int chan, int len);
/*! \brief Perform an EXI transfer
 *
 * \param chan EXI channel to transfer on. (0 ~ 2)
 * \param type exi_transfer_t enumeration specifying the transfer type.
 * \param dma TRUE if this is a DMA, FALSE for an immediate transfer.
 * \return TRUE if transfer was successfully started, FALSE otherwise.
 * \sa exi_transfer_ended()
 */
BOOL exi_transfer(int chan, exi_transfer_t type, BOOL dma);
/*! \brief Check if an EXI transfer is complete
 *
 * \param chan EXI channel to check for completion. (0 ~ 2)
 * \return TRUE if transfer is complete, FALSE if transfer is still in progress.
 * \sa exi_transfer()
 */
BOOL exi_transfer_ended(int chan);
/*! \brief Get data from an Immediate EXI transfer
 *
 * \param chan EXI channel to get data from. (0 ~ 2)
 * \return The data retrieved from the EXI transfer.
 * \sa exi_immediate_send()
 */
u32 exi_immediate_recv(int chan);
/*! \brief Set data for an Immediate EXI transfer
 *
 * \param chan EXI channel to get data from. (0 ~ 2)
 * \param data the data sent during the EXI transfer.
 * \return TRUE if setting was successful, FALSE otherwise.
 * \sa exi_immediate_recv()
 */
BOOL exi_immediate_send(int chan, u32 data);

#endif

