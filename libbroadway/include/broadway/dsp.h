/*
	libbroadway - A general purpose library to control the Wii.
	DSP support

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of the interface to the DSP. Buggy.
 * \todo Disassemble the DSP init code.
 * \todo Clean up dsp_initialize()
 */

#ifndef __DSP_H__
#define __DSP_H__

#include <types.h>
#include <broadway.h>

/*! All the information needed to send a microcode to the DSP.
 */
typedef struct {
	void*	iram_src;	/*!< Source address of the IRAM (32byte align) */
	u16	iram_dst;	/*!< Destination address of the IRAM */
	u16	iram_len;	/*!< Length of IRAM (32byte align) */
	void*	dram_src;	/*!< Source address of the DRAM (32byte align) */
	u16	dram_dst;	/*!< Destination address of the DRAM */
	u16	dram_len;	/*!< Length of DRAM (32byte align) */
	u16	pc;		/*!< The PC to jump to */
} dsp_ucode_t;

/*! \brief Initialize the DSP subsystem
 *
 * Initializes the DSP subsystem, and performs all other required 
 * initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa dsp_shutdown()
 */
int dsp_initialize(void);
/*! \brief Shut down the DSP subsystem
 *
 * Shuts down the DSP subsystem, and performs all other required shut down
 * tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa dsp_initialize()
 */
int dsp_shutdown(void);

/*! \brief Resets the DSP
 */
void dsp_reset(void);

/*! \brief Sends mail to the DSP
 *
 * \param data the data to be sent to the DSP.
 * \sa dsp_read_outbox(), dsp_check_outbox(), dsp_recv_mail()
 */
void dsp_send_mail(u32 data);
/*! \brief Reads mail sent to the DSP
 *
 * \return The data sent to the DSP.
 * \sa dsp_send_mail(), dsp_check_outbox(), dsp_recv_mail()
 */
u32 dsp_read_outbox(void);
/*! \brief Recieves mail from the DSP
 *
 * \return The data recieved from the DSP.
 * \sa dsp_read_outbox(), dsp_check_inbox(), dsp_send_mail()
 */
u32 dsp_recv_mail(void);
/*! \brief Checks if the incoming mail is valid
 *
 * \return TRUE if the incoming mail is valid, FALSE otherwise.
 * \sa dsp_read_outbox(), dsp_check_outbox(), dsp_recv_mail()
 */
BOOL dsp_check_inbox(void);
/*! \brief Checks if the outgoing mail is valid
 *
 * \return TRUE if the outgoing mail is valid, FALSE otherwise.
 * \sa dsp_read_outbox(), dsp_check_inbox(), dsp_recv_mail()
 */
BOOL dsp_check_outbox(void);

/*! \brief Halts the DSP
 *
 * \return TRUE if the the DSP was previously running, FALSE otherwise.
 * \sa dsp_unhalt()
 */
BOOL dsp_halt(void);
/*! \brief Unhalts the DSP
 *
 * \return TRUE if the the DSP was previously running, FALSE otherwise.
 * \sa dsp_halt()
 */
BOOL dsp_unhalt(void);

/*! \brief Register the DSP IRQ handler
 *
 * \param exec the callback to be called when the IRQ fires. Pass NULL to
 *             unregister the IRQ.
 * \param data the data to be passed to the IRQ when it fires.
 * \return Returns 1 on success, 0 on failure.
 * \sa irq_dsp_get_handler()
 */
int irq_dsp_register_handler(int (*exec)(u32 irq, void* data), void* data);
/*! \brief Gets the DSP IRQ handler
 *
 * \return The irq_handler_t structure containing information about the
 *         handler.
 * \sa irq_dsp_register_handler()
 */
irq_handler_t irq_dsp_get_handler(void);
/*! \brief Enables the DSP IRQ
 *
 * \sa irq_dsp_disable()
 */
void irq_dsp_enable(void);
/*! \brief Disables the DSP IRQ
 *
 * \sa irq_dsp_enable()
 */
void irq_dsp_disable(void);

/*! \brief Uploads a uCode to the DSP
 *
 * The DSP must not be halted when this function is called.
 * \param ucode the uCode to upload to the DSP.
 * \return Returns 1 on success, 0 on failure.
 */
int dsp_upload_ucode(dsp_ucode_t ucode);

#endif

