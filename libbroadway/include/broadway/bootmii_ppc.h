/*
	libbroadway - A general purpose library to control the Wii.
	PowerPC helper functions

Copyright (C) 2008		Segher Boessenkool <segher@kernel.crashing.org>
Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * All the SPRs for Broadway, and several PowerPC specific functions.
 */

#ifndef __BOOTMII_PPC_H__
#define __BOOTMII_PPC_H__

#include <types.h>
#include <broadway/hollywood.h>

/* Broadway SPRs */
#define SPR_XER		(   1)	/*!< Integer Exception Register */
#define SPR_LR		(   8)	/*!< Link Register */
#define SPR_CTR		(   9)	/*!< Count Register */
#define SPR_DSISR	(  18)	/*!< DSI Exception Cause Register */
#define SPR_DAR		(  19)	/*!< Data Address Register */
#define SPR_DEC		(  22)	/*!< Decrementer Register */
#define SPR_SDR1	(  25)	/*!< Page Table base address */
#define SPR_SRR0	(  26)	/*!< Save/Restore Register 0 */
#define SPR_SRR1	(  27)	/*!< Save/Restore Register 1 */
#define SPR_SPRG0	( 272)	/*!< OS reserved */
#define SPR_SPRG1	( 273)	/*!< OS reserved */
#define SPR_SPRG2	( 274)	/*!< OS reserved */
#define SPR_SPRG3	( 275)	/*!< OS reserved */
#define SPR_EAR		( 282)	/*!< External Access Register */
#define SPR_PVR		( 287)	/*!< Processor Version Register */
#define SPR_IBAT0U	( 528)	/*!< Instruction Block-address Translation Upper Register 0 */
#define SPR_IBAT0L	( 529)	/*!< Instruction Block-address Translation Lower Register 0 */
#define SPR_IBAT1U	( 530)	/*!< Instruction Block-address Translation Upper Register 1 */
#define SPR_IBAT1L	( 531)	/*!< Instruction Block-address Translation Lower Register 1 */
#define SPR_IBAT2U	( 532)	/*!< Instruction Block-address Translation Upper Register 2 */
#define SPR_IBAT2L	( 533)	/*!< Instruction Block-address Translation Lower Register 2 */
#define SPR_IBAT3U	( 534)	/*!< Instruction Block-address Translation Upper Register 3 */
#define SPR_IBAT3L	( 535)	/*!< Instruction Block-address Translation Lower Register 3 */
#define SPR_DBAT0U	( 536)	/*!< Data Block-address Translation Upper Register 0 */
#define SPR_DBAT0L	( 537)	/*!< Data Block-address Translation Lower Register 0 */
#define SPR_DBAT1U	( 538)	/*!< Data Block-address Translation Upper Register 1 */
#define SPR_DBAT1L	( 539)	/*!< Data Block-address Translation Lower Register 1 */
#define SPR_DBAT2U	( 540)	/*!< Data Block-address Translation Upper Register 2 */
#define SPR_DBAT2L	( 541)	/*!< Data Block-address Translation Lower Register 2 */
#define SPR_DBAT3U	( 542)	/*!< Data Block-address Translation Upper Register 3 */
#define SPR_DBAT3L	( 543)	/*!< Data Block-address Translation Lower Register 3 */
#define SPR_GQR0	( 912)	/*!< Graphics Quantization Register 0 */
#define SPR_GQR1	( 913)	/*!< Graphics Quantization Register 1 */
#define SPR_GQR2	( 914)	/*!< Graphics Quantization Register 2 */
#define SPR_GQR3	( 915)	/*!< Graphics Quantization Register 3 */
#define SPR_GQR4	( 916)	/*!< Graphics Quantization Register 4 */
#define SPR_GQR5	( 917)	/*!< Graphics Quantization Register 5 */
#define SPR_GQR6	( 918)	/*!< Graphics Quantization Register 6 */
#define SPR_GQR7	( 919)	/*!< Graphics Quantization Register 7 */
#define SPR_HID2	( 920)	/*!< Hardware Implementation-Dependant Register 2 */
#define SPR_WPAR	( 921)	/*!< Write Pipe Address Register */
#define SPR_DMA_U	( 922)	/*!< Direct Memory Access Upper Register */
#define SPR_DMA_L	( 923)	/*!< Direct Memory Access Lower Register */
#define SPR_UMMCR0	( 936)	/*!< User Monitor Mode Control Register 0 */
#define SPR_UPMC1	( 937)	/*!< User Performance Monitor Count Register 1 */
#define SPR_UPMC2	( 938)	/*!< User Performance Monitor Count Register 2 */
#define SPR_USIA	( 939)	/*!< User Sampled Instruction Address Register */
#define SPR_UMMCR1	( 940)	/*!< User Monitor Mode Control Register 0 */
#define SPR_UPMC3	( 941)	/*!< User Performance Monitor Count Register 3 */
#define SPR_UPMC4	( 942)	/*!< User Performance Monitor Count Register 4 */
#define SPR_USDA	( 943)	/*!< User Sampled Data Address Register */
#define SPR_MMCR0	( 952)	/*!< Monitor Mode Control Register 0 */
#define SPR_PMC1	( 953)	/*!< Performance Monitor Count Register 1 */
#define SPR_PMC2	( 954)	/*!< Performance Monitor Count Register 2 */
#define SPR_SIA		( 955)	/*!< Sampled Instruction Address Register */
#define SPR_MMCR1	( 956)	/*!< Monitor Mode Control Register 1 */
#define SPR_PMC3	( 957)	/*!< Performance Monitor Count Register 3 */
#define SPR_PMC4	( 958)	/*!< Performance Monitor Count Register 4 */
#define SPR_SDA		( 959)	/*!< Sampled Data Address Register */
#define SPR_HID0	(1008)	/*!< Hardware Implementation-Dependant Register 0 */
#define SPR_HID1	(1009)	/*!< Hardware Implementation-Dependant Register 1 */
#define SPR_IABR	(1010)	/*!< Instruction Address Breakpoint Register */
#define SPR_HID4	(1011)	/*!< Hardware Implementation-Dependant Register 4 */
#define SPR_DABR	(1013)	/*!< Data Address Breakpoint Register */
#define SPR_L2CR	(1017)	/*!< L2 Cache Control Register */
#define SPR_ICTC	(1019)	/*!< Instruction Cache Throttling Control Register */
#define SPR_THRM1	(1020)	/*!< Thermal Management Register 1 */
#define SPR_THRM2	(1021)	/*!< Thermal Management Register 2 */
#define SPR_THRM3	(1022)	/*!< Thermal Management Register 3 */


#define MEM2_BSS	__attribute__ ((section (".bss.mem2")))		/*!< Puts some data into the MEM2 .bss section */
#define MEM2_DATA	__attribute__ ((section (".data.mem2")))	/*!< Puts some data into the MEM2 .data section */
#define MEM2_RODATA	__attribute__ ((section (".rodata.mem2")))	/*!< Puts some data into the MEM2 .rodata section */
#define ALIGNED(x)	__attribute__ ((aligned(x)))

/*! \brief Creates an array on the stack.
 *
 * Uhh...
 * \param type the type of array to be created.
 * \param name the name of the array to be created.
 * \param cnt how many "type"s to allocate.
 * \param alignment how many bytes to align the array to.
 */
#define STACK_ALIGN(type, name, cnt, alignment)         \
	u8 _al__##name[((sizeof(type)*(cnt)) + (alignment) + \
	(((sizeof(type)*(cnt))%(alignment)) > 0 ? ((alignment) - \
	((sizeof(type)*(cnt))%(alignment))) : 0))]; \
	type *name = (type*)(((u32)(_al__##name)) + ((alignment) - (( \
	(u32)(_al__##name))&((alignment)-1))))

#define VIRT_TO_PHYS(x)		((u32)(x) & 0x3FFFFFFF)	/*!< Converts virtual addresses to physical */
#define PHYS_TO_VIRT(x)		((u32)(x) | 0x80000000)	/*!< Converts physical addresses to cached virtual */
#define PHYS_TO_VIRT_NOCACHE(x)	((u32)(x) | 0xC0000000)	/*!< Converts physical addresses to uncached virtual */

/* Basic I/O. */
/*! \brief Reads a 32-bit memory register
 *
 * \param addr the address of the register.
 * \return The value of the register.
 * \sa write32(), set32(), clear32(), flip32(), mask32()
 */
static inline u32 read32(u32 addr)
{
	u32 x;
	asm volatile("lwz %0,0(%1) ; sync" : "=r"(x) : "b"(PHYS_TO_VIRT_NOCACHE(addr)));
	return x;
}

/*! \brief Writes a 32-bit memory register
 *
 * \param addr the address of the register.
 * \param x the value to be written to the register.
 * \sa read32(), set32(), clear32(), flip32(), mask32()
 */
static inline void write32(u32 addr, u32 x)
{
	asm("stw %0,0(%1) ; eieio" : : "r"(x), "b"(PHYS_TO_VIRT_NOCACHE(addr)));
}

/*! \brief Sets bits on a 32-bit memory register
 *
 * \param addr the address of the register.
 * \param set the value to be ORed to the register.
 * \sa read32(), write32(), clear32(), flip32(), mask32()
 */
static inline void set32(u32 addr, u32 set)
{
	write32(addr, read32(addr) | set);
}

/*! \brief Clears bits on a 32-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be converse and ANDed to the register.
 * \sa read32(), write32(), set32(), flip32(), mask32()
 */
static inline void clear32(u32 addr, u32 clear)
{
	write32(addr, read32(addr) & (~clear));
}

/*! \brief Flips bits on a 32-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be XORed to the register.
 * \sa read32(), write32(), set32(), clear32(), mask32()
 */
static inline void flip32(u32 addr, u32 flip)
{
	write32(addr, read32(addr) ^ flip);
}

/*! \brief Masks bits on a 32-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be converse and ANDed to the register.
 * \param set the value to be ORed to the register. This happens after the AND.
 * \sa read32(), write32(), set32(), clear32(), flip32()
 */
static inline void mask32(u32 addr, u32 clear, u32 set)
{
	write32(addr, (read32(addr) & (~clear)) | set);
}

/*! \brief Reads a 16-bit memory register
 *
 * \param addr the address of the register.
 * \return The value of the register.
 * \sa write16(), set16(), clear16(), flip16(), mask16()
 */
static inline u16 read16(u32 addr)
{
	u16 x;
	asm volatile("lhz %0,0(%1) ; sync" : "=r"(x) : "b"(PHYS_TO_VIRT_NOCACHE(addr)));
	return x;
}

/*! \brief Writes a 16-bit memory register
 *
 * \param addr the address of the register.
 * \param x the value to be written to the register.
 * \sa read16(), set16(), clear16(), flip16(), mask16()
 */
static inline void write16(u32 addr, u16 x)
{
	asm("sth %0,0(%1) ; eieio" : : "r"(x), "b"(PHYS_TO_VIRT_NOCACHE(addr)));
}

/*! \brief Sets bits on a 16-bit memory register
 *
 * \param addr the address of the register.
 * \param set the value to be ORed to the register.
 * \sa read16(), write16(), clear16(), flip16(), mask16()
 */
static inline void set16(u32 addr, u16 set)
{
	write16(addr, read16(addr) | set);
}

/*! \brief Clears bits on a 16-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be converse and ANDed to the register.
 * \sa read16(), write16(), set16(), flip16(), mask16()
 */
static inline void clear16(u32 addr, u16 clear)
{
	write16(addr, read16(addr) & (~clear));
}

/*! \brief Flips bits on a 16-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be XORed to the register.
 * \sa read16(), write16(), set16(), clear16(), mask16()
 */
static inline void flip16(u32 addr, u16 flip)
{
	write16(addr, read16(addr) ^ flip);
}

/*! \brief Masks bits on a 16-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be converse and ANDed to the register.
 * \param set the value to be ORed to the register. This happens after the AND.
 * \sa read16(), write16(), set16(), clear16(), flip16()
 */
static inline void mask16(u32 addr, u16 clear, u16 set)
{
	write16(addr, (read16(addr) & (~clear)) | set);
}

/*! \brief Reads an 8-bit memory register
 *
 * \param addr the address of the register.
 * \return The value of the register.
 * \sa write8(), set8(), clear8(), flip8(), mask8()
 */
static inline u8 read8(u32 addr)
{
	u8 x;
	asm volatile("lbz %0,0(%1) ; sync" : "=r"(x) : "b"(PHYS_TO_VIRT_NOCACHE(addr)));
	return x;
}

/*! \brief Writes an 8-bit memory register
 *
 * \param addr the address of the register.
 * \param x the value to be written to the register.
 * \sa read8(), set8(), clear8(), flip8(), mask8()
 */
static inline void write8(u32 addr, u8 x)
{
	asm("stb %0,0(%1) ; eieio" : : "r"(x), "b"(PHYS_TO_VIRT_NOCACHE(addr)));
}

/*! \brief Sets bits on an 8-bit memory register
 *
 * \param addr the address of the register.
 * \param set the value to be ORed to the register.
 * \sa read8(), write8(), clear8(), flip8(), mask8()
 */
static inline void set8(u32 addr, u8 set)
{
	write8(addr, read8(addr) | set);
}

/*! \brief Clears bits on an 8-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be converse and ANDed to the register.
 * \sa read8(), write8(), set8(), flip8(), mask8()
 */
static inline void clear8(u32 addr, u8 clear)
{
	write8(addr, read8(addr) & (~clear));
}

/*! \brief Flips bits on an 8-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be XORed to the register.
 * \sa read8(), write8(), set8(), clear8(), mask8()
 */
static inline void flip8(u32 addr, u8 flip)
{
	write8(addr, read8(addr) ^ flip);
}

/*! \brief Masks bits on an 8-bit memory register
 *
 * \param addr the address of the register.
 * \param clear the value to be converse and ANDed to the register.
 * \param set the value to be ORed to the register. This happens after the AND.
 * \sa read8(), write8(), set8(), clear8(), flip8()
 */
static inline void mask8(u32 addr, u8 clear, u8 set)
{
	write8(addr, (read8(addr) & (~clear)) | set);
}

/* Address mapping. */
/*! \brief Converts a virtual address to a physical one
 *
 * \param p the virtual address.
 * \return the physical address.
 */
static inline u32 virt_to_phys(const void *p)
{
	return (u32)(VIRT_TO_PHYS(p));
}

/*! \brief Converts a physical address to a cached virtual one
 *
 * \param p the physical address.
 * \return the cached virtual address.
 */
static inline void *phys_to_virt(u32 x)
{
	return (void *)(PHYS_TO_VIRT(x));
}

/*! \brief Converts a physical address to a non-cached virtual one
 *
 * \param p the physical address.
 * \return the non-cached virtual address.
 */
static inline void *phys_to_virt_nocache(u32 x)
{
	return (void *)(PHYS_TO_VIRT_NOCACHE(x));
}

/* Cache synchronisation. */
/*! \brief Invalidates the cache for the data
 *
 * \param p the start of the memory to be read.
 * \param len how far the cache needs to be invalidated.
 */
void sync_before_read(void *p, u32 len);
/*! \brief Flushes the cache for the data
 *
 * \param p the start of the memory that was written.
 * \param len how far the cache needs to be flushed.
 */
void flush_after_write(void *p, u32 len);
/*! \brief Stores the cache for the data
 *
 * \param p the start of the memory that was written.
 * \param len how far the cache needs to be stored.
 */
void sync_after_write(const void *p, u32 len);
/*! \brief Stores the cache for the data and invalidates the instructions
 *
 * \param p the start of the memory that is to be executed.
 * \param len how far the cache needs to be stored and invalidated.
 */
void sync_before_exec(const void *p, u32 len);
/*! \brief Synchronizes the data and instruction caches
 */
void ppcsync(void);

/* Time. */
/*! \brief Delay with microsecond granularity
 *
 * \param us how many microseconds to delay.
 */
void udelay(u32 us);
/*! \brief Get processor ticks since power-on
 *
 * \return How many processor ticks have elapsed since power-on.
 */
u64 getticks();
/*! \brief Delay with processor tick granularity
 *
 * \param ticks how many processor ticks to wait.
 */
void tickdelay(u64 ticks);
#define usleep(n)	udelay(n)	/*!< Renaming for compatibility */
#define mftb()		getticks()	/*!< Renaming for processor terms */

/* Special purpose registers. */
/*! \brief Moves a value into an SPR
 *
 * \param n the SPR.
 * \param x the value to be put into the SPR.
 * \sa mfspr()
 */
#define mtspr(n, x) asm("mtspr %1,%0" : : "r"(x), "i"(n))
/*! \brief Gets the value of an SPR
 *
 * \param n the SPR.
 * \return The value of the SPR.
 * \sa mfspr()
 */
#define mfspr(n) ({ \
	u32 x; asm volatile("mfspr %0,%1" : "=r"(x) : "i"(n)); x; \
})

/* Exceptions. */
/*! \brief Initializes exceptions
 */
void exception_init(void);
/*! \brief Exception handler type
 */
typedef void (*except_handler)(int exception);
/*! Exception handler table */
extern volatile except_handler exception_handler_table[0x10];

/*! Reboot the system */
#define reboot()		write32(HW_RESETS, 0)
/*! Get the version of the Hollywood */
#define hollywood_version()	((read32(HW_VERSION) >> 4) & 0xF)
/*! Get the revision of the Hollywood */
#define hollywood_revision()	((read32(HW_VERSION) >> 0) & 0xF)

/*! \brief Switch to Doze mode
 *
 * Switches the power-save mode to doze.
 * \sa nap_mode(), sleep_mode(), power_save()
 */
static inline void doze_mode()
{
	u32 hid0;
	asm volatile("mfspr %0,1008" : "=r"(hid0));
	/* Disable sleep, nap and doze bits */
	hid0 &= ~(7 << 21);
	/* Enable doze bit */
	hid0 |= (1 << 23);
	asm volatile("mtspr 1008,%0" : : "r"(hid0));
}

/*! \brief Switch to Nap mode
 *
 * Switches the power-save mode to nap.
 * \sa doze_mode(), sleep_mode(), power_save()
 */
static inline void nap_mode()
{
	/* HID0 */
	u32 hid0;
	asm volatile("mfspr %0,1008" : "=r"(hid0));
	/* Disable sleep, nap and doze bits */
	hid0 &= ~(7 << 21);
	/* Enable nap bit */
	hid0 |= (1 << 22);
	asm volatile("mtspr 1008,%0" : : "r"(hid0));
}

/*! \brief Switch to Sleep mode
 *
 * Switches the power-save mode to sleep.
 * \sa doze_mode(), nap_mode(), power_save()
 */
static inline void sleep_mode()
{
	/* HID0 */
	u32 hid0;
	asm volatile("mfspr %0,1008" : "=r"(hid0));
	/* Disable sleep, nap and doze bits */
	hid0 &= ~(7 << 21);
	/* Enable sleep bit */
	hid0 |= (1 << 21);
	asm volatile("mtspr 1008,%0" : : "r"(hid0));
}

/*! \brief Switch to power-save mode
 *
 * Switches the the power-save mode that was previously set.
 * \sa doze_mode(), nap_mode(), sleep_mode()
 */
static inline void power_save()
{
	/* Switch to power-save mode */
	asm volatile(
		"mfmsr	0		\n"
		"oris	0, 0, 0x80	\n"
		"sync			\n"
		"mtmsr	0		\n"
		"isync			\n"
		: : : "0");
}

#endif

