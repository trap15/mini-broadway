/*
	libbroadway - A general purpose library to control the Wii.
	Public PowerPC-side interface to MINI

Copyright (C) 2009			Andre Heider "dhewg" <dhewg@wiibrew.org>
Copyright (C) 2009			Haxx Enterprises <bushing@gmail.com>
Copyright (C) 2009			John Kelley <wiidev@kelley.ca>
Copyright (C) 2008, 2009		Sven Peter <svenpeter@gmail.com>
Copyright (C) 2010			Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of several MINI calls.
 */

#ifndef __MINI_IPC_H__
#define __MINI_IPC_H__

#define SDHC_ENOCARD		-0x1001	/*!< No card inserted */
#define SDHC_ESTRANGE		-0x1002	/*!< ??? */
#define SDHC_EOVERFLOW		-0x1003	/*!< Overflow */
#define SDHC_ETIMEDOUT		-0x1004	/*!< Request timed out */
#define SDHC_EINVAL		-0x1005	/*!< Invalid */
#define SDHC_EIO		-0x1006	/*!< I/O error */

#define SDMMC_NO_CARD		1	/*!< No card inserted */
#define SDMMC_NEW_CARD		2	/*!< New card */
#define SDMMC_INSERTED		3	/*!< Card inserted */

#define NAND_ECC_OK		0	/*!< ECC is proper */
#define NAND_ECC_CORRECTED	1	/*!< ECC was corrected */
#define NAND_ECC_UNCORRECTABLE -1	/*!< ECC was uncorrectable */

int sd_get_state(void);
int sd_protected(void);
int sd_mount(void);
int sd_select(void);
int sd_read(u32 start_block, u32 blk_cnt, void *buffer);
int sd_write(u32 start_block, u32 blk_cnt, const void *buffer);
u32 sd_getsize(void);

/*! \brief Reboot the PowerPC to a specific vector
 *
 * \param addr the address the PowerPC will boot at.
 * \param len how long the code at addr is
 * \return Anything must be an error code
 */
int ipc_powerpc_boot(const void *addr, u32 len);
/*! \brief Reboot the PowerPC to a file
 *
 * \param file the path to the file that will be launched.
 * \return Anything must be an error code
 */
int ipc_powerpc_boot_file(const char *file);

#define TMD_BM_MARK(x) ((u16*)&(x->reserved[4]))
// 'BM'
#define TMD_BM_MAGIC 0x424d

typedef struct sig_rsa2048_t sig_rsa2048_t;
struct sig_rsa2048_t {
	u32	type;
	u8	sig[256];
	u8	fill[60];
} __attribute__((packed));

typedef struct tmd_content_t tmd_content_t;
struct tmd_content_t {
	u32	cid;
	u16	index;
	u16	type;
	u64	size;
	u8	hash[20];
} __attribute__((packed));

typedef struct tmd_t tmd_t;
struct tmd_t {
	sig_rsa2048_t	signature;
	char		issuer[0x40];
	u8		version;
	u8		ca_crl_version;
	u8		signer_crl_version;
	u8		fill2;
	u64		sys_version;
	u64		title_id;
	u32		title_type;
	u16		group_id;
	u16		zero;
	u16		region;
	u8		ratings[16];
	u8		reserved[42];
	u32		access_rights;
	u16		title_version;
	u16		num_contents;
	u16		boot_index;
	u16		fill3;
	tmd_content_t	boot_content;
} __attribute__((packed));

/*! \brief Return to boot2 with modified launch title
 *
 * \param hi the high 32bits of the launch title
 * \param lo the low 32bits of the launch title
 * \return Anything must be an error code
 */
u32 boot2_run(u32 hi, u32 lo);
/*! \brief Get boot2's TMD
 *
 * \return boot2's TMD, or (tmd_t*)-1.
 */
tmd_t *boot2_tmd(void);

typedef struct otp_t otp_t;
struct otp_t {
	u8	boot1_hash[20];
	u8	common_key[16];
	u32	ng_id;
	union { // first two bytes of nand_hmac overlap last two bytes of ng_priv. no clue why
		struct {
			u8 ng_priv[30];
			u8 _wtf1[18];
		};
		struct {
			u8 _wtf2[28];
			u8 nand_hmac[20];
		};
	};
	u8	nand_key[16];
	u8	rng_key[16];
	u32	unk1;
	u32	unk2; // 0x00000007
} __attribute__((packed));

typedef struct eep_ctr_t eep_ctr_t;
struct eep_ctr_t {
	u8	boot2version;
	u8	unknown1;
	u8	unknown2;
	u8	pad;
	u32	update_tag;
	u16	checksum;
} __attribute__((packed));

typedef struct seeprom_t seeprom_t;
struct seeprom_t {
	union {
		struct {
			u32		ms_key_id;
			u32		ca_key_id;
			u32		ng_key_id;
			u8		ng_sig[60];
			eep_ctr_t	counters[2];
			u8		fill[0x18];
			u8		korean_key[16];
		};
		u8 data[256];
	};
} __attribute__((packed));

/*! \brief Get OTP data
 *
 * \param otp a pointer to an otp_t to be filled.
 */
void getotp(otp_t *otp);
/*! \brief Get SEEPROM data
 *
 * \param seeprom a pointer to an seeprom_t to be filled.
 */
void getseeprom(seeprom_t *seeprom);
/*! \brief Get the Git version of MINI
 *
 * \param buf buffer to be filled.
 * \param len length of the buffer to be filled.
 */
void getMiniGitVer(char *buf, u16 len);
	
/*! \brief Reset the AES engine
 */
void aes_reset(void);
/*! \brief Set the key for the AES engine
 *
 * \param key the key to be used.
 */
void aes_set_key(u8 *key);
/*! \brief Set the IV for the AES engine
 *
 * \param iv the IV to be used.
 */
void aes_set_iv(u8 *iv);
/*! \brief Decrypt data using the AES engine
 *
 * \param src the data to decrypt. (16 byte aligned)
 * \param dst the destination for the decrypted data. (16 byte aligned)
 * \param blocks how many blocks to decrypt. (blocks are 16 bytes)
 * \param keep_iv if 1, chain from last command. Otherwise use the set IV.
 */
void aes_decrypt(u8 *src, u8 *dst, u32 blocks, u8 keep_iv);

void nand_reset(void);
u32 nand_getid(void);
u8 nand_status(void);
int nand_read(u32 pageno, void *data, void *ecc);
void nand_write(u32 pageno, void *data, void *ecc);
void nand_erase(u32 pageno);

/*! \brief Load code into the ARM
 *
 * \param code the code to be sent.
 * \param size the length of the code to be sent.
 * \return 0 on success, error code otherwise
 */
int slave_load(void* code, u32 size);
/*! \brief Schedule execution of code
 */
void slave_execute(void);
/*! \brief Load parameter data into the ARM
 *
 * \param data the data to be sent.
 * \param size the length of the data to be sent.
 */
void slave_params(void* data, u32 size);
/*! \brief Retrieve results of executed code
 *
 * \return Memory pointing to the results of the executed code.
 */
void* slave_results(void);
/*! \brief Checks if the slave is done executing
 *
 * \return 1 if the slave is still executing, 0 if the slave is done.
 */
int slave_executing(void);


#endif

