/*
	libbroadway - A general purpose library to control the Wii.
	Paired Singles manipulation

Copyright (C) 2010	Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of the paired singles instructions.
 * These functions are better documented on http://wiibrew.org/wiki/Paired_single
 *
 * \todo Comparison functions
 * \bug madds1, muls1, merge10, and merge11 are broken/act oddly.
 */

#ifndef __PAIRED_H__
#define __PAIRED_H__

/*! De/quantization formats
 */
typedef enum {
	GQR_TYPE_FLOAT = 0,	/*!< Floating point (no scale) */
	GQR_TYPE_U8 = 4,	/*!< Unsigned 8bit integer */
	GQR_TYPE_U16,		/*!< Unsigned 16bit integer */
	GQR_TYPE_S8,		/*!< Signed 8bit integer */
	GQR_TYPE_S16,		/*!< Signed 16bit integer */
} gqr_type_t;

/*! \brief Sets up a GQR
 *
 * This function loads a Graphics Quantization Register with the specified
 * parameters, to prepare for loading a Paired Single.
 * \param gqr which GQR to load.
 * \param mode whether to change the load or store information. 0 for store, 1 for load.
 * \param type the conversion type. (as described by the gqr_load_t enum)
 * \param scale the conversion scale value. This is only applied to integer types, and 
 *              a signed integer that scales as in val = num / (2^scale).
 * \sa paired_get_gqr()
 */
void paired_set_gqr(int gqr, int mode, gqr_type_t type, int scale);
/*! \brief Gets information from a GQR
 *
 * \param gqr which GQR to obtain information from.
 * \param mode whether to get load or store information. 0 for store, 1 for load.
 * \param type a pointer to a variable to hold the conversion type.
 * \param scale a pointer to a variable to hold the conversion scale value.
 * \sa paired_set_gqr()
 */
void paired_get_gqr(int gqr, int mode, gqr_type_t *type, int *scale);

/*! \brief Loads a Paired Single
 *
 * \param gqr the GQR to use for dequantization of the data.
 * \param data a pointer to the data to dequantize into the paired single. Must be a 2-element array.
 * \param single if this is non-zero, only one number will be dequantized into PS0.
 *               PS1 will be loaded with 1.0.
 * \return A double containing the paired singles. DO NOT MODIFY THIS WITHOUT USING THE
 *         paired_ FUNCTIONS OR BAD THINGS WILL HAPPEN!
 */
#define paired_load(gqr, data, single) ({		\
	double pair;					\
	asm volatile					\
	("	psq_lx		%0, 0, %1, 0, %3\n"	\
	 : "=f"(pair) /* The FPR target */		\
	 : "rm"(data), "i"(single), "i"(gqr)		\
	);						\
	pair;						\
})
/* Har har, I used indexed format for cheatz */

/*! \brief Stores a Paired Single
 *
 * \param gqr the GQR to use for quantization of the data.
 * \param data a pointer to which the data quantized from the paired single will be placed. Must a 2-element array.
 * \param single if this is non-zero, only one number will be quantized from PS0.
 * \param pair A double containing the paired singles.
 */
#define paired_store(gqr, data, single, pair)		\
	asm volatile					\
	("	psq_st		%1, %0, 0, %3\n"	\
	 : "=m"(data)					\
	 : "f"(pair), "i"(single), "i"(gqr)		\
	);

/*! \brief Absolute value of a Paired Single
 *
 * \param pair the pair to get the absolute values of.
 * \return The resulting pair.
 */
#define paired_abs(pair) ({			\
	double out;				\
	asm volatile				\
	("	ps_abs		%0, %1\n"	\
	 : "=f"(out)				\
	 : "f"(pair)				\
	);					\
	out;					\
})

/*! \brief Negative absolute value of a Paired Single
 *
 * \param pair the pair to get the negative absolute values of.
 * \return The resulting pair.
 */
#define paired_nabs(pair) ({			\
	double out;				\
	asm volatile				\
	("	ps_nabs		%0, %1\n"	\
	 : "=f"(out)				\
	 : "f"(pair)				\
	);					\
	out;					\
})

/*! \brief Negative value of a Paired Single
 *
 * \param pair the pair to get the negative of.
 * \return The resulting pair.
 */
#define paired_neg(pair) ({			\
	double out;				\
	asm volatile				\
	("	ps_neg		%0, %1\n"	\
	 : "=f"(out)				\
	 : "f"(pair)				\
	);					\
	out;					\
})

/*! \brief Estimated reciprocal of a Paired Single
 *
 * \param pair the pair to get the estimated reciprocal of.
 * \return The resulting pair.
 */
#define paired_reciprocal(pair) ({		\
	double out;				\
	asm volatile				\
	("	ps_res		%0, %1\n"	\
	 : "=f"(out)				\
	 : "f"(pair)				\
	);					\
	out;					\
})

/*! \brief Estimated reciprocal of the square root of a Paired Single
 *
 * \param pair the pair to get the estimated reciprocal of the square root of.
 * \return The resulting pair.
 */
#define paired_sqrt_reciprocal(pair) ({		\
	double out;				\
	asm volatile				\
	("	ps_rsqrte	%0, %1\n"	\
	 : "=f"(out)				\
	 : "f"(pair)				\
	);					\
	out;					\
})

/*! \brief Add Paired Singles
 *
 * \param pair0 the first pair.
 * \param pair1 the seconds pair.
 * \return The resulting pair.
 */
#define paired_add(pair0, pair1) ({		\
	double out;				\
	asm volatile				\
	("	ps_add		%0, %1, %2\n"	\
	 : "=f"(out)				\
	 : "f"(pair0), "f"(pair1)		\
	);					\
	out;					\
})

/*! \brief Divide Paired Singles
 *
 * \param dend the dividend pair.
 * \param sor the divisor pair.
 * \return The quotient pair.
 */
#define paired_div(dend, sor) ({		\
	double out;				\
	asm volatile				\
	("	ps_div		%0, %1, %2\n"	\
	 : "=f"(out)				\
	 : "f"(dend), "f"(sor)			\
	);					\
	out;					\
})

/*! \brief Multiply Paired Singles
 *
 * \param pair0 the first pair.
 * \param pair1 the seconds pair.
 * \return The product pair.
 */
#define paired_mul(pair0, pair1) ({		\
	double out;				\
	asm volatile				\
	("	ps_mul		%0, %1, %2\n"	\
	 : "=f"(out)				\
	 : "f"(pair0), "f"(pair1)		\
	);					\
	out;					\
})

/*! \brief Subtract Paired Singles
 *
 * \param uend the minuend.
 * \param hend the subtrahend.
 * \return The difference pair.
 */
#define paired_sub(uend, hend) ({		\
	double out;				\
	asm volatile				\
	("	ps_sub		%0, %1, %2\n"	\
	 : "=f"(out)				\
	 : "f"(uend), "f"(hend)			\
	);					\
	out;					\
})

/* TODO: Comparison functions! */

/*! \brief Multiply then add Paired Singles
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair.
 * \param add the addition pair.
 * \return The resulting pair.
 */
#define paired_madd(mul0, mul1, add) ({			\
	double out;					\
	asm volatile					\
	("	ps_madd		%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1), "f"(add)		\
	);						\
	out;						\
})

/*! \brief Multiply then add Paired Singles Scalar high
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair. (only PS0 is used)
 * \param add the addition pair.
 * \return The resulting pair.
 */
#define paired_madds0(mul0, mul1, add) ({		\
	double out;					\
	asm volatile					\
	("	ps_madds0	%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1), "f"(add)		\
	);						\
	out;						\
})

/*! \brief Multiply then add Paired Singles Scalar low
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair. (only PS1 is used)
 * \param add the addition pair.
 * \return The resulting pair.
 */
#define paired_madds1(mul0, mul1, add) ({		\
	double out;					\
	asm volatile					\
	("	ps_madds1	%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1), "f"(add)		\
	);						\
	out;						\
})

/*! \brief Multiply then subtract Paired Singles
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair.
 * \param sub the subtraction pair.
 * \return The resulting pair.
 */
#define paired_msub(mul0, mul1, sub) ({			\
	double out;					\
	asm volatile					\
	("	ps_msub		%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1), "f"(sub)		\
	);						\
	out;						\
})

/*! \brief Multiply Scalar high
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair. (only PS0 is used)
 * \return The product pair.
 */
#define paired_muls0(mul0, mul1) ({			\
	double out;					\
	asm volatile					\
	("	ps_muls0	%0, %1, %2\n"		\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1)				\
	);						\
	out;						\
})

/*! \brief Multiply Scalar low
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair. (only PS1 is used)
 * \return The product pair.
 */
#define paired_muls1(mul0, mul1) ({			\
	double out;					\
	asm volatile					\
	("	ps_muls1	%0, %1, %2\n"		\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1)				\
	);						\
	out;						\
})

/*! \brief Negative multiply then add Paired Singles
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair.
 * \param add the addition pair.
 * \return The resulting pair.
 */
#define paired_nmadd(mul0, mul1, add) ({		\
	double out;					\
	asm volatile					\
	("	ps_nmadd	%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1), "f"(add)		\
	);						\
	out;						\
})

/*! \brief Negative multiply then subtract Paired Singles
 *
 * \param mul0 the first multiply pair.
 * \param mul1 the other multiply pair.
 * \param sub the subtraction pair.
 * \return The resulting pair.
 */
#define paired_nmsub(mul0, mul1, sub) ({		\
	double out;					\
	asm volatile					\
	("	ps_nmsub	%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(mul0), "f"(mul1), "f"(sub)		\
	);						\
	out;						\
})

/*! \brief Merge high
 *
 * \param pair0 the first pair. (only PS0 is used)
 * \param pair1 the other pair. (only PS0 is used)
 * \return The merged pair.
 */
#define paired_merge00(pair0, pair1) ({			\
	double out;					\
	asm volatile					\
	("	ps_merge00	%0, %1, %2\n"		\
	 : "=f"(out)					\
	 : "f"(pair0), "f"(pair1)			\
	);						\
	out;						\
})

/*! \brief Merge direct
 *
 * \param pair0 the first pair. (only PS0 is used)
 * \param pair1 the other pair. (only PS1 is used)
 * \return The merged pair.
 */
#define paired_merge01(pair0, pair1) ({			\
	double out;					\
	asm volatile					\
	("	ps_merge01	%0, %1, %2\n"		\
	 : "=f"(out)					\
	 : "f"(pair0), "f"(pair1)			\
	);						\
	out;						\
})

/*! \brief Merge swapped
 *
 * \param pair0 the first pair. (only PS1 is used)
 * \param pair1 the other pair. (only PS0 is used)
 * \return The merged pair.
 */
#define paired_merge10(pair0, pair1) ({			\
	double out;					\
	asm volatile					\
	("	ps_merge10	%0, %1, %2\n"		\
	 : "=f"(out)					\
	 : "f"(pair0), "f"(pair1)			\
	);						\
	out;						\
})

/*! \brief Merge low
 *
 * \param pair0 the first pair. (only PS1 is used)
 * \param pair1 the other pair. (only PS1 is used)
 * \return The merged pair.
 */
#define paired_merge11(pair0, pair1) ({			\
	double out;					\
	asm volatile					\
	("	ps_merge11	%0, %1, %2\n"		\
	 : "=f"(out)					\
	 : "f"(pair0), "f"(pair1)			\
	);						\
	out;						\
})

/*! \brief Select
 *
 * if(sel(ps0) >= 0)
 *         ret(ps0) = pos(ps0)
 * else
 *         ret(ps0) = neg(ps0)
 * if(sel(ps1) >= 0)
 *         ret(ps1) = pos(ps1)
 * else
 *         ret(ps1) = neg(ps1)
 * \param sel the selector pair.
 * \param pos the pair to use for positives.
 * \param neg the pair to use for negatives.
 * \return The resulting pair
 */
#define paired_select(sel, pos, neg) ({			\
	double out;					\
	asm volatile					\
	("	ps_sel		%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(sel), "f"(pos), "f"(neg)			\
	);						\
	out;						\
})

/*! \brief Vector Sum high
 *
 * \param pair0 the first addition pair. (only PS0 used)
 * \param pair1 the second addition pair. (only PS1 used)
 * \param pair2 the copy pair. (only PS1 used)
 * \return The resulting pair.
 */
#define paired_sum0(pair0, pair1, pair2) ({		\
	double out;					\
	asm volatile					\
	("	ps_sum0		%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(pair0), "f"(pair2), "f"(pair1)		\
	);						\
	out;						\
})

/*! \brief Vector Sum low
 *
 * \param pair0 the first addition pair. (only PS0 used)
 * \param pair1 the second addition pair. (only PS1 used)
 * \param pair2 the copy pair. (only PS0 used)
 * \return The resulting pair.
 */
#define paired_sum1(pair0, pair1, pair2) ({		\
	double out;					\
	asm volatile					\
	("	ps_sum1		%0, %1, %2, %3\n"	\
	 : "=f"(out)					\
	 : "f"(pair0), "f"(pair2), "f"(pair1)		\
	);						\
	out;						\
})

#endif

