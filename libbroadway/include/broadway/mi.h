/*
	libbroadway - A general purpose library to control the Wii.
	MI support

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of the Memory Interface (MI). Has 4 protection zones that can
 * be used to detect when writes, reads, or both happen upon specified regions
 * of memory. Buggy.
 */

#ifndef __MI_H__
#define __MI_H__

#include <types.h>
#include <broadway.h>

/*! \brief Initialize the MI subsystem
 *
 * Initializes the MI subsystem, registers the handler for the MI interrupt
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa mi_shutdown()
 */
int mi_initialize(void);
/*! \brief Shut down the MI subsystem
 *
 * Shuts down the MI subsystem, unregisters the handler for the MI interrupt
 * and performs all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa mi_initialize()
 */
int mi_shutdown(void);

/*! \brief Protects a region of memory
 *
 * \param addr the start address of the protected region.
 * \param size how large the protected region is in bytes.
 * \param r whether the memory region can be read from
 * \param w whether the memory region can be written to
 * \param exec the callback that is called when the region is accessed in a
 *             way that is not allowed.
 * \param data data provided to the callback when it fires.
 * \return The region number used to protect the memory. This number is also
 *         which IRQ is fired when it is accessed badly. If -1, an error
 *         occurred trying to protect the region.
 * \sa irq_mi_enable(), irq_mi_disable(), and mi_unprotect_region()
 */
int mi_protect_region(void* addr, int size, BOOL r, BOOL w, int (*exec)(u32 irq, void* data), void* data);
/*! \brief Unprotects a region of memory
 *
 * \param rgn the region number to unprotect. This is given when you protected
 *            the region.
 * \return Whether the unprotection worked.
 */
int mi_unprotect_region(int rgn);

/*! \brief Enables a specific MI IRQ
 *
 * \param irq the IRQ to enable
 * \sa irq_mi_disable()
 */
void irq_mi_enable(u32 irq);
/*! \brief Disables a specific MI IRQ
 *
 * \param irq the IRQ to disable
 * \sa irq_mi_enable()
 */
void irq_mi_disable(u32 irq);

#endif

