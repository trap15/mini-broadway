/*
	libbroadway - A general purpose library to control the Wii.
	AI support

Copyright (C) 2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of the Audio Interface (AI). Controls various portions of the
 * audio output (including volume and sample rate).
 */

#ifndef __AI_H__
#define __AI_H__

#include <types.h>
#include <broadway.h>

/*! The sample rates supported by AI
 */
typedef enum {
	AI_SAMPLE_RATE_48000 = 0,	/*!< 48kHz */
	AI_SAMPLE_RATE_32000,		/*!< 32kHz */
} ai_sample_rate_t;

/*! \brief Initialize the AI subsystem
 *
 * Initializes the AI subsystem, registers the handler for the AI interrupt
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa ai_shutdown()
 */
int ai_initialize(void);
/*! \brief Shut down the AI subsystem
 *
 * Shuts down the AI subsystem, unregisters the handler for the AI interrupt
 * and performs all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa ai_initialize()
 */
int ai_shutdown(void);

/*! \brief Set the sample rate
 *
 * Must only be called when the AI is not playing audio.
 * \param rate a value from ai_sample_rate_t specifying the sample rate to use.
 * \sa ai_get_sample_rate()
 */
void ai_set_sample_rate(ai_sample_rate_t rate);
/*! \brief Get the sample rate
 *
 * \return The sample rate that AI was set to use. -1 if there was an error.
 * \sa ai_set_sample_rate()
 */
ai_sample_rate_t ai_get_sample_rate(void);

/*! \brief Enable the sample-reached IRQ
 *
 * Turns on the IRQ that fires when the AI plays a specified sample.
 * \param on_sample the sample that the IRQ fires on.
 * \param exec the callback that is called when the IRQ fires.
 * \param data data provided to the callback when it fires.
 * \sa ai_disable_irq()
 */
void ai_enable_irq(int on_sample, int (*exec)(u32 irq, void* data), void* data);
/*! \brief Disable the sample-reached IRQ
 *
 * Turns off the IRQ that fires when the AI plays a specific sample.
 * \sa ai_enable_irq()
 */
void ai_disable_irq(void);

/*! \brief Sets the volume of the stereo output
 *
 * Sets the volume of the output sound, with the left and right speakers
 * being changable independantly
 * \param left the volume on the left speaker. 0 is mute, 0xFF is maximum.
 * \param right the volume on the right speaker. 0 is mute, 0xFF is maximum.
 * \sa ai_get_volume()
 */
void ai_set_volume(u8  left, u8  right);
/*! \brief Gets the volume of the stereo output
 *
 * \param left a pointer to which the left volume is put. NULL if you don't
 *             want the output.
 * \param right a pointer to which the right volume is put. NULL if you don't
 *             want the output.
 * \sa ai_set_volume()
 */
void ai_get_volume(u8* left, u8* right);

/*! \brief Gets how many samples have been played
 *
 * Gets the number of stereo samples that have been played since the counter
 * was last reset (either by a system reset or a manual reset.)
 * \return How many samples were played. 0xFFFFFFFF if an error occurred.
 * \sa ai_reset_sample_counter
 */
u32 ai_samples_played(void);
/*! \brief Resets the sample played counter
 */
void ai_reset_sample_counter(void);

/*! \brief Returns whether AI is playing audio or not
 *
 * \return Returns 1 if it was playing, 0 otherwise. -1 if an error occurred.
 * \sa ai_play()
 */
int ai_is_playing(void);
/*! \brief Starts or stops the AI from playing audio.
 *
 * \param play if 1 starts playing, or if 0 stops playing.
 * \return Returns 1 if it was playing, 0 otherwise. -1 if an error occurred.
 * \sa ai_is_playing()
 */
int ai_play(int play);

#endif

