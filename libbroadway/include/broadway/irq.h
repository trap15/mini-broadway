/*
	libbroadway - A general purpose library to control the Wii.
	IRQ support

Copyright (C) 2009		Bernhard Urban <lewurm@gmx.net>
Copyright (C) 2009		Sebastian Falbesoner <sebastian.falbesoner@gmail.com>
Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file 
 * This file contains all functions related to Hollywood and Broadway IRQs, and
 * IRQ number definitions for all other subsystems.
 */

#ifndef __IRQ_H__
#define __IRQ_H__

#include <types.h>
#include <broadway/hollywood.h>

/* Broadway Processor Interface Registers */
#define HW_BWIRQFLAG		PI_INT_CAUSE	/*!< Broadway IRQ flags */
#define HW_BWIRQMASK		PI_INT_MASK	/*!< Broadway IRQ mask */

#define IRQ_HW_TIMER		(0)	/*!< The Hollywood IRQ fired by the Starlet Timer */
#define IRQ_HW_NAND		(1)	/*!< The Hollywood IRQ fired by the NAND */
#define IRQ_HW_AES		(2)	/*!< The Hollywood IRQ fired by the AES engine */
#define IRQ_HW_SHA1		(3)	/*!< The Hollywood IRQ fired by the SHA-1 engine */
#define IRQ_HW_EHCI		(4)	/*!< The Hollywood IRQ fired by the EHCI controller */
#define IRQ_HW_OHCI0		(5)	/*!< The Hollywood IRQ fired by the OHCI controller 0 */
#define IRQ_HW_OHCI1		(6)	/*!< The Hollywood IRQ fired by the OHCI controller 1 */
#define IRQ_HW_SDHC		(7)	/*!< The Hollywood IRQ fired by the SDHC controller */
#define IRQ_HW_WIFI		(8)	/*!< The Hollywood IRQ fired by the WiFi controller */
#define IRQ_HW_GPIO1B		(10)	/*!< The Hollywood IRQ fired by the Broadway GPIOs */
#define IRQ_HW_GPIO1		(11)	/*!< The Hollywood IRQ fired by the Starlet GPIOs */
#define IRQ_HW_RESET		(17)	/*!< The Hollywood IRQ fired by the Reset Button */
#define IRQ_HW_PPCIPC		(30)	/*!< The Hollywood IRQ fired by the Broadway IPC */
#define IRQ_HW_IPC		(31)	/*!< The Hollywood IRQ fired by the Starlet IPC */
/* Not a real IRQ, just a little helpful something */
#define IRQ_HW_MAX		(32)

// http://hitmen.c02.at/files/yagcd/yagcd/chap5.html#sec5.4
#define IRQ_BW_RESET		(1)	/*!< The Broadway IRQ fired by the Reset Switch */
#define IRQ_BW_DI		(2)	/*!< The Broadway IRQ fired by the DVD Interface */
#define IRQ_BW_SI		(3)	/*!< The Broadway IRQ fired by the Serial Interface */
#define IRQ_BW_EXI		(4)	/*!< The Broadway IRQ fired by the External Interface */
#define IRQ_BW_AI 	 	(5)	/*!< The Broadway IRQ fired by the Audio Interface */
#define IRQ_BW_DSP 	 	(6)	/*!< The Broadway IRQ fired by the DSP */
#define IRQ_BW_MEM 	 	(7)	/*!< The Broadway IRQ fired by the Memory Interface */
#define IRQ_BW_VI 	 	(8)	/*!< The Broadway IRQ fired by the Video Interface */
#define IRQ_BW_PE_TOKEN		(9)	/*!< The Broadway IRQ fired by the Pixel Engine for Tokens */
#define IRQ_BW_PE_FINISH	(10)	/*!< The Broadway IRQ fired by the Pixel Engine when it finishes */
#define IRQ_BW_CP 	 	(11)	/*!< The Broadway IRQ fired by the Command Processor FIFO */
#define IRQ_BW_DEBUG		(12)	/*!< The Broadway IRQ fired by the Debugger */
#define IRQ_BW_HSP 	 	(13)	/*!< The Broadway IRQ fired by the Highspeed Port */
#define IRQ_BW_HW 		(14)	/*!< The Broadway IRQ fired by the Hollywood IRQs */
#define IRQ_BW_RESET_SW 	(16)	/*!< The Broadway IRQ fired by the Reset Switch State */
/* Not a real IRQ, just a little helpful something */
#define IRQ_BW_MAX		(32)

#define IRQ_MI_MEM0		(0)	/*!< The MI IRQ fired for region 0 */
#define IRQ_MI_MEM1		(1)	/*!< The MI IRQ fired for region 1 */
#define IRQ_MI_MEM2		(2)	/*!< The MI IRQ fired for region 2 */
#define IRQ_MI_MEM3		(3)	/*!< The MI IRQ fired for region 3 */
#define IRQ_MI_MEMADDR		(4)	/*!< The MI IRQ fired for all regions (?) */
/* Not a real IRQ, just a little helpful something */
#define IRQ_MI_MAX		(5)

#define IRQ_EXI_EXI0		(0)	/*!< EXI IRQ for Channel 0 */
#define IRQ_EXI_TC0		(1)	/*!< Transfer complete IRQ for Channel 0 */
#define IRQ_EXI_EXT0		(2)	/*!< EXT IRQ for Channel 0 */
#define IRQ_EXI_EXI1		(3)	/*!< EXI IRQ for Channel 1 */
#define IRQ_EXI_TC1		(4)	/*!< Transfer complete IRQ for Channel 1 */
#define IRQ_EXI_EXT1		(5)	/*!< EXT IRQ for Channel 1 */
#define IRQ_EXI_EXI2		(6)	/*!< EXI IRQ for Channel 2 */
#define IRQ_EXI_TC2		(7)	/*!< Transfer complete IRQ for Channel 2 */
#define IRQ_EXI_EXT2		(8)	/*!< EXT IRQ for Channel 2 */
/* Not a real IRQ, just a little helpful something */
#define IRQ_EXI_MAX		(9)

#define IRQ_DI_BREAK		(0)	/*!< Break complete DI IRQ */
#define IRQ_DI_TC		(1)	/*!< Transfer complete DI IRQ */
#define IRQ_DI_ERROR		(2)	/*!< Device error DI IRQ */
#define IRQ_DI_COVER		(3)	/*!< Cover DI IRQ */
/* Not a real IRQ, just a little helpful something */
#define IRQ_DI_MAX		(4)

#define IRQF(n)			(1 << n)	/*!< Converts IRQ number to IRQ mask */
#define IRQF_HW_TIMER		(IRQF(IRQ_HW_TIMER))	/*!< Flag for IRQ_HW_TIMER */
#define IRQF_HW_NAND		(IRQF(IRQ_HW_NAND))	/*!< Flag for IRQ_HW_NAND */
#define IRQF_HW_AES		(IRQF(IRQ_HW_AES))	/*!< Flag for IRQ_HW_AES */
#define IRQF_HW_SHA1		(IRQF(IRQ_HW_SHA1))	/*!< Flag for IRQ_HW_SHA1 */
#define IRQF_HW_EHCI		(IRQF(IRQ_HW_EHCI))	/*!< Flag for IRQ_HW_EHCI */
#define IRQF_HW_OHCI0		(IRQF(IRQ_HW_OHCI0))	/*!< Flag for IRQ_HW_OHCI0 */
#define IRQF_HW_OHCI1		(IRQF(IRQ_HW_OHCI1))	/*!< Flag for IRQ_HW_OHCI1 */
#define IRQF_HW_SDHC		(IRQF(IRQ_HW_SDHC))	/*!< Flag for IRQ_HW_SDHC */
#define IRQF_HW_WIFI		(IRQF(IRQ_HW_WIFI))	/*!< Flag for IRQ_HW_WIFI */
#define IRQF_HW_GPIO1B		(IRQF(IRQ_HW_GPIO1B))	/*!< Flag for IRQ_HW_GPIO1B */
#define IRQF_HW_GPIO1		(IRQF(IRQ_HW_GPIO1))	/*!< Flag for IRQ_HW_GPIO1 */
#define IRQF_HW_RESET		(IRQF(IRQ_HW_RESET))	/*!< Flag for IRQ_HW_RESET */
#define IRQF_HW_PPCIPC		(IRQF(IRQ_HW_PPCIPC))	/*!< Flag for IRQ_HW_PPCIPC */
#define IRQF_HW_IPC		(IRQF(IRQ_HW_IPC))	/*!< Flag for IRQ_HW_IPC */

#define IRQF_BW_RESET		(IRQF(IRQ_BW_RESET))	/*!< Flag for IRQ_BW_RESET */
#define IRQF_BW_DI		(IRQF(IRQ_BW_DI))	/*!< Flag for IRQ_BW_DI */
#define IRQF_BW_SI		(IRQF(IRQ_BW_SI))	/*!< Flag for IRQ_BW_SI */
#define IRQF_BW_EXI		(IRQF(IRQ_BW_EXI))	/*!< Flag for IRQ_BW_EXI */
#define IRQF_BW_AI		(IRQF(IRQ_BW_AI))	/*!< Flag for IRQ_BW_AI */
#define IRQF_BW_DSP		(IRQF(IRQ_BW_DSP))	/*!< Flag for IRQ_BW_DSP */
#define IRQF_BW_MEM		(IRQF(IRQ_BW_MEM))	/*!< Flag for IRQ_BW_MEM */
#define IRQF_BW_VI		(IRQF(IRQ_BW_VI))	/*!< Flag for IRQ_BW_VI */
#define IRQF_BW_PE_TOKEN	(IRQF(IRQ_BW_PE_TOKEN))	/*!< Flag for IRQ_BW_PE_TOKEN */
#define IRQF_BW_PE_FINISH	(IRQF(IRQ_BW_PE_FINISH))/*!< Flag for IRQ_BW_PE_FINISH */
#define IRQF_BW_CP		(IRQF(IRQ_BW_CP))	/*!< Flag for IRQ_BW_CP */
#define IRQF_BW_DEBUG		(IRQF(IRQ_BW_DEBUG))	/*!< Flag for IRQ_BW_DEBUG */
#define IRQF_BW_HSP		(IRQF(IRQ_BW_HSP))	/*!< Flag for IRQ_BW_HSP */
#define IRQF_BW_HW		(IRQF(IRQ_BW_HW))	/*!< Flag for IRQ_BW_HW */
#define IRQF_BW_RESET_SW	(IRQF(IRQ_BW_RESET_SW))	/*!< Flag for IRQ_BW_RESET_SW */

#define IRQF_MI_MEM0		(IRQF(IRQ_MI_MEM0))	/*!< Flag for IRQ_MI_MEM0 */
#define IRQF_MI_MEM1		(IRQF(IRQ_MI_MEM1))	/*!< Flag for IRQ_MI_MEM1 */
#define IRQF_MI_MEM2		(IRQF(IRQ_MI_MEM2))	/*!< Flag for IRQ_MI_MEM2 */
#define IRQF_MI_MEM3		(IRQF(IRQ_MI_MEM3))	/*!< Flag for IRQ_MI_MEM3 */
#define IRQF_MI_MEMADDR		(IRQF(IRQ_MI_MEMADDR))	/*!< Flag for IRQ_MI_MEMADDR */

#define IRQF_EXI_EXI0		(IRQF(IRQ_EXI_EXI0))	/*!< Flag for IRQ_EXI_EXI0 */
#define IRQF_EXI_TC0		(IRQF(IRQ_EXI_TC0))	/*!< Flag for IRQ_EXI_TC0 */
#define IRQF_EXI_EXT0		(IRQF(IRQ_EXI_EXT0))	/*!< Flag for IRQ_EXI_EXT0 */
#define IRQF_EXI_EXI1		(IRQF(IRQ_EXI_EXI1))	/*!< Flag for IRQ_EXI_EXI1 */
#define IRQF_EXI_TC1		(IRQF(IRQ_EXI_TC1))	/*!< Flag for IRQ_EXI_TC1 */
#define IRQF_EXI_EXT1		(IRQF(IRQ_EXI_EXT1))	/*!< Flag for IRQ_EXI_EXT1 */
#define IRQF_EXI_EXI2		(IRQF(IRQ_EXI_EXI2))	/*!< Flag for IRQ_EXI_EXI2 */
#define IRQF_EXI_TC2		(IRQF(IRQ_EXI_TC2))	/*!< Flag for IRQ_EXI_TC2 */
#define IRQF_EXI_EXT2		(IRQF(IRQ_EXI_EXT2))	/*!< Flag for IRQ_EXI_EXT2 */

#define IRQF_DI_BREAK		(IRQF(IRQ_DI_BREAK))	/*!< Flag for IRQ_DI_BREAK */
#define IRQF_DI_TC		(IRQF(IRQ_DI_TC))	/*!< Flag for IRQ_DI_TC */
#define IRQF_DI_ERROR		(IRQF(IRQ_DI_ERROR))	/*!< Flag for IRQ_DI_ERROR */
#define IRQF_DI_COVER		(IRQF(IRQ_DI_COVER))	/*!< Flag for IRQ_DI_COVER */

/*! A combination of all the Hollywood IRQ flags */
#define IRQF_HW_ALL	(IRQF_HW_TIMER  | IRQF_HW_NAND   | IRQF_HW_AES   | IRQF_HW_SHA1  | \
			 IRQF_HW_EHCI   | IRQF_HW_OHCI0  | IRQF_HW_OHCI1 | IRQF_HW_SDHC  | \
			 IRQF_HW_WIFI   | IRQF_HW_GPIO1B | IRQF_HW_GPIO1 | IRQF_HW_RESET | \
			 IRQF_HW_PPCIPC | IRQF_HW_IPC )

/*! A combination of all the Broadway IRQ flags */
#define IRQF_BW_ALL	(IRQF_BW_RESET    | IRQF_BW_DI        | IRQF_BW_SI       | IRQF_BW_EXI   | \
			 IRQF_BW_AI       | IRQF_BW_DSP       | IRQF_BW_MEM      | IRQF_BW_VI    | \
			 IRQF_BW_PE_TOKEN | IRQF_BW_PE_FINISH | IRQF_BW_CP       | IRQF_BW_DEBUG | \
			 IRQF_BW_HSP      | IRQF_BW_HW        | IRQF_BW_RESET_SW )

/*! A combination of all the MI IRQ flags */
#define IRQF_MI_ALL	(IRQF_MI_MEM0 | IRQF_MI_MEM1 | IRQF_MI_MEM2 | IRQF_MI_MEM3 | \
			 IRQF_MI_MEMADDR)

/*! A combination of all the EXI IRQ flags */
#define IRQF_EXI_ALL	(IRQF_EXI_EXI0 | IRQF_EXI_TC0 | IRQF_EXI_EXT0 | \
			 IRQF_EXI_EXI1 | IRQF_EXI_TC1 | IRQF_EXI_EXT1 | \
			 IRQF_EXI_EXI2 | IRQF_EXI_TC2 | IRQF_EXI_EXT2 )

/*! A combination of all the DI IRQ flags */
#define IRQF_DI_ALL	(IRQF_DI_BREAK | IRQF_DI_TC | IRQF_DI_ERROR | IRQF_DI_COVER)

/*! IRQ handler structure
 */
typedef struct {
	int	(*exec)(u32 irq, void* data);	/*!< The callback */
	void*	data;				/*!< Data provided to the callback */
} irq_handler_t;

/*! \brief Initialize the IRQs
 *
 * Initializes the IRQs, registers default IRQ handlers, and performs all 
 * other required initialization tasks.
 * \sa irq_shutdown()
 */
void irq_initialize(void);
/*! \brief Shuts down the IRQs
 *
 * Disables IRQs, and re-delegates all the IRQs back to the Starlet.
 * \sa irq_initialize()
 */
void irq_shutdown(void);

/*! \brief Enables a specific Broadway IRQ
 *
 * \param irq the IRQ to enable
 * \sa irq_bw_disable()
 */
void irq_bw_enable(u32 irq);
/*! \brief Disables a specific Broadway IRQ
 *
 * \param irq the IRQ to disable
 * \sa irq_bw_enable()
 */
void irq_bw_disable(u32 irq);
/*! \brief Enables a specific Hollywood IRQ
 *
 * \param irq the IRQ to enable
 * \sa irq_hw_disable()
 */
void irq_hw_enable(u32 irq);
/*! \brief Disables a specific Hollywood IRQ
 *
 * \param irq the IRQ to disable
 * \sa irq_hw_enable()
 */
void irq_hw_disable(u32 irq);

/*! \brief Enables IRQs
 *
 * Enables acceptance of IRQs
 * \sa irq_disable(), and irq_restore()
 */
void irq_enable(void);
/*! \brief Disables IRQs
 *
 * Disables acceptance of IRQs
 * \return A mask value that should be passed into irq_restore() when turning
 *         IRQs back on.
 * \sa irq_restore(), and irq_enable()
 */
u32  irq_disable(void);
/*! \brief Restores IRQ state
 *
 * Restores IRQ state from irq_disable().
 * \param was_on the mask returned from irq_disable()
 * \sa irq_disable(), and irq_enable()
 */
void irq_restore(u32 was_on);

/*! \brief Register a Hollywood IRQ handler
 *
 * \param irqn the IRQ whose handle is to be registered.
 * \param exec the callback to be called when the IRQ fires. Pass NULL to
 *             unregister the IRQ.
 * \param data the data to be passed to the IRQ when it fires.
 * \return Returns 1 on success, 0 on failure.
 * \sa irq_hw_get_handler()
 */
int irq_hw_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data);
/*! \brief Gets a Hollywood IRQ handler
 *
 * Gets the IRQ handler information for a Hollywood IRQ.
 * \param irqn the IRQ whose handler is to be returned.
 * \return The irq_handler_t structure containing information about the
 *         handler.
 * \sa irq_hw_register_handler()
 */
irq_handler_t irq_hw_get_handler(u32 irqn);
/*! \brief Register a Broadway IRQ handler
 *
 * \param irqn the IRQ whose handle is to be registered.
 * \param exec the callback to be called when the IRQ fires. Pass NULL to
 *             unregister the IRQ.
 * \param data the data to be passed to the IRQ when it fires.
 * \return Returns 1 on success, 0 on failure.
 */
int irq_bw_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data);
/*! \brief Gets a Broadway IRQ handler
 *
 * Gets the IRQ handler information for a Broadway IRQ.
 * \param irqn the IRQ whose handler is to be returned.
 * \return The irq_handler_t structure containing information about the
 *         handler.
 * \sa irq_bw_register_handler()
 */
irq_handler_t irq_bw_get_handler(u32 irqn);

#endif


