/*
	libbroadway - A general purpose library to control the Wii.
	DI support

Copyright (C) 2010-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

/*! \file
 * An abstraction of the Drive Interface (DI). Low level access is abstracted;
 * a higher level abstraction should be available soon.
 */

#ifndef __DI_H__
#define __DI_H__

#include <types.h>
#include <broadway.h>

/*! The known command numbers for di_send_command()
 */
typedef enum {
	DI_CMD_INQUIRY		= 0x12,	/*!< DMA, read, gets drive info */
	DI_CMD_DMAREAD		= 0xA8,	/*!< DMA, read, subcmd2 is di_dmaread_cmd_t */
	DI_CMD_SEEK		= 0xAB,	/*!< Imm, read, seeks the drive */
	DI_CMD_GETERROR		= 0xE0, /*!< Imm, read, request error status */
	DI_CMD_PLAYAUDIO	= 0xE1, /*!< Imm, read, play audio stream (?) */
	DI_CMD_AUDIOSTATUS	= 0xE2, /*!< Imm, read, request audio status */
	DI_CMD_STOPMOTOR	= 0xE3, /*!< Imm, read, stop drive motor */
	DI_CMD_DVDAUDIO		= 0xE4, /*!< Imm, read, subcmd1 is on/off */
	DI_CMD_DEBUG		= 0xFE, /*!< Imm, subcmd1 is di_debug_cmd_t */
	DI_CMD_UNLOCK		= 0xFF, /*!< Imm, read, unlock the drive */
} di_cmd_t;

/*! The known DI_CMD_DMAREAD subcommands
 */
typedef enum {
	DI_DMAREAD_SECTOR	= 0x00,	/*!< DMA, read, reads a sector */
	DI_DMAREAD_DISCID	= 0x40,	/*!< DMA, read, reads Disc ID/Init Disc */
} di_dmaread_cmd_t;

/*! The known DI_CMD_DEBUG subcommands
 */
typedef enum {
	DI_DEBUG_MEMORY		= 0x01, /*!< Imm, read/write memory/cache */
	DI_DEBUG_DRVCONTROL	= 0x11, /*!< Imm, read, various drive controls */
	DI_DEBUG_JUMP		= 0x12, /*!< Imm, read, JSR to off */
} di_debug_cmd_t;

/*! DI_DEBUG_DRVCONTROL subcommands
 */
typedef enum {
	DI_MOTOR_DOWN		= 0x0000, /*!< Spin down the motor */
	DI_MOTOR_UP		= 0x0100, /*!< Spin up the motor */
	DI_DRIVE_ACCEPT		= 0x0400, /*!< Force the drive to accept the disc */
	DI_DRIVE_CHECKDISK	= 0x0800, /*!< Force the drive to check the disc */
} di_drvctrl_cmd2_t;

/*! Error codes (top byte)
 */
typedef enum {
	DI_ERROR1_OK		= 0x00,	/*!< No error */
	DI_ERROR1_LIDOPEN	= 0x01,	/*!< Lid open */
	DI_ERROR1_DISKCHANGED	= 0x02,	/*!< Disc changed */
	DI_ERROR1_NODISC	= 0x03,	/*!< No disc */
	DI_ERROR1_MOTOROFF	= 0x04,	/*!< Motor off */
	DI_ERROR1_BADDISC	= 0x05,	/*!< Disc not initialized/disc ID not read */
} di_error1_t;

/*! Error codes (bottom 3 bytes)
 */
typedef enum {
	DI_ERROR2_OK		= 0x000000,	/*!< No error */
	DI_ERROR2_MOTORSTOP	= 0x020400,	/*!< Motor stopped */
	DI_ERROR2_DISCID	= 0x020401,	/*!< Disc ID not read */
	DI_ERROR2_COVEROPEN	= 0x023A00,	/*!< Medium not preset/Cover opened */
	DI_ERROR2_NOSEEK	= 0x030200,	/*!< No seek complete */
	DI_ERROR2_READERROR	= 0x031100,	/*!< Unrecovered read error */
	DI_ERROR2_TRANSFER	= 0x040800,	/*!< Transfer protocol error */
	DI_ERROR2_COMMAND	= 0x052000,	/*!< Invalid command operation code */
	DI_ERROR2_AUDIOBUF	= 0x052001,	/*!< Audio buffer not set */
	DI_ERROR2_LBARANGE	= 0x052100,	/*!< LBA out of range */
	DI_ERROR2_INVALIDFIELD	= 0x052400,	/*!< Invalid field in command packet */
	DI_ERROR2_AUDIOCMD	= 0x052401,	/*!< Invalid audio command */
	DI_ERROR2_CONFIG	= 0x052402,	/*!< Configuration out of permitted period */
	DI_ERROR2_ENDOFUSER	= 0x056300,	/*!< End of user area encountered on this track */
	DI_ERROR2_MEDIUMCHANGE	= 0x062800,	/*!< Medium may have changed */
	DI_ERROR2_OPREMOVAL	= 0x0B5A01,	/*!< Operator medium removal request */
} di_error2_t;

/*! \brief Initialize the DI subsystem
 *
 * Initializes the DI subsystem, registers the handler for the DI interrupt
 * and performs all other required initialization tasks.
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa di_shutdown()
 */
int di_initialize(void);
/*! \brief Shut down the DI subsystem
 *
 * Shuts down the DI subsystem, unregisters the handler for the DI interrupt
 * and performs all other required shut down tasks.
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa di_initialize()
 */
int di_shutdown(void);

/*! \brief Resets the drive
 *
 * \param hard if TRUE, perform a hard drive reset. If FALSE, just soft reset.
 */
void di_reset(BOOL hard);

/*! \brief Detects if the drive cover is open
 *
 * \return TRUE if the cover is open, FALSE if it's closed.
 */
BOOL di_cover_open(void);

/*! \brief Set the command to be sent for a transfer
 *
 * \param cmd the command to use.
 * \param subcmd1 a parameter to cmd.
 * \param subcmd2 a parameter to cmd.
 * \param off offset to data to use. (in 32bit words)
 * \param len length of the data to use.
 */
void di_set_command(di_cmd_t cmd, u8 subcmd1, u16 subcmd2, u32 off, u32 len);

/*! \brief Set DMA address and length
 *
 * \param addr address of the buffer in main memory. (32byte aligned)
 * \param len length of the buffer. (32byte aligned)
 */
void di_set_dma(u32 addr, u32 len);

/*! \brief Read the immediate data buffer
 *
 * \return The immediate data buffer.
 */
u32 di_immediate_recv(void);
/*! \brief Write the immediate data buffer
 *
 * \param data data to write to immediate data buffer.
 */
void di_immediate_send(u32 data);

/*! \brief Starts a transfer from the DI
 *
 * \param write if TRUE, this transfer is a write. If FALSE, it's a read.
 * \param dma if TRUE, this transfer is a DMA. If FALSE, it's an immediate transfer.
 * \sa di_transfer_ended()
 */
void di_transfer(BOOL write, BOOL dma);

/*! \brief Check if the DI transfer is complete
 *
 * \return TRUE if transfer is complete, FALSE if transfer is still in progress.
 * \sa di_transfer()
 */
BOOL di_transfer_ended(void);

/*! \brief Enables a specific DI IRQ
 *
 * \param irq the IRQ to enable
 * \sa irq_di_disable()
 */
void irq_di_enable(u32 irq);
/*! \brief Disables a specific DI IRQ
 *
 * \param irq the IRQ to disable
 * \sa irq_di_enable()
 */
void irq_di_disable(u32 irq);

/*! \brief Register a DI IRQ handler
 *
 * \param irqn the IRQ whose handle is to be registered.
 * \param exec the callback to be called when the IRQ fires. Pass NULL to
 *             unregister the IRQ.
 * \param data the data to be passed to the IRQ when it fires.
 * \return Returns 1 on success, 0 on failure.
 * \sa irq_di_get_handler()
 */
int irq_di_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data);
/*! \brief Gets a DI IRQ handler
 *
 * Gets the IRQ handler information for a DI IRQ.
 * \param irqn the IRQ whose handler is to be returned.
 * \return The irq_handler_t structure containing information about the
 *         handler.
 * \sa irq_di_register_handler()
 */
irq_handler_t irq_di_get_handler(u32 irqn);

#endif

