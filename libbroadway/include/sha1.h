/*! \file
 * A simple SHA-1 implementation.
 */

#ifndef __SHA1_H__
#define __SHA1_H__

/*! \brief Hashes a buffer using SHA-1
 *
 * \param ptr a pointer to the data to be hashed.
 * \param size the size of the data to be hashed in bytes.
 * \param outbuf a pointer to where the hash is to be placed. Must be at least
 *               20 bytes long.
 */
void SHA1(unsigned char *ptr, unsigned int size, unsigned char *outbuf);

#endif

