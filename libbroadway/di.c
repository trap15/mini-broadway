/*
	libbroadway - A general purpose library to control the Wii.
	DI support

Copyright (C) 2010-2011		Alex Marshall <trap15@raidenii.net>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <string.h>
#include <types.h>
#include <broadway.h>

/* When set during the reset, performs a hard reset instead of soft */
#define DI_BIT_HARDRESET	(1 << 2)
/* Unsure what this bit does, required to be set for all DI resetting */
#define DI_BIT_RESET		(1 << 0)

static irq_handler_t irq_di_handler_table[IRQ_DI_MAX];

int irq_di_register_handler(u32 irqn, int (*exec)(u32 irq, void* data), void* data)
{
	irq_di_handler_table[irqn % IRQ_DI_MAX].exec = exec;
	irq_di_handler_table[irqn % IRQ_DI_MAX].data = data;
	return 1;
}

irq_handler_t irq_di_get_handler(u32 irqn)
{
	return irq_di_handler_table[irqn % IRQ_DI_MAX];
}

static int _di_irq_handler(u32 irq, void* data)
{
	(void)irq;
	(void)data;
	u32 status, cover, newstatus = 0, newcover = 0;
	status = read32(DI_STATUS);
	cover = read32(DI_COVER);
	if((status & (3 << 1)) == (3 << 1)) {
		if(irq_di_handler_table[IRQ_DI_ERROR].exec)
			irq_di_handler_table[IRQ_DI_ERROR].exec(IRQ_DI_ERROR, irq_di_handler_table[IRQ_DI_ERROR].data);
		newstatus |= 3 << 1;
	}
	if((status & (3 << 3)) == (3 << 3)) {
		if(irq_di_handler_table[IRQ_DI_TC].exec)
			irq_di_handler_table[IRQ_DI_TC].exec(IRQ_DI_TC, irq_di_handler_table[IRQ_DI_TC].data);
		newstatus |= 3 << 3;
	}
	if((status & (3 << 5)) == (3 << 5)) {
		if(irq_di_handler_table[IRQ_DI_BREAK].exec)
			irq_di_handler_table[IRQ_DI_BREAK].exec(IRQ_DI_BREAK, irq_di_handler_table[IRQ_DI_BREAK].data);
		newstatus |= 3 << 5;
	}
	write32(DI_STATUS, newstatus);
	if((cover & (3 << 1)) == (3 << 1)) {
		if(irq_di_handler_table[IRQ_DI_COVER].exec)
			irq_di_handler_table[IRQ_DI_COVER].exec(IRQ_DI_COVER, irq_di_handler_table[IRQ_DI_COVER].data);
		newcover |= 3 << 1;
	}
	write32(DI_COVER, newcover);
	return 1;
}

int di_initialize(void)
{
	int i;
	for(i = 0; i < IRQ_DI_MAX; i++) {
		irq_di_register_handler(i, NULL, NULL);
		irq_di_disable(i);
	}
	if(!irq_bw_register_handler(IRQ_BW_DI, _di_irq_handler, NULL))
		return 0;
	irq_bw_enable(IRQ_BW_DI);
	return 1;
}

int di_shutdown(void)
{
	irq_bw_disable(IRQ_BW_DI);
	if(!irq_bw_register_handler(IRQ_BW_DI, NULL, NULL))
		return 0;
	return 1;
}

void di_reset(BOOL hard)
{
	u32 v;
	irq_di_enable(IRQ_DI_COVER);
	v = read32(PI_RESET);
	write32(PI_RESET, (v & ~DI_BIT_HARDRESET) | DI_BIT_RESET);
	usleep(20); /* Arbitrary pause time */
	if(hard)
		write32(PI_RESET, v | DI_BIT_HARDRESET | DI_BIT_RESET);
	else
		write32(PI_RESET, v | DI_BIT_RESET);
}

void irq_di_enable(u32 irq)
{
	switch(irq) {
		case IRQ_DI_BREAK:
			set32(DI_STATUS, 3 << 5);
			break;
		case IRQ_DI_TC:
			set32(DI_STATUS, 3 << 3);
			break;
		case IRQ_DI_ERROR:
			set32(DI_STATUS, 3 << 1);
			break;
		case IRQ_DI_COVER:
			set32(DI_COVER, 3 << 1);
			break;
	}
}

void irq_di_disable(u32 irq)
{
	switch(irq) {
		case IRQ_DI_BREAK:
			clear32(DI_STATUS, 1 << 5);
			break;
		case IRQ_DI_TC:
			set32(DI_STATUS, 1 << 3);
			break;
		case IRQ_DI_ERROR:
			set32(DI_STATUS, 1 << 1);
			break;
		case IRQ_DI_COVER:
			set32(DI_COVER, 1 << 1);
			break;
	}
}

BOOL di_cover_open(void)
{
	return (read32(DI_COVER) & 1) ? TRUE : FALSE;
}

void di_set_command(di_cmd_t cmd, u8 subcmd1, u16 subcmd2, u32 off, u32 len)
{
	write32(DI_COMMAND, (cmd << 24) | (subcmd1 << 16) | (subcmd2));
	write32(DI_CMD_OFFSET, off);
	write32(DI_CMD_LEN, len);
}

void di_set_dma(u32 addr, u32 len)
{
	addr &= ~0x1F;
	len &= ~0x1F;
	write32(DI_DMA_ADDR, addr);
	write32(DI_DMA_LEN, len);
}

u32 di_immediate_recv(void)
{
	return read32(DI_IMM_BUF);
}

void di_immediate_send(u32 data)
{
	write32(DI_IMM_BUF, data);
}

void di_transfer(BOOL write, BOOL dma)
{
	write32(DI_CONTROL, (write << 2) | (dma << 1) | 1);
}

BOOL di_transfer_ended(void)
{
	return (read32(DI_CONTROL) & 1) ? FALSE : TRUE;
}


