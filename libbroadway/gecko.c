/*
	libbroadway - A general purpose library to control the Wii.
	USBGecko support

Copyright (c) 2008		Nuke - <wiinuke@gmail.com>
Copyright (C) 2008, 2009	Hector Martin "marcan" <marcan@marcansoft.com>
Copyright (C) 2009		Andre Heider "dhewg" <dhewg@wiibrew.org>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <stdio.h>
#include <stdarg.h>
#include <broadway.h>
#include "../libnewintf/newintf.h"

static int gecko_console_enabled = 0;

u32 gecko_command(u32 command)
{
	u32 i;
	// Memory Card Port B (Channel 1, Device 0, Frequency 3 (32Mhz Clock))
	exi_set_clock(1, EXI_CLOCK_32MHz);
	exi_device_select(1, 0);
	exi_immediate_send(1, command);
	exi_set_immediate_length(1, 2);
	exi_transfer(1, EXI_TRANSFER_READ_WRITE, FALSE);
	i = 1000;
	while(!exi_transfer_ended(1) && (i--));
	i = exi_immediate_recv(1);
	exi_set_clock(1, EXI_CLOCK_1MHz);
	exi_device_deselect(1, 0);
	return i;
}

u32 gecko_getid(void)
{
	u32 i;
	// Memory Card Port B (Channel 1, Device 0, Frequency 3 (32Mhz Clock))
	exi_set_clock(1, EXI_CLOCK_32MHz);
	exi_device_select(1, 0);
	exi_immediate_send(1, 0);
	exi_set_immediate_length(1, 2);
	exi_transfer(1, EXI_TRANSFER_READ_WRITE, FALSE);
	while(!exi_transfer_ended(1));
	exi_set_immediate_length(1, 4);
	exi_transfer(1, EXI_TRANSFER_READ_WRITE, FALSE);
	while(!exi_transfer_ended(1));
	i = exi_immediate_recv(1);
	exi_set_clock(1, EXI_CLOCK_1MHz);
	exi_device_deselect(1, 0);
	return i;
}

u32 gecko_sendbyte(char sendbyte)
{
	u32 i = 0;
	i = gecko_command(0xB0000000 | (sendbyte<<20));
	if(i & 0x04000000)
		return 1; // Return 1 if byte was sent
	return 0;
}

int gecko_isalive(void)
{
	u32 i;

	i = gecko_getid();
	if(i != 0x00000000)
		return 0;

	i = gecko_command(0x90000000);
	if((i & 0xFFFF0000) != 0x04700000)
		return 0;

	return 1;
}

int gecko_sendbuffer(const void *buffer, u32 size)
{
	u32 left = size;
	char *ptr = (char*)buffer;

	while(left > 0) {
		if(!gecko_sendbyte(*ptr))
			break;
		ptr++;
		left--;
	}
	return (size - left);
}

static long _gecko_console_write(struct _reent* r, int fd, const void* buf, size_t cnt)
{
	(void)r;
	(void)fd;
	gecko_sendbuffer(buf, cnt);
	return cnt;
}

newlib_device_t gecko_console_device = {
	0, 0,
	"stderr", /* Tag */
	TRUE, /* Is TTY */
	NULL, /* open */
	NULL, /* close */
	NULL, /* lseek */
	NULL, /* read */
	_gecko_console_write, /* write */
	NULL, /* stat */
	NULL, /* fstat */
	NULL, /* link */
	NULL, /* unlink */
};

int gecko_initialize(void)
{
	/* unlock EXI */
	exi_set_lock(FALSE);

	if(!gecko_isalive())
		return 0;

	if(!newlib_add_device(gecko_console_device)) {
		return 0;
	}
	gecko_console_enabled = 1;
	return 1;
}

int gecko_shutdown(void)
{
	gecko_console_enabled = 0;
	if(!newlib_remove_device(gecko_console_device.tag))
		return 0;
	return 1;
}

int gecko_printf(const char *fmt, ...)
{
	if (!gecko_console_enabled)
		return 0;

	udelay(100); // evil hack :)

	va_list args;
	char buffer[1024];
	int i;

	va_start(args, fmt);
	i = vsprintf(buffer, fmt, args);
	va_end(args);

	return gecko_sendbuffer(buffer, i);
}

