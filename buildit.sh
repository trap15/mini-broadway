#!/bin/sh

# Copyright (C) 2007 Segher Boessenkool <segher@kernel.crashing.org>
# Copyright (C) 2009 Hector Martin "marcan" <hector@marcansoft.com>
# Copyright (C) 2009 Andre Heider "dhewg" <dhewg@wiibrew.org>
# Copyright (C) 2010 Alex Marshall "trap15" <trap15@raidenii.net>

# Released under the terms of the GNU GPL, version 2
SCRIPTDIR=`dirname $PWD/$0`

BINUTILS_VER=2.20.1
BINUTILS_DIR="binutils-$BINUTILS_VER"
BINUTILS_TARBALL="binutils-$BINUTILS_VER""a.tar.bz2"
BINUTILS_URI="http://ftp.gnu.org/gnu/binutils/$BINUTILS_TARBALL"

GMP_VER=5.0.1
GMP_DIR="gmp-$GMP_VER"
GMP_TARBALL="gmp-$GMP_VER.tar.bz2"
GMP_URI="http://ftp.gnu.org/gnu/gmp/$GMP_TARBALL"

MPFR_VER=2.4.2
MPFR_DIR="mpfr-$MPFR_VER"
MPFR_TARBALL="mpfr-$MPFR_VER.tar.bz2"
MPFR_URI="http://www.mpfr.org/mpfr-$MPFR_VER/$MPFR_TARBALL"

MPC_VER=0.8.2
MPC_DIR="mpc-$MPC_VER"
MPC_TARBALL="mpc-$MPC_VER.tar.gz"
MPC_URI="http://www.multiprecision.org/mpc/download/$MPC_TARBALL"

GCC_VER=4.4.4
GCC_DIR="gcc-$GCC_VER"
GCC_TARBALL="gcc-$GCC_VER.tar.bz2"
GCC_URI="http://ftp.gnu.org/gnu/gcc/gcc-$GCC_VER/$GCC_TARBALL"

GDB_VER=7.1
GDB_DIR="gdb-$GDB_VER"
GDB_TARBALL="gdb-$GDB_VER""a.tar.bz2"
GDB_URI="http://ftp.gnu.org/gnu/gdb/$GDB_TARBALL"

NEWLIB_VER=1.18.0
NEWLIB_DIR="newlib-$NEWLIB_VER"
NEWLIB_TARBALL="newlib-$NEWLIB_VER.tar.gz"
NEWLIB_URI="ftp://sources.redhat.com/pub/newlib/$NEWLIB_TARBALL"

DSPTOOL_URI="http://dolphin-emu.googlecode.com/svn/trunk/Source"
# Source for the DSPTool source. "Dolphin" means to gather from the SVN. Probably wont work usually.
# Anything else uses the built-in source code, which is already tested to work.
DSPSRC="Builtin"

BOOTMIICLIENT_VER_SMALL=83e1ed2
BOOTMIICLIENT_VER=83e1ed244d26474435d6f097c1a75b11504ce2ad
BOOTMIICLIENT_TARBALL="bootmii-utils-tree.tar.gz"
BOOTMIICLIENT_URI="http://gitweb.bootmii.org/?a=snapshot&p=bootmii-utils.git&h=$BOOTMIICLIENT_VER&t=tgz"

ARM_TARGET=armeb-eabi
POWERPC_TARGET=powerpc-elf

if [ -z $MAKEOPTS ]; then
	MAKEOPTS=-j3
fi

# End of configuration section.

case `uname -s` in
	*BSD*)
		MAKE=gmake
		;;
	*)
		MAKE=make
esac

export PATH=$WIIDEV/bin:$PATH

die() {
	echo $@
	exit 1
}

cleansrc() {
	[ -e $WIIDEV/$BINUTILS_DIR ] && rm -rf $WIIDEV/$BINUTILS_DIR
	[ -e $WIIDEV/$GCC_DIR ] && rm -rf $WIIDEV/$GCC_DIR
	[ -e $WIIDEV/$GDB_DIR ] && rm -rf $WIIDEV/$GDB_DIR
}

cleanbuild() {
	[ -e $WIIDEV/build_binutils ] && rm -rf $WIIDEV/build_binutils
	[ -e $WIIDEV/build_gcc ] && rm -rf $WIIDEV/build_gcc
	[ -e $WIIDEV/build_gdb ] && rm -rf $WIIDEV/build_gdb
	[ -e $WIIDEV/build_newlib ] && rm -rf $WIIDEV/build_newlib
	[ -e $WIIDEV/build_client ] && rm -rf $WIIDEV/build_client
	[ -e $WIIDEV/build_dsp ] && rm -rf $WIIDEV/build_dsp
}

download() {
	DL=1
	if [ -f "$WIIDEV/$2" ]; then
		echo "Testing $2..."
# Check bz2 and gz
		tar tjf "$WIIDEV/$2" >/dev/null 2>&1 && DL=0
		if [ $DL -eq 1 ]; then
			tar tzf "$WIIDEV/$2" >/dev/null 2>&1 && DL=0
		fi
	fi

	if [ $DL -eq 1 ]; then
		echo "Downloading $2..."
		wget "$1" -c -O "$WIIDEV/$2" || die "Could not download $2"
	fi
}

download2() {
	echo "Downloading $3..."
	wget "$DSPTOOL_URI/$1/$3" -c -O "$WIIDEV/build_dsp/$2/$3" || die "Could not download $3"
}

download3() {
	echo "Downloading $1..."
	wget "$BOOTMIICLIENT_URI=$1" -c -O "$WIIDEV/build_client/$1" || die "Could not download $1"
}

extract() {
	echo "Extracting $1..."
	tar xf "$WIIDEV/$1" -C "$2" || die "Error unpacking $1"
}

makedirs() {
	mkdir -p $WIIDEV/build_binutils || die "Error making binutils build directory $WIIDEV/build_binutils"
	mkdir -p $WIIDEV/build_gcc || die "Error making gcc build directory $WIIDEV/build_gcc"
	mkdir -p $WIIDEV/build_gdb || die "Error making gdb build directory $WIIDEV/build_gdb"
	mkdir -p $WIIDEV/build_newlib || die "Error making newlib build directory $WIIDEV/build_newlib"
	mkdir -p $WIIDEV/build_client || die "Error making bootmii client build directory $WIIDEV/build_client"
	mkdir -p $WIIDEV/build_dsp || die "Error making dsp assembler build directory $WIIDEV/build_dsp"
}

buildbinutils() {
	TARGET=$1
	(
		cd $WIIDEV/build_binutils && \
		$WIIDEV/$BINUTILS_DIR/configure --target=$TARGET \
			--prefix=$WIIDEV --disable-werror --disable-multilib && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building binutils for target $TARGET"
}

buildgccarm() {
	TARGET=$1
	(
		cd $WIIDEV/build_gcc && \
		$WIIDEV/$GCC_DIR/configure --target=$TARGET --enable-targets=all \
			--prefix=$WIIDEV --disable-multilib \
			--enable-languages=c --without-headers \
			--disable-nls --disable-threads --disable-shared \
			--disable-libmudflap --disable-libssp --disable-libgomp \
			--disable-decimal-float \
			--enable-checking=release && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building gcc for target $TARGET"
}

buildgccppc() {
# TODO: Figure out why C++ doesn't build
	TARGET=$1
	NEWLIBFLAG=$2
	(
		cd $WIIDEV/build_gcc && \
		$WIIDEV/$GCC_DIR/configure --target=$TARGET --enable-targets=all \
			--prefix=$WIIDEV --disable-multilib \
			--enable-languages="c" --disable-libssp \
			--enable-checking=release $NEWLIBFLAG && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building gcc for target $TARGET $NEWLIBFLAG"
}

buildnewlib() {
	TARGET=$1
	(
		cd $WIIDEV/build_newlib && \
		$WIIDEV/$NEWLIB_DIR/configure --target=$TARGET \
			--prefix=$WIIDEV && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building newlib for target $TARGET"
}

buildgdb() {
	TARGET=$1
	(
		cd $WIIDEV/build_gdb && \
		$WIIDEV/$GDB_DIR/configure --target=$TARGET \
			--prefix=$WIIDEV --disable-werror --disable-multilib \
			--disable-sim && \
		$MAKE $MAKEOPTS && \
		$MAKE install
	) || die "Error building gdb for target $TARGET"
}


buildarm() {
	echo "******* Building ARM toolchain"
	download "$BINUTILS_URI" "$BINUTILS_TARBALL"
	download "$GMP_URI" "$GMP_TARBALL"
	download "$MPFR_URI" "$MPFR_TARBALL"
	download "$GCC_URI" "$GCC_TARBALL"
	download "$GDB_URI" "$GDB_TARBALL"

	cleansrc

	extract "$BINUTILS_TARBALL" "$WIIDEV"
	extract "$GCC_TARBALL" "$WIIDEV"
	extract "$GDB_TARBALL" "$WIIDEV"
	extract "$GMP_TARBALL" "$WIIDEV/$GCC_DIR"
	mv "$WIIDEV/$GCC_DIR/$GMP_DIR" "$WIIDEV/$GCC_DIR/gmp" || die "Error renaming $GMP_DIR -> gmp"
	extract "$MPFR_TARBALL" "$WIIDEV/$GCC_DIR"
	mv "$WIIDEV/$GCC_DIR/$MPFR_DIR" "$WIIDEV/$GCC_DIR/mpfr" || die "Error renaming $MPFR_DIR -> mpfr"

	cleanbuild
	makedirs
	echo "******* Building ARM binutils"
	buildbinutils $ARM_TARGET
	echo "******* Building ARM GCC"
	buildgccarm $ARM_TARGET
	echo "******* Building ARM GDB"
	buildgdb $ARM_TARGET
	echo "******* ARM toolchain built and installed"
}

buildpowerpc() {
	echo "******* Building PowerPC toolchain"
	download "$NEWLIB_URI" "$NEWLIB_TARBALL"
	download "$BINUTILS_URI" "$BINUTILS_TARBALL"
	download "$GMP_URI" "$GMP_TARBALL"
	download "$MPFR_URI" "$MPFR_TARBALL"
	download "$MPC_URI" "$MPC_TARBALL"
	download "$GCC_URI" "$GCC_TARBALL"
	download "$GDB_URI" "$GDB_TARBALL"

	cleansrc

	extract "$BINUTILS_TARBALL" "$WIIDEV"
	extract "$GCC_TARBALL" "$WIIDEV"
	extract "$NEWLIB_TARBALL" "$WIIDEV"
	extract "$GDB_TARBALL" "$WIIDEV"
	extract "$GMP_TARBALL" "$WIIDEV/$GCC_DIR"
	mv "$WIIDEV/$GCC_DIR/$GMP_DIR" "$WIIDEV/$GCC_DIR/gmp" || die "Error renaming $GMP_DIR -> gmp"
	extract "$MPFR_TARBALL" "$WIIDEV/$GCC_DIR"
	mv "$WIIDEV/$GCC_DIR/$MPFR_DIR" "$WIIDEV/$GCC_DIR/mpfr" || die "Error renaming $MPFR_DIR -> mpfr"
	extract "$MPC_TARBALL" "$WIIDEV/$GCC_DIR"
	mv "$WIIDEV/$GCC_DIR/$MPC_DIR" "$WIIDEV/$GCC_DIR/mpc" || die "Error renaming $MPC_DIR -> mpc"

	cleanbuild
	makedirs
	echo "******* Building PowerPC binutils"
	buildbinutils $POWERPC_TARGET
	echo "******* Building PowerPC Base GCC"
	buildgccppc $POWERPC_TARGET
	echo "******* Building PowerPC Newlib"
	buildnewlib $POWERPC_TARGET
	echo "******* Building PowerPC GCC"
	buildgccppc $POWERPC_TARGET --with-newlib
	echo "******* Building PowerPC GDB"
	buildgdb $POWERPC_TARGET
	echo "******* PowerPC toolchain built and installed"
}

dspgather_dolphin() {
	mkdir -p "$WIIDEV/build_dsp/Common"
	mkdir -p "$WIIDEV/build_dsp/DSP"
	download2 "DSPTool/Src" "" "DSPTool.cpp"
	download2 "Core/Common/Src" "Common" "Atomic_GCC.h"
	download2 "Core/Common/Src" "Common" "Atomic_Win32.h"
	download2 "Core/Common/Src" "Common" "Atomic.h"
	download2 "Core/Common/Src" "Common" "ChunkFile.h"
	download2 "Core/Common/Src" "Common" "Common.h"
	download2 "Core/Common/Src" "Common" "CommonFuncs.h"
	download2 "Core/Common/Src" "Common" "CommonPaths.h"
	download2 "Core/Common/Src" "Common" "CommonTypes.h"
	download2 "Core/Common/Src" "Common" "DebugInterface.h"
	download2 "Core/Common/Src" "Common" "FileUtil.h"
	download2 "Core/Common/Src" "Common" "FixedSizeQueue.h"
	download2 "Core/Common/Src" "Common" "Hash.h"
	download2 "Core/Common/Src" "Common" "Log.h"
	download2 "Core/Common/Src" "Common" "LogManager.h"
	download2 "Core/Common/Src" "Common" "MemoryUtil.h"
	download2 "Core/Common/Src" "Common" "MsgHandler.h"
	download2 "Core/Common/Src" "Common" "Setup.h"
	download2 "Core/Common/Src" "Common" "StringUtil.h"
	download2 "Core/Common/Src" "Common" "Timer.h"
	download2 "Core/Common/Src" "Common" "FileUtil.cpp"
	download2 "Core/Common/Src" "Common" "LogManager.cpp"
	download2 "Core/Common/Src" "Common" "Hash.cpp"
	download2 "Core/Common/Src" "Common" "MemoryUtil.cpp"
	download2 "Core/Common/Src" "Common" "Misc.cpp"
	download2 "Core/Common/Src" "Common" "MsgHandler.cpp"
	download2 "Core/Common/Src" "Common" "StringUtil.cpp"
	download2 "Core/Common/Src" "Common" "Timer.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "assemble.h"
	download2 "Core/Core/Src/DSP" "DSP" "disassemble.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPAccelerator.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPAnalyzer.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPBreakpoints.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPCodeUtil.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPCommon.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPCore.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPHost.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPHWInterface.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPIntUtil.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPMemoryMap.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPStacks.h"
	download2 "Core/Core/Src/DSP" "DSP" "DSPTables.h"
	download2 "Core/Core/Src/DSP" "DSP" "LabelMap.h"
	download2 "Core/Core/Src/DSP" "DSP" "assemble.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "disassemble.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPAccelerator.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPAnalyzer.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPCodeUtil.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPCore.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPHWInterface.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPMemoryMap.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPStacks.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "DSPTables.cpp"
	download2 "Core/Core/Src/DSP" "DSP" "LabelMap.cpp"
}

dspgather_builtin() {
	cp -R dsptool/* "$WIIDEV/build_dsp"
}

builddsp() {
	cleanbuild
	makedirs
	echo "******* Gathering DSP assembler sources"
	if [ $DSPSRC = "Dolphin" ]; then
		dspgather_dolphin
	else
		dspgather_builtin
	fi
	echo "******* Applying patches"
	cp dspcore.patch $WIIDEV/build_dsp/DSP/
	cp common.patch $WIIDEV/build_dsp/Common/
	cp Makefile.dsp $WIIDEV/build_dsp/Makefile
	cd $WIIDEV/build_dsp/DSP && \
	patch -p1 < dspcore.patch
	cd $WIIDEV/build_dsp/Common && \
	patch -p1 < common.patch
	echo "******* Building DSP assembler"
	cd $WIIDEV/build_dsp && \
	$MAKE && \
	$MAKE install
	echo "******* DSP assembler built and installed"
}

buildclient() {
	cleanbuild
	makedirs
	echo "******* Gathering BootMii Client sources"
	download "$BOOTMIICLIENT_URI" "$BOOTMIICLIENT_TARBALL"
	cp $SCRIPTDIR/common/common.mk "$WIIDEV/build_client/"
	echo "bootmii: \$(OBJS) \$(NOLINKOBJS)" >> "$WIIDEV/build_client/common.mk"
	echo "	@echo \"  LINK      \$@\"" >> "$WIIDEV/build_client/common.mk"
	echo "	@\$(LD) \$(LDFLAGS) \$(OBJS) \$(LIBS) \$(SYSLIBS) -o \$@" >> "$WIIDEV/build_client/common.mk"
	mkdir -p "$WIIDEV/build_client/code"
	extract "$BOOTMIICLIENT_TARBALL" "$WIIDEV/build_client/code"
	mv $WIIDEV/build_client/code/bootmii-utils-$BOOTMIICLIENT_VER_SMALL/* "$WIIDEV/build_client/code"
	echo "******* Building BootMii Client"
	cd $WIIDEV/build_client/code && \
	$MAKE install
	echo "******* BootMii Client built and installed"
}

if [ -z "$WIIDEV" ]; then
	die "Please set WIIDEV in your environment."
fi

if [ $# -eq 0 ]; then
	die "Please specify build type(s) (all/arm/powerpc/dsp/client/clean)"
fi

if [ "$1" = "all" ]; then
	buildpowerpc
	buildarm
	builddsp
	buildclient
	cleanbuild
	cleansrc
	exit 0
fi

while true; do
	if [ $# -eq 0 ]; then
		exit 0
	fi
	case $1 in
		arm)		buildarm ;;
		powerpc)	buildpowerpc ;;
		dsp)		builddsp ;;
		client)		buildclient ;;
		clean)		cleanbuild; cleansrc; exit 0 ;;
		*)
			die "Unknown build type $1"
			;;
	esac
	shift;
done

