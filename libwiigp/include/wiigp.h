/*
	libwiigp - a GX API for BootMii

Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>
Copyright (C) 2009		Thomas Daede <bztdlinux@gmail.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#ifndef __WIIGP_H__
#define __WIIGP_H__

/*! \brief Initialize the WiiGP library.
 *
 * \return If initialization was successful, returns 1. Otherwise 0.
 * \sa wiigp_shutdown()
 */
int wiigp_initialize(void);
/*! \brief Shut down the WiiGP library.
 *
 * \return If shut down was successful, returns 1. Otherwise 0.
 * \sa wiigp_initialize()
 */
int wiigp_shutdown(void);

/* BP functions */
/*! \brief Set the EFB copy box on the BP.
 *
 * \param x X coordinates to top-left of rectangle in EFB to copy.
 * \param y Y coordinates to top-left of rectangle in EFB to copy.
 * \param w (Width-1) of the rectangle in EFB to copy.
 * \param h (Height-1) of the rectangle in EFB to copy.
 */
void wiigp_bp_copy_box(u16 x, u16 y, u16 w, u16 h);
/*! \brief Set the XFB address and stride.
 *
 * \param xfb Address of XFB. Must be a physical address!
 * \param stride Row stride of the XFB. 10 bits.
 */
void wiigp_bp_set_xfb(u32* xfb, u16 stride);
/*! \brief Begins a BP copy.
 *
 * \param clear If TRUE, the EFB is cleared during the copy. Otherwise it is not.
 */
void wiigp_bp_start_copy(BOOL clear);
/*! \brief Sets the copy clear color.
 *
 * The EFB is filled with this color during copy clearing.
 * \param a Alpha element.
 * \param r Red element.
 * \param g Green element.
 * \param b Blue element.
 * \param depth Clear depth.
 */
void wiigp_bp_copy_clear(u8 a, u8 r, u8 g, u8 b, float depth)
/*! \brief Sets BP filters.
 *
 * \param f The 4 filter values.
 */
void wiigp_bp_set_filter(u32 f[4]);
/*! \brief Sets BP VFilters.
 *
 * \param f The 7 vertical filters.
 */
void wiigp_bp_set_vfilter(u8 f[7]);

#endif

