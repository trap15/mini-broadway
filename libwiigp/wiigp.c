/*
	libwiigp - a GX API for BootMii

Copyright (C) 2009-2010		Alex Marshall <trap15@raidenii.net>
Copyright (C) 2009		Thomas Daede <bztdlinux@gmail.com>

# This code is licensed to you under the terms of the GNU GPL, version 2;
# see file COPYING or http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt
*/

#include <malloc.h>
#include <string.h>

int wiigp_initialize(void)
{
	return 1;
}

int wiigp_shutdown(void)
{
	return 1;
}

void wiigp_bp_copy_box(u16 x, u16 y, u16 w, u16 h)
{
}

void wiigp_bp_set_xfb(u32 *xfb, u16 stride)
{
}

void wiigp_bp_start_copy(BOOL clear)
{
}

void wiigp_bp_copy_clear(u8 a, u8 r, u8 g, u8 b, float depth)
{
}

void wiigp_bp_set_filter(u32 f[4])
{
}

void wiigp_bp_set_vfilter(u8 f[7])
{
}

